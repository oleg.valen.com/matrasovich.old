<?php

use Yii;

return [
    'adminEmail' => 'matrasovich.com@gmail.com',
    'titleEnd' => ' | Купить в Днепре, Киеве, Харькове: цена, отзывы, продажа',
    'descriptionEnd' => ' ☎ 099-267-83-20',
];

<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="breadcrumbs">
    <div class="container is-size-6">
        <?= $breadcrumbs ?>
    </div>
</div>

<section class="section">
    <div class="container">
        <h1 class="title"><?= $blog->blogDescriptions->name ?></h1>
        <div class="content is-clearfix">
            <?= \yii\helpers\Html::decode($blog->blogDescriptions->text) ?>
        </div>
    </div>
</section>
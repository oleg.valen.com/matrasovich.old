<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use Yii;
use app\assets\CartAsset;

CartAsset::register($this);

?>

<section class="section container content">
    <h1 class="title"><?= Yii::t('app', 'Корзина'); ?></h1>
    <div class="table__cart">
    </div>

    <?php $form = ActiveForm::begin(['id' => 'cart-form', 'options' => ['class' => 'cart-form']]); ?>
    <label class="label"><?= Yii::t('app', 'Как с Вами связаться'); ?></label>
    <div class="columns">
        <div class="column">
            <?= $form->field($modelCartForm, 'telephone')
                ->textInput([
                    'autofocus' => true,
                    'placeholder' => 'Телефон',
                    'class' => 'input is-primary'])
                ->label(false) ?>
        </div>
        <div class="column">
            <?= $form->field($modelCartForm, 'lastName')
                ->textInput([
                    'placeholder' => 'Фамилия',
                    'class' => 'input is-primary'])
                ->label(false) ?>
        </div>
        <div class="column">
            <?= $form->field($modelCartForm, 'firstName')
                ->textInput([
                    'placeholder' => 'Имя',
                    'class' => 'input is-primary'])
                ->label(false) ?>
        </div>
        <div class="column">
            <?= $form->field($modelCartForm, 'middleName')
                ->textInput([
                    'placeholder' => 'Отчество',
                    'class' => 'input is-primary'])
                ->label(false) ?>
        </div>
    </div>
    <div class="columns">
        <div class="column">
            <?= $form->field($modelCartForm, 'serviceType')->radioList([
                'DoorsWarehouse' => 'Отделение',
                'DoorsDoors' => 'Адрес',
            ]); ?>
        </div>
    </div>
    <div class="columns search__container">
        <div class="column is-one-quarter" data-search-handler="city">
            <div class="control">
                <?= $form->field($modelCartForm, 'city')
                    ->textInput([
                        'placeholder' => 'Нас. пункт',
                        'class' => 'input is-primary search__city'])
                    ->label(false) ?>
            </div>
        </div>
        <div class="column is-one-quarter cart-form__item active cart-form__by-hand cart-form__warehouse"
             data-search-handler="warehouse">
            <div class="control">
                <?= $form->field($modelCartForm, 'warehouse')
                    ->textInput([
                        'placeholder' => 'Отделение',
                        'class' => 'input is-primary search__warehouse'])
                    ->label(false) ?>
            </div>
        </div>
        <div class="column is-one-quarter cart-form__item cart-form__by-hand cart-form__doors"
             data-search-handler="street">
            <div class="control">
                <?= $form->field($modelCartForm, 'street')
                    ->textInput([
                        'placeholder' => 'Улица',
                        'class' => 'input is-primary search__street'])
                    ->label(false) ?>
            </div>
        </div>
        <div class="column is-1 cart-form__item cart-form__by-hand cart-form__doors">
            <?= $form->field($modelCartForm, 'building')
                ->textInput([
                    'placeholder' => 'Дом',
                    'class' => 'input is-primary'])
                ->label(false) ?>
        </div>
        <div class="column is-1 cart-form__item">
            <?= $form->field($modelCartForm, 'flat')
                ->textInput([
                    'placeholder' => 'Квартира',
                    'class' => 'input is-primary'])
                ->label(false) ?>
        </div>

    </div>

    <div class="buttons">
        <button type="submit"
                class="button is-warning ok-cart-order has-text-weight-bold"><?= Yii::t('app', 'Заказать'); ?></button>
        <button class="button is-warning ok-cart-delete-all"><?= Yii::t('app', 'Удалить'); ?></button>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<div class="modal ok-modal-cart">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="container notification is-warning">
            <!--            <button class="delete"></button>-->
            <p class="thank-you">Спасибо за заказ!</p>
            <p>В ближайшее время с вами свяжется наш менеджер!</p>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>

<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\SearchForm;
use simialbi\yii2\schemaorg\helpers\JsonLDHelper;

AppAsset::register($this);
$modelSearchForm = $this->params['modelSearchForm'];
if (is_null($modelSearchForm)) {
    $modelSearchForm = new SearchForm();
}
Yii::$app->params['language'] = Yii::$app->language == 'ru' ? 'ua' : 'ru';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"<?= isset(Yii::$app->params['ogprefix']) ? Yii::$app->params['ogprefix'] : '' ?>>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="<?= Url::to('@web/images/fav.png') ?>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?= Url::to('@web/images/fav.png') ?>"
          type="image/x-icon"/>
    <!--    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" data-auto-replace-svg="nest"></script>
    <?php $this->head() ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-106330932-1', 'auto');
        ga('send', 'pageview');
    </script>
    <?php if (isset(Yii::$app->params['opengraph'])): ?>
        <?= Yii::$app->params['opengraph']->render(); ?>
    <?php endif; ?>
    <?php if (!empty(Yii::$app->params['jsonld'])): ?>
        <?php JsonLDHelper::render(); ?>
    <?php endif; ?>
    <!-- Yandex.Metrika counter -->
    <!--    <script type="text/javascript">-->
    <!--        (function (m, e, t, r, i, k, a) {-->
    <!--            m[i] = m[i] || function () {-->
    <!--                (m[i].a = m[i].a || []).push(arguments)-->
    <!--            };-->
    <!--            m[i].l = 1 * new Date();-->
    <!--            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)-->
    <!--        })-->
    <!--        (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");-->
    <!---->
    <!--        ym(57361828, "init", {-->
    <!--            clickmap: true,-->
    <!--            trackLinks: true,-->
    <!--            accurateTrackBounce: true,-->
    <!--            webvisor: true-->
    <!--        });-->
    <!--    </script>-->
    <!-- /Yandex.Metrika counter -->
</head>

<body>
<input type="hidden" name="_language" value="<?= Yii::$app->language ?>">
<section class="hero">
    <div class="hero-head is-hidden-mobile has-background-white-ter">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand">
                    <a href="<?= Url::to(['/']) ?>" style="padding-left: 0.4em; padding-top: 0.4em;"
                       title="<?= Yii::t('app', 'Интернет-магазин топперов на диван, ортопедических матрасов и спальных аксессуаров'); ?>">
                        <img src="<?= Url::to('@web/images/logo.png') ?>" style="height: 3em;"
                             alt="<?= Yii::t('app', 'Интернет-магазин топперов на диван, ортопедических матрасов и спальных аксессуаров'); ?>">
                    </a>
                    <div class="navbar-burger burger navbar-burger-a" data-target="navbarExampleTransparentExample">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div id="navbarMenuHeroA" class="navbar-menu navbar-menu-a">
                    <div class="navbar-end">
                        <?php $form = ActiveForm::begin(['action' => ['site/search'], 'method' => 'get']); ?>
                        <div class="navbar-item has-text-grey-darker field has-addons">
                            <div class="control">
                                <?= $form->field($modelSearchForm, 'q', [
                                    'inputOptions' => [
                                        'placeholder' => Yii::t('app', 'Поиcк'),
                                        'class' => 'input is-primary',
                                        'name' => 'q',
                                    ],
                                    'template' => '{input}',
                                ])
                                    ->label(false) ?>
                            </div>
                            <div class="control">
                                <button class="button is-primary"><?= Yii::t('app', 'Поиcк') ?></button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                        <a class="navbar-item has-text-grey-darker" href="<?= Url::to(['site/about-us']) ?>"
                           title="<?= Yii::t('app', 'О нас'); ?>"><?= Yii::t('app', 'О нас'); ?></a>
                        <a class="navbar-item has-text-grey-darker" href="<?= Url::to(['site/dostavka-i-oplata']) ?>"
                           title="<?= Yii::t('app', 'Доставка и оплата'); ?>"><?= Yii::t('app', 'Доставка и оплата'); ?></a>
                        <a class="navbar-item has-text-grey-darker" href="<?= Url::to(['site/garantiya-i-vozvrat']) ?>"
                           title="<?= Yii::t('app', 'Гарантия и возврат'); ?>"><?= Yii::t('app', 'Гарантия и возврат'); ?></a>
                        <div class="navbar-item">
                            <a href="tel:099-267-83-20" class="is-size-5"><strong
                                        class="has-text-grey-dark">099 267 83 20</strong></a>
                            <a class="is-hidden-touch" title="Viber" href="viber://chat?number=380986823617"><span
                                        class="icon is-medium" style="color: #59267C">
                                    <i class="fab fa-viber fa-lg"></i></span></a>
                            <a class="is-hidden-desktop" title="Viber"
                               href="viber://add?number=380992678320">
                    <span class="icon is-medium" style="color: #59267C"><i
                                class="fab fa-viber fa-lg"></i></span></a>
                            <a title="Telegram" href="tg://resolve?domain=matrasovich"><span
                                        class="icon is-medium" style="color: #55acee"><i
                                            class="fab fa-telegram fa-lg"></i></span></a>
                        </div>
                        <div class="navbar-item" id="navbar-item__language">
                            <a class="button is-primary is-outlined"
                               href="<?= urldecode(Url::current(['language' => Yii::$app->params['language']])) ?>">
                                <?= Yii::$app->params['language']; ?>
                            </a></div>
                        <a class="navbar-item has-text-primary" href="<?= Url::to(['site/favorites']) ?>"
                           title="<?= Yii::t('app', 'Избранное'); ?>">
                            <span class="has-badge-rounded has-badge-warning icons-header__favorites"><span
                                        class="icon is-medium"><i
                                            class="fas fa-heart fa-lg"
                                            style="cursor: pointer"></i></span></span>
                        </a>
                        <a class="navbar-item has-text-primary" href="<?= Url::to(['site/compare']) ?>"
                           title="<?= Yii::t('app', 'Сравнение'); ?>">
                            <span class="has-badge-rounded has-badge-warning icons-header__compare"><span
                                        class="icon is-medium"><i
                                            class="fas fa-balance-scale fa-lg"
                                            style="cursor: pointer"></i></span></span>
                        </a>
                        <a class="navbar-item has-text-primary" href="<?= Url::to(['site/cart']) ?>"
                           title="<?= Yii::t('app', 'Корзина'); ?>">
                            <span class="has-badge-rounded has-badge-warning icons-header__cart"><span
                                        class="icon is-medium"><i
                                            class="fas fa-shopping-cart fa-lg"
                                            style="cursor: pointer"></i></span></span>
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <div class="ok-hero-head is-hidden-tablet has-background-white-ter">
        <nav class="navbar">
            <div class="is-pulled-left">
                <a href="<?= Url::to(['/']) ?>" style="padding-left: 0.4em; padding-top: 0.6em;"
                   title="Интернет-магазин топперов на диван, ортопедических матрасов и спальных аксессуаров">
                    <img src="<?= Url::to('@web/images/logo.png') ?>"
                         style="height: 2em;"
                         alt="Интернет-магазин топперов на диван, ортопедических матрасов и спальных аксессуаров">
                </a>
            </div>
            <div class="is-pulled-right">
                <ul>
                    <li><a class="has-text-primary" href="#contacts" title="<?= Yii::t('app', 'Контакты'); ?>"><span
                                    class="icon is-medium"><i
                                        class="fas fa-phone fa-lg"></i></span></a>
                    <li><a class="has-text-primary" href="<?= Url::to(['site/favorites']) ?>"
                           title="<?= Yii::t('app', 'Избранное'); ?>">
                            <span class="has-badge-rounded has-badge-warning icons-header__favorites"><span
                                        class="icon"><i
                                            class="fas fa-heart fa-lg"
                                            style="cursor: pointer"></i></span></span>
                        </a></li>
                    <li><a class="has-text-primary" href="<?= Url::to(['site/compare']) ?>"
                           title="<?= Yii::t('app', 'Сравнение'); ?>">
                            <span class="has-badge-rounded has-badge-warning icons-header__compare"><span
                                        class="icon"><i
                                            class="fas fa-balance-scale fa-lg"
                                            style="cursor: pointer"></i></span></span>
                        </a></li>
                    <li><a class="has-text-primary" href="<?= Url::to(['site/cart']) ?>"
                           title="<?= Yii::t('app', 'Корзина'); ?>">
                            <span class="has-badge-rounded has-badge-warning icons-header__cart"><span
                                        class="icon"><i
                                            class="fas fa-shopping-cart fa-lg"
                                            style="cursor: pointer"></i></span></span>
                        </a></li>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <!-- Hero footer: will stick at the bottom -->
    <div class="hero-foot">

        <div class="is-hidden-tablet">
            <div class="navbar-brand">
                <div class="navbar-burger burger navbar-burger-b" data-target="navbarExampleTransparentExample"
                     style="margin-left: unset;">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="language__mobile">
                    <a class="button is-primary is-outlined"
                       href="<?= urldecode(Url::current(['language' => Yii::$app->params['language']])) ?>">
                        <?= Yii::$app->params['language']; ?>
                    </a></div>
            </div>
            <div id="navbarMenuHeroB" class="navbar-menu navbar-menu-b">
                <div class="navbar-end">
                    <?php $form = ActiveForm::begin(['action' => ['site/search'], 'method' => 'get']); ?>
                    <div class="has-text-grey-darker field has-addons">
                        <div class="control">
                            <?= $form->field($modelSearchForm, 'q', [
                                'inputOptions' => [
                                    'placeholder' => Yii::t('app', 'Поиcк'),
                                    'class' => 'input is-primary',
                                    'name' => 'q',
                                ],
                                'template' => '{input}',
                            ])
                                ->label(false) ?>
                        </div>
                        <div class="control">
                            <button class="button is-primary"><?= Yii::t('app', 'Поиcк') ?></button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <a href="<?= Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c']) ?>"
                       class="navbar-item"><?= Yii::t('app', 'Матрасы на диван') ?></a>
                    <a href="<?= Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c']) ?>"
                       class="navbar-item"><?= Yii::t('app', 'Ортопедические матрасы') ?></a>
                    <a href="<?= Url::to(['category/index', 'category_id' => 5, 's1' => 'namatrasniki', 's2' => 'c']) ?>"
                       class="navbar-item "><?= Yii::t('app', 'Наматрасники') ?></a>
                    <a href="<?= Url::to(['category/index', 'category_id' => 6, 's1' => 'podushki', 's2' => 'c']) ?>"
                       class="navbar-item "><?= Yii::t('app', 'Подушки') ?></a>
                    <a href="<?= Url::to(['category/index', 'category_id' => 7, 's1' => 'odeyala', 's2' => 'c']) ?>"
                       class="navbar-item "><?= Yii::t('app', 'Одеяла') ?></a>
                </div>
            </div>
        </div>

        <div class="tabs is-hidden-mobile has-background-black-ter">
            <div class="container">
                <ul>
                    <li><a href="<?= Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4"><?= Yii::t('app', 'Матрасы на диван') ?></a>
                    </li>
                    <li><a href="<?= Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4"><?= Yii::t('app', 'Ортопедические матрасы') ?></a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['category/index', 'category_id' => 5, 's1' => 'namatrasniki', 's2' => 'c']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4"><?= Yii::t('app', 'Наматрасники') ?></a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['category/index', 'category_id' => 6, 's1' => 'podushki', 's2' => 'c']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4"><?= Yii::t('app', 'Подушки') ?></a>
                    </li>
                    <li><a href="<?= Url::to(['category/index', 'category_id' => 7, 's1' => 'odeyala', 's2' => 'c']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4"><?= Yii::t('app', 'Одеяла') ?></a>
                    </li>
                </ul>
            </div>
        </div>

    </div>

</section>

<?php $this->beginBody() ?>
<main><?= $content ?></main>

<section class="container content section">
    <div class="ok-last-seen-products"></div>
</section>

<div class="scroll-top">
    <span class="icon has-text-primary"><i class="fas fa-chevron-circle-up"></i></span>
</div>
<section class="section is-clearfix" id="contacts">
    <div class="container">
        <div class="columns">
            <div class="column level is-one-third-tablet">
                <class class="level-item is-pulled-left">
            <span class="icon has-text-grey-light is-small ok-main-contact-icons"><i
                        class="fas fa-phone-square"></i></span>
                    <a href="tel:099-267-83-20" class="is-size-4"><strong
                                class="has-text-grey-dark">099 267 83 20</strong></a></class>
            </div>
            <div class="column level is-one-quarter-mobile">
                <class class="level-item is-pulled-left">
        <span class="icon has-text-grey-light is-small ok-main-contact-icons"><i
                    class="fas fa-pen"></i></span>
                    <a class="ok-main-connect-icons is-hidden-touch" title="Viber"
                       href="viber://chat?number=380992678320">
                    <span class="icon is-medium" style="color: #59267C"><i
                                class="fab fa-viber fa-2x"></i></span></a>
                    <a class="ok-main-connect-icons is-hidden-desktop" title="Viber"
                       href="viber://add?number=380992678320">
                    <span class="icon is-medium" style="color: #59267C"><i
                                class="fab fa-viber fa-2x"></i></span></a>
                    <a class="ok-main-connect-icons" title="Telegram" href="tg://resolve?domain=matrasovich"><span
                                class="icon has-text-info is-medium" style="color: #55acee"><i
                                    class="fab fa-telegram fa-2x"></i></span></a>
                </class>
            </div>
        </div>
    </div>
</section>

<footer class="hero">
    <div class="hero-head has-background-black-ter">
        <section class="section container">
            <div class="columns">
                <div class="column is-one-quarter">
                    <span class="subtitle has-text-grey-lighter"><?= Yii::t('app', 'Информация') ?></span>
                    <ul>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/about-us']) ?>"><?= Yii::t('app', 'О нас') ?></a></li>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/stocks']) ?>"><?= Yii::t('app', 'Наличие на складе') ?></a>
                        </li>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/dostavka-i-oplata']) ?>"><?= Yii::t('app', 'Доставка и оплата') ?></a>
                        </li>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/garantiya-i-vozvrat']) ?>"><?= Yii::t('app', 'Гарантия и возврат') ?></a>
                        </li>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/kontrol-kachestva']) ?>"><?= Yii::t('app', 'Контроль качества') ?></a>
                        </li>
                </div>
                <div class="column is-one-quarter">
                    <span class="subtitle has-text-grey-lighter"><?= Yii::t('app', 'Разное') ?></span>
                    <ul>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/pravila-ekspluatacii-matrasa']) ?>"><?= Yii::t('app', 'Правила эксплуатации матраса') ?>
                            </a></li>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/privacy']) ?>"><?= Yii::t('app', 'Политика безопасности') ?></a>
                        </li>
                        <li><a class="has-text-grey-light"
                               href="<?= Url::to(['site/terms']) ?>"><?= Yii::t('app', 'Условия соглашения') ?></a></li>
                    </ul>
                </div>
                <div class="column is-one-quarter">
                    <span class="subtitle has-text-grey-lighter"><?= Yii::t('app', 'График работы') ?></span>
                    <ul class="has-text-grey-light">
                        <li><?= Yii::t('app', 'Пн - Вс с 9 до 21') ?></li>
                        <li><?= Yii::t('app', 'Без выходных') ?></li>
                    </ul>
                </div>
                <div class="column is-one-quarter">
                    <img src="<?= Url::to('@web/images/visamastercard.png') ?> " style="height: 2.5em;"
                         alt="Visa, MasterCard">
                </div>
            </div>
        </section>
    </div>
</footer>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>


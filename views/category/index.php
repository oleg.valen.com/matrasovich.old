<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->registerMetaTag(['name' => 'description', 'content' => $description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $keywords]);
$this->registerMetaTag(['name' => 'robots', 'content' => $robots]);
$this->title = $title;
?>
<div id="breadcrumbs">
    <div class="container is-size-7-mobile is-size-6-tablet">
        <?= $breadcrumbs ?>
    </div>
</div>

<section class="section container content ok-content">
    <header><h1 class="title is-size-4"><?= $h1 ?></h1></header>
    <span class="icon has-text-primary is-hidden-tablet ok-main-btn-filter" style="margin-bottom: 0.5em;"><i
                class="fas fa-lg fa-<?= $btnFilter ?> ok-main-icon-filter"></i></span>
    <div class="is-clearfix"></div>
    <div class="columns is-multiline">
        <aside class="column is-one-fifth ok-main-filter<?= $btnFilter == 'filter' ? ' is-hidden-mobile' : '' ?>">
            <?php if ($filterDelete): ?>
                <div class="field is-grouped is-grouped-multiline">
                    <div class="control">
                        <div class="tags has-addons">
                            <a class="tag is-primary has-text-weight-bold"
                               href="<?= $filterDeleteClearHref; ?>"><?= Yii::t('app', 'Очистить') ?></a>
                            <a class="tag is-delete" href="<?= $filterDeleteClearHref; ?>"></a>
                        </div>
                    </div>
                    <?php foreach ($filterDelete as $name => $href): ?>
                        <div class="control">
                            <div class="tags has-addons">
                                <a class="tag is-primary" href="<?= $href; ?>"><?= $name; ?></a>
                                <a class="tag is-delete" href="<?= $href; ?>"></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php foreach ($filter as $agd): ?>
                <ul class="subtitle"><?= $agd['name'] ?>
                    <?php foreach ($agd['attrs'] as $ad): ?>
                        <li class="field">
                            <a href="<?= $ad['href'] ?>"<?= $ad['rel'] ?>>
                                <input class="is-checkradio" type="checkbox"<?= $ad['checked'] ?>>
                                <label><?= $ad['name'] ?></label>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endforeach; ?>
        </aside>

        <div class="column">
            <aside class="level has-background-white-bis">
                <div class="level-left"></div>
                <div class="level-right">
                    <div class="level-item">
                        <div class="dropdown" style="margin: 0.4em 0;">
                            <div class="dropdown-trigger">
                                <button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
                                    <span><?= Yii::t('app', $sortName) ?></span>
                                    <span class="icon is-small"><i class="fas fa-angle-down"
                                                                   aria-hidden="true"></i></span>
                                </button>
                            </div>
                            <div class="dropdown-menu" id="dropdown-menu" role="menu">
                                <div class="dropdown-content">
                                    <a href="<?= urldecode(Url::current(['sort' => 'rating', 'page' => null])) ?>"
                                       class="dropdown-item<?= $sortName === 'По рейтингу' ? ' is-active' : '' ?>"><?= Yii::t('app', 'По рейтингу') ?></a>
                                    <a href="<?= urldecode(Url::current(['sort' => 'cheap', 'page' => null])) ?>"
                                       class="dropdown-item<?= $sortName === 'По возрастанию цены' ? ' is-active' : '' ?>"><?= Yii::t('app', 'По возрастанию цены') ?></a>
                                    <a href="<?= urldecode(Url::current(['sort' => 'expensive', 'page' => null])) ?>"
                                       class="dropdown-item<?= $sortName === 'По убыванию цены' ? ' is-active' : '' ?>"><?= Yii::t('app', 'По убыванию цены') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>

            <?php if ($isNoStandart && !empty($products)): ?>
                <p><?= Yii::t('app', 'Цена матраса нестандартного размера (цена за 1 кв.м. указана в товаре как "Нестандарт") расчитывается:') ?></p>
                <ul>
                    <li>-
                        <code class="has-text-link"><?= Yii::t('app', 'длина (м) х ширина (м) х цена 1 кв.м.') ?></code>
                    </li>
                    <li>- <?= Yii::t('app', 'ширина меньше 0.6 м и длина меньше 1.2 м:') ?> <code
                                class="has-text-link"><?= Yii::t('app', '1.2 х длина (м) х ширина (м) х цена 1 кв.м.') ?></code>
                    </li>
                    <li>- <?= Yii::t('app', 'круглый матрас') ?>: <code
                                class="has-text-link"><?= Yii::t('app', '3.14 х радиус(м) х радиус (м) х цена 1 кв.м.') ?></code>
                    </li>
                    <li>- <?= Yii::t('app', 'матрас любой формы') ?>: <code
                                class="has-text-link"><?= Yii::t('app', '1.2 х площадь (кв.м.) х цена 1 кв.м.') ?></code>
                    </li>
                </ul>
                <p><?= Yii::t('app', 'Заказ матраса нестандартного размера возможен только производства ЕММ.') ?></p>
                <br>
            <?php endif; ?>

            <?php if (!empty($products)): ?>
                <section class="columns is-multiline">
                    <?php foreach ($products as $item): ?>
                        <?php $stickerDiscount = 0;
                        $hasFreeShipping = false; ?>
                        <article class="box column is-one-third-tablet has-background-white-bis">
                            <div class="ok-block-tag">
                                <?php if (!empty($item->productStickers)): ?>
                                    <?php foreach ($item->productStickers as $productSticker): ?>
                                        <?php
                                        if (substr($productSticker->sticker->name, 0, 1) == '-') {
                                            $pos = strpos($productSticker->sticker->name, '%');
                                            $stickerDiscount = intval(substr($productSticker->sticker->name, 1, $pos - 1));
                                        };
                                        if ($productSticker->sticker->name == 'Бесплатная доставка') {
                                            $hasFreeShipping = true;
                                        }
                                        ?>
                                        <p class="field ok-tag-field"><span
                                                    class="tag <?= $productSticker->sticker->class ?> has-text-weight-bold ok-tag"><?= Yii::t('app', $productSticker->sticker->name) ?></span>
                                        </p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <?php if ((!$isSized ? $item->price : $item->productOption->value) > 5000 && !$hasFreeShipping): ?>
                                    <p class="field ok-tag-field"><span
                                                class="tag is-danger has-text-weight-bold ok-tag"><?= Yii::t('app', 'Бесплатная доставка') ?></span>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <?php $params = ['product/index', 'product_id' => $item->product_id, 's1' => $item->seo_url, 's2' => 'p']; ?>
                            <?php if ($isSized) $params['s'] = $attributeId; ?>
                            <a href="<?= Url::to($params) ?>"
                               title="<?= $item->productDescription->name ?>">
                                <figure class="image ok-content-figure">
                                    <?= Html::img("/{$item->getImage()->getPath('650x650')}", ['alt' => $item->productDescription->name]) ?>
                                </figure>
                            </a>
                            <div class="has-text-centered"><a
                                        href="<?= Url::to($params) ?>"
                                        title="<?= $item->productDescription->name ?>"
                                        class="has-text-grey-dark ok-carousel-name"><?= $item->productDescription->name ?></a>
                            </div>
                            <?php if (array_key_exists($item->product_id, $reviewsStars)): ?>
                                <div class="product_reviews">
                                    <a href="<?= Url::to(array_merge($params, ['#' => 'reviews'])) ?>">
                                        <p class="has-text-centered">
                                            <?php foreach ($reviewsStars[$item->product_id]['rates'] as $r): ?>
                                                <span class="icon is-small has-text-primary"><i
                                                            class="<?= $r ?>"></i>
                                        </span>
                                            <?php endforeach; ?>
                                            <span><?= $reviewsStars[$item->product_id]['count'] ?></span>
                                            <?php if ($reviews[$item->product_id]['reviewsHasImages']): ?>
                                                <span class="icon is-small has-text-primary"
                                                      style="margin-left: 0.3em;"><i
                                                            class="fas fa-lg fa-camera"></i></span>
                                            <?php endif; ?>
                                        </p>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <div class="has-text-centered">
                                    <span class="has-text-grey-darker has-text-weight-bold is-size-5">
                                        <?= Yii::$app->formatter->asInteger(!$isSized ? $item->price : $item->productOption->value) ?> <?= Yii::t('app', 'грн.') ?></span>
                                <?php
                                if ($stickerDiscount != 0) {
                                    echo '<span style="text-decoration: line-through;" class="has-text-grey">' . Yii::$app->formatter->asInteger((!$isSized ? $item->price : $item->productOption->value) / ((100 - $stickerDiscount) / 100)) . Yii::t('app', 'грн.') . '</span>';
                                }
                                ?>
                                <?= $isSized ? "<span class='is-size-7'> ({$size})</span>" : '' ?>
                            </div>
                            <div class="has-text-centered">
                                <a class="button is-warning ok-product-compare"
                                   title="<?= Yii::t('app', 'Добавить в сравнение') ?>"><span class="icon"
                                                                                              data-product-id=<?= $item->product_id; ?>><i
                                                class="fas fa-balance-scale"
                                                style="cursor: pointer;"></i></span></a>
                                <a class="button is-warning ok-product-favorite"
                                   title="<?= Yii::t('app', 'Добавить в избранное') ?>"><span
                                            class="icon"
                                            data-product-id=<?= $item->product_id; ?>><i
                                                class="fas fa-heart" style="cursor: pointer;"></i></span>
                                </a>
                            </div>
                        </article>
                        <div class="clearfix"></div>
                    <?php endforeach; ?>
                </section>

                <?php if ($pages->totalCount > 9): ?>
                    <aside class="level has-background-white-bis">
                        <div class="level-left"></div>
                        <div class="level-right">
                            <div class="level-item">
                                <?= \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages,
                                    'prevPageLabel' => '<',
                                    'nextPageLabel' => '>',
                                    'maxButtonCount' => 5,
                                    'disableCurrentPageButton' => true,
                                    'options' => [
                                        'class' => 'pagination',
                                        'id' => 'pagination',
                                    ]
                                ]); ?>
                            </div>
                        </div>
                    </aside>
                <?php endif; ?>

            <?php else: ?>
                <p><?= Yii::t('app', 'Нет данных по выбранному фильтру. Попробуйте отключить некоторые фильтры!') ?></p>
            <?php endif; ?>

        </div>
    </div>
</section>

<?php if ($text_description): ?>
    <?= $text_description ?>
<?php endif; ?>





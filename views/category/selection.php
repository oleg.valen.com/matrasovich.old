<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content selection">
    <h1 class="title is-size-4">Подбор матраса по параметрам</h1>
    <?php $form = ActiveForm::begin(); ?>
    <div class="field">
        <div class="control">
            <?= $form->field($modelSa, 'categoryId', [
                'template' => "{label}\n<div class=\"select is-primary is-medium\" style='display: grid'>{input}</div>\n{hint}\n{error}",
            ])->dropDownList($categories)->label(false); ?>
        </div>
    </div>
    <?php foreach ($modelsAdContent as $agdName => $ads): ?>
        <p class="has-text-weight-bold"><?= $agdName; ?></p>
        <?php if (strcmp($agdName, 'Размер') !== 0): ?>
            <?php foreach ($ads as $index => $ad): ?>
                <?php echo $form->field($modelsAd[$index], "[$index]value")->checkbox([
                    'label' => $ad['name'],
                    'template' => '<div class="switch is-small">{input}{beginLabel}<span class="is-size-6">{labelTitle}</span>{endLabel}{error}{hint}</div>',
                ]);
                ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="size">
                <?php foreach ($ads as $index => $ad): ?>
                    <?php echo $form->field($modelsAd[$index], "[$index]value")->radio([
                        'label' => $ad['name'],
                        'template' => "<div class=\"is-checkradio\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",
                    ]); ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <hr>
    <?php endforeach; ?>
    <div class="sort">
        <?php echo $form->field($modelSa, "sortAsc")->radio([
            'label' => 'По возрастанию цены',
            'template' => "<div class=\"is-checkradio\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",
        ]); ?>
        <?php echo $form->field($modelSa, "sortDesc")->radio([
            'label' => 'По убыванию цены',
            'template' => "<div class=\"is-checkradio\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>",
        ]); ?>
    </div>
    <hr>
    <div class="field">
        <div class="control">
            <?= Html::submitButton('Найти', ['class' => 'selection button is-warning is-medium has-text-weight-bold']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</section>

<?php if ($isPost && !$categoryIsChanged): ?>
    <section class="section container content">
        <?php if (!empty($products)): ?>
            <div class="columns is-multiline">
                <?php foreach ($products as $item): ?>
                    <?php $stickerDiscount = 0;
                    $hasFreeShipping = false; ?>
                    <div class="column is-one-quarter-desktop is-one-third-tablet">
                        <div class="ok-block-tag">
                            <?php if (!empty($item->productStickers)): ?>
                                <?php foreach ($item->productStickers as $productSticker): ?>
                                    <?php
                                    if (substr($productSticker->sticker->name, 0, 1) == '-') {
                                        $pos = strpos($productSticker->sticker->name, '%');
                                        $stickerDiscount = intval(substr($productSticker->sticker->name, 1, $pos - 1));
                                    };
                                    if ($productSticker->sticker->name == 'Бесплатная доставка') {
                                        $hasFreeShipping = true;
                                    }
                                    ?>
                                    <p class="field ok-tag-field"><span
                                                class="tag <?= $productSticker->sticker->class ?> is-light ok-tag"><?= $productSticker->sticker->name ?></span>
                                    </p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if ($item->productOption->value > 5000 && !$hasFreeShipping): ?>
                                <p class="field ok-tag-field"><span
                                            class="tag is-danger is-light ok-tag">Бесплатная доставка</span>
                                </p>
                            <?php endif; ?>
                        </div>
                        <a href="<?= Url::to("/{$item->seo_url}/p{$item->product_id}?s={$attributeId}") ?>"
                           title="<?= $item['name'] ?>">
                            <figure class="image ok-content-figure">
                                <?= Html::img("/{$item->getImage()->getPath('300x300')}", ['alt' => $item['name']]) ?>
                            </figure>
                        </a>
                        <p class="has-text-centered"><a
                                    href="<?= Url::to("/{$item->seo_url}/p{$item->product_id}?s={$attributeId}") ?>"
                                    title="<?= $item->name ?>"
                                    class="has-text-grey-dark ok-carousel-name"><?= $item->name ?></a>
                        </p>
                        <p class="has-text-centered">
                                    <span class="has-text-grey-darker has-text-weight-bold is-size-5">
                                        <?= Yii::$app->formatter->asInteger($item->productOption->value) ?> грн.</span>
                            <?php
                            if ($stickerDiscount != 0) {
                                echo '<span style="text-decoration: line-through;" class="has-text-grey">' . Yii::$app->formatter->asInteger($item->productOption->value / ((100 - $stickerDiscount) / 100)) . ' грн. </span>';
                            }
                            ?>
                        </p>
                        <div class="has-text-centered">
                            <a class="button is-warning has-text-weight-bold is-medium ok-main-btn-call-us"
                               title="Быстрая покупка"
                               data-name="<?= $item->name ?>">Купить</a>
                        </div>
                        <hr>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <p>Нет данных по выбранному фильтру. Попробуйте отключить некоторые фильтры!</p>
        <?php endif; ?>
    </section>
<?php endif; ?>

<div class="section modal ok-main-modal-call-us">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Обратный звонок</p>
            <button class="delete ok-main-modal-delete" aria-label="close"></button>
        </header>
        <?php $form = ActiveForm::begin(['id' => 'call-us-form',]); ?>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Задайте вопрос</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <?= $form->field($modelCallUsForm, 'writeUs')->textarea(['autofocus' => true, 'placeholder' => 'Напишите свой вопрос', 'class' => 'input is-primary ok-main-modal-text',])->label(false) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Как с Вами связаться</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <?= $form->field($modelCallUsForm, 'telephone')->textInput(['autofocus' => true, 'placeholder' => 'Телефон', 'class' => 'input is-primary ok-main-modal-phone'])->label(false) ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <div class="control">
                <button class="button is-primary ok-main-btn-modal-send">Отправить</button>
            </div>
        </footer>
        <?php ActiveForm::end(); ?>
        <div class="container notification is-warning">
            <button class="delete"></button>
            <p class="thank-you">Спасибо за заказ!</p>
            <p>В течении 5 минут с вами свяжется наш менеджер!</p>
        </div>
    </div>
</div>
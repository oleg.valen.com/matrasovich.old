<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/magiczoomplus.js', ['depends' => [AppAsset::class]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/magiczoomplus.css', ['depends' => [AppAsset::class]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/magiczoomplus.module.css', ['depends' => [AppAsset::class]]);
$this->registerJsFile('https://ppcalc.privatbank.ua/pp_calculator/resources/js/calculator.js', ['depends' => [AppAsset::class]]);
$this->registerMetaTag(['name' => 'description', 'content' => $product->productDescription->description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $product->productDescription->keyword]);
$this->registerMetaTag(['name' => 'robots', 'content' => $robots]);
$this->title = $product->productDescription->title;
$formatter = Yii::$app->formatter;
Yii::$app->params['ogprefix'] = ' prefix="og: http://ogp.me/ns#
 fb: http://ogp.me/ns/fb#
 product: http://ogp.me/ns/product#"';
Yii::$app->params['opengraph'] = $opengraph;
Yii::$app->params['jsonld'] = true;
?>
<div id="breadcrumbs">
    <div class="container is-size-7-mobile is-size-6-tablet">
        <?= $breadcrumbs ?>
    </div>
</div>

<section class="section container content">
    <header>
        <h1 class="title is-size-4 has-text-centered-mobile product_h1"><?= $product->productDescription->h1 ?></h1>
        <?php if ($reviewsStars): ?>
            <div class="product_reviews">
                <a href="<?= Url::to([
                    'product/index',
                    'product_id' => $product->product_id,
                    's1' => $product->seo_url,
                    's2' => 'p',
                    '#' => 'reviews']) ?>">
                    <p class="has-text-centered-mobile">
                        <?php foreach ($reviewsStars[$product->product_id]['rates'] as $r): ?>
                            <span class="icon is-small has-text-primary"><i
                                        class="<?= $r ?>"></i>
                                        </span>
                        <?php endforeach; ?>
                        <span><?= $reviewsStars[$product->product_id]['count'] ?></span>
                        <?php if ($reviewsHasImages): ?>
                            <span class="icon is-small has-text-primary" style="margin-left: 0.3em;"><i
                                        class="fas fa-lg fa-camera"></i></span>
                        <?php endif; ?>
                    </p>
                </a>
            </div>
        <?php endif; ?>
    </header>
    <br>
    <div class="columns">
        <div class="column is-three-quarters">
            <div class="product-img-box">
                <!-- Begin magiczoomplus -->
                <div class="MagicToolboxContainer selectorsBottom minWidth">
                    <?php $hasFreeShipping = false; ?>
                    <div class="magic-slide mt-active" data-magic-slide="zoom">
                        <a id="MagicZoomPlusImage38950"
                           class="MagicZoom"
                           href="<?= "/{$product->getImage()->getPathToOrigin()}" ?>"
                           title="<?= $product->name ?>"
                           data-options="zoomWidth:600;zoomHeight:600;selectorTrigger:hover;lazyZoom:true;rightClick:true;zoomMode:preview;expandCaption:false;cssClass:white-bg;hint:off;"
                           data-mobile-options="zoomWidth:auto;zoomHeight:auto;lazyZoom:false;zoomMode:zoom;cssClass:;hint:off;textHoverZoomHint:Touch to zoom;textClickZoomHint:<?= Yii::t('app', 'Дважды нажмите, чтобы увеличить') ?>;textExpandHint:Tap to expand;">
                            <?php if (!empty($product->productStickers)): ?>
                                <div class="ok-block-tag">
                                    <?php foreach ($product->productStickers as $productSticker): ?>
                                        <p class="field ok-tag-field"><span
                                                    class="tag <?= $productSticker->sticker->class ?> has-text-weight-bold ok-tag"><?= Yii::t('app', $productSticker->sticker->name) ?></span>
                                        </p>
                                        <?php
                                        if ($productSticker->sticker->name == Yii::t('app', 'Бесплатная доставка')) {
                                            $hasFreeShipping = true;
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                            <img itemprop="image"
                                 src="<?= "/{$product->getImage()->getPath('650x650')}" ?>"
                                 alt="<?= $product->name ?>"/>
                        </a>
                    </div>
                    <div class="magic-slide" data-magic-slide="360"></div>
                    <div class="MagicToolboxSelectorsContainer">
                        <div id="MagicToolboxSelectors38950" class="">
                            <?php foreach ($product->getImages() as $img): ?>
                                <a data-magic-slide-id="zoom"
                                   data-zoom-id="MagicZoomPlusImage38950"
                                   href="<?= "/{$img->getPathToOrigin()}" ?>"
                                   data-image="<?= "/{$img->getPath('650x650')}" ?>"
                                   title="<?= $product->name ?>"><img
                                            src="<?= "/{$img->getPath('65x65')}" ?>"
                                            alt="<?= $product->name ?>"/></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (!empty($detailAttributes)): ?>
                <br>
                <table class="is-fullwidth is-hoverable is-striped table">
                    <thead>
                    <tr>
                        <th colspan="1"><?= Yii::t('app', 'Характеристика') ?></th>
                        <th colspan="1"><?= Yii::t('app', 'Значение') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($detailAttributes as $da): ?>
                        <tr>
                            <td><?= $da['detailAttributeDescriptions'][0]['key']; ?></td>
                            <td>
                                <?php if (key_exists('href', $da)): ?>
                                    <a class="has-text-link"
                                       href="<?= $da['href']; ?>"><?= $da['detailAttributeDescriptions'][0]['value']; ?></a>
                                <?php else: ?>
                                    <?= $da['detailAttributeDescriptions'][0]['value']; ?>
                                <?php endif; ?>
                                <?php if (key_exists('hasDetailed', $da)): ?>
                                    <span
                                            class="icon has-text-primary product-icon-info"
                                            data-product-id=<?= $product->product_id; ?>
                                            data-detail-id=<?= $da['detail_id']; ?>><i
                                                class="fas fa-lg fa-info-circle" style="cursor: pointer;"></i>
                                    </span>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
        <div class="column">
            <div class="has-text-centered-mobile">
                <p>
            <span class="title has-text-grey-darker ok-product-price"><?= $formatter->asInteger($price) ?>
                грн.</span>
                    <?php if ($stickerDiscount): ?>
                        <span style="text-decoration: line-through;"
                              class="has-text-grey is-size-4 ok-product-old-price"
                              data-sticker-discount="<?= $stickerDiscount ?>"><?= $oldSum; ?> грн.</span>
                    <?php endif; ?>
                </p>
                <div class="field">
                    <div class="control has-text-centered-mobile">
                        <div class="select is-primary is-medium">
                            <select id="size-option">
                                <?php foreach ($options as $option): ?>
                                    <option value="<?= $option['attribute_id'] ?>"
                                            data-price="<?= $formatter->asInteger($option['value']) ?> грн."
                                            data-delivery-warehouse="<?= $option['delivery']['warehouse'] ?>"
                                            data-delivery-doors="<?= $option['delivery']['doors'] ?>">
                                        <?php echo $option['name'] . ' (' . $formatter->asInteger($option['value']) . ' ' . Yii::t('app', 'грн.') . ')' ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="has-text-centered-mobile">
                <a class="button is-warning is-large has-text-weight-bold ok-product-cart"
                   title="<?= Yii::t('app', 'Купить') ?>"
                   data-product-id=<?= $product->product_id; ?>><?= Yii::t('app', 'Купить') ?></a>
                <a class="button is-warning is-large ok-product-compare"
                   title="<?= Yii::t('app', 'Добавить в сравнение') ?>"><span
                            class="icon"
                            data-product-id=<?= $product->product_id; ?>><i
                                class="fas fa-balance-scale" style="cursor: pointer;"></i></span>
                </a>
                <a class="button is-warning is-large ok-product-favorite"
                   title="<?= Yii::t('app', 'Добавить в избранное') ?>"><span
                            class="icon"
                            data-product-id=<?= $product->product_id; ?>><i
                                class="fas fa-heart" style="cursor: pointer;"></i></span>
                </a>
            </div>
            <hr>
            <div class="product_right__extra">
                <div><span class="icon is-large has-text-primary"><i
                                class="fas fa-lg fa-shipping-fast"></i></span>
                    <a href="<?= Url::to(['site/dostavka-i-oplata']) ?>">
                        <span class="ok-underline-dotted"><?= Yii::t('app', 'Стоимость доставки') ?></span></a>
                </div>
                <div>на отделение (грн): <span
                            class="has-text-danger has-text-weight-bold delivery-warehouse is-size-2"><?= $delivery['warehouse'] ?></span>
                </div>
                <div>до дверей (грн): <span
                            class="has-text-danger has-text-weight-bold delivery-doors is-size-2"><?= $delivery['doors'] ?></span>
                </div>
                <div class="ok-level-item"><span class="icon is-large has-text-primary"><i
                                class="fas fa-lg fa-certificate"></i></span>
                    <a href="<?= Url::to(['site/about-us']) ?>">
                        <span class="ok-underline-dotted"><?= Yii::t('app', 'Сертификаты качества') ?></span></a>
                </div>
                <div class="ok-level-item"><span class="icon is-large has-text-primary"><i
                                class="fas fa-lg fa-thumbs-up"></i></span>
                    <a href="<?= Url::to(['site/kontrol-kachestva']) ?>">
                        <span class="ok-underline-dotted"><?= Yii::t('app', 'Контроль качества') ?></span></a>
                </div>
                <div class="ok-level-item"><span class="icon is-large has-text-primary"><i
                                class="fas fa-lg fa-user-shield"></i></span>
                    <a href="<?= Url::to(['site/garantiya-i-vozvrat']) ?>">
                        <span class="ok-underline-dotted"><?= Yii::t('app', 'Гарантия возврата') ?></span></a>
                </div>
                <div class="ok-level-item"><span class="icon is-large has-text-primary"><i
                                class="fas fa-lg fa-layer-group"></i></span>
                    <a class="ok-product-nostandart">
                        <span class="ok-underline-dotted"><?= Yii::t('app', 'Нет нужного размера или формы?') ?></span></a>
                </div>
            </div>
            <hr>
        </div>
    </div>
</section>

<?= Html::decode($product->productDescription->text_description) ?>

<?php if (!empty($productSimilar) && count($productSimilar['products']) > 1): ?>
    <section class="section container content">
        <h2 class="title is-size-5"><?= Yii::t('app', 'Похожие товары') ?></h2>
        <div class="table-container">
            <table class="is-fullwidth is-hoverable is-striped table is-size-7">
                <thead>
                <tr>
                    <th></th>
                    <?php foreach ($productSimilar['products'] as $item): ?>
                        <th>
                            <?php if ($item->product_id == $product->product_id): ?>
                                <img src="<?= "/{$item->getImage()->getPath('250x250')}" ?>"
                                     alt="<?= $item->productDescription->name ?>"/>
                            <?php else: ?>
                                <?php $params = ['product/index', 'product_id' => $item->product_id, 's1' => $item->seo_url, 's2' => 'p']; ?>
                                <?php if ($isSized) $params['s'] = $attributeId; ?>
                                <a href="<?= Url::to($params) ?>"
                                   title="<?= $item->productDescription->name ?>"><img
                                            src="<?= "/{$item->getImage()->getPath('250x250')}" ?>"
                                            alt="<?= $item->productDescription->name ?>"/></a>
                            <?php endif; ?>
                        </th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th><?= Yii::t('app', 'Наименование') ?></th>
                    <?php foreach ($productSimilar['products'] as $item): ?>
                        <td>
                            <?php if ($item->product_id == $product->product_id): ?>
                                <span class="has-text-grey"><?= $item->productDescription->name ?></span>
                            <?php else: ?>
                                <a class="has-text-link"
                                    <?php $params = ['product/index', 'product_id' => $item->product_id, 's1' => $item->seo_url, 's2' => 'p']; ?>
                                    <?php if ($isSized) $params['s'] = $attributeId; ?>
                                   href="<?= Url::to($params) ?>"
                                   title="<?= $item->productDescription->name ?>"><?= $item->productDescription->name ?></a>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <?php foreach ($productSimilar['details'] as $key => $details): ?>
                    <tr>
                        <th><?= $key; ?></th>
                        <?php foreach ($details as $id => $detail): ?>
                            <td<?= $id == $product->product_id ? ' class = "has-text-grey"' : ''; ?>>
                                <?php
                                end($detail);
                                $lastElementKey = key($detail);
                                foreach ($detail as $k => $v) {
                                    echo $v;
                                    if ($k !== $lastElementKey) {
                                        echo '<br>';
                                    }
                                }
                                ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
<?php endif; ?>

<section class="section container content" id="reviews">
    <h2 class="title is-size-5"><?= Yii::t('app', 'Отзывы') ?></h2>
    <?php foreach ($reviews as $r): ?>
        <article>
            <div class="columns">
                <div class="column is-2">
                    <strong><?= Html::encode($r['review']->name); ?></strong>, <?= date('d.m.Y', $r['review']->created_at); ?>
                    <br>
                    <?php foreach ($r['rates'] as $rate): ?>
                        <span class="icon is-small has-text-primary"><i
                                    class="<?= $rate ?>"></i>
                                        </span>
                    <?php endforeach; ?>
                </div>
                <?php if ($r['images']): ?>
                    <div class="column">
                        <?php foreach ($r['images'] as $image): ?>
                            <figure class="image is-64x64 product_review__image"
                                    data-review-id="<?= $r['review_id'] ?>"><?= Html::img("/{$image->getPath('64x64')}"); ?></figure>
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <div class="column"><?= Html::encode($r['review']->text); ?></div>
                <?php endif; ?>
            </div>
            <?php if ($r['images']): ?>
                <div><?= Html::encode($r['review']->text); ?></div>
            <?php endif; ?>
        </article>
        <hr>
    <?php endforeach; ?>
    <button class="button is-primary product_review-give-review">
        <?= Yii::t('app', 'Оставить отзыв') ?>
    </button>
</section>

<div class="section modal product-info-modal" style="overflow: scroll;">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title"></p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
        </section>
    </div>
</div>

<div class="section modal product-nostandart-modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title"><?= Yii::t('app', 'Расчет нестандартного размера') ?></p>
            <button class="delete ok-main-modal-delete" aria-label="close"></button>
        </header>
        <?php $form = ActiveForm::begin(['id' => 'nostandart-form',]); ?>
        <section class="modal-card-body">
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label"><?= Yii::t('app', 'Цена за 1 кв.м.') ?></label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <?= $form->field($modelNostandartForm, 'price')
                                ->textInput(['id' => 'price', 'type' => 'number', 'placeholder' => 'Цена за 1 кв.м.', 'class' => 'input is-primary', 'disabled' => true,])
                                ->label(false); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label"><?= Yii::t('app', 'Ширина (см)') ?></label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <?= $form->field($modelNostandartForm, 'width')
                                ->textInput(['autofocus' => true, 'id' => 'width', 'type' => 'number', 'placeholder' => Yii::t('app', 'Ширина'), 'class' => 'input is-primary'])
                                ->label(false); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label"><?= Yii::t('app', 'Длина (см)') ?></label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <?= $form->field($modelNostandartForm, 'length')
                                ->textInput(['id' => 'length', 'type' => 'number', 'placeholder' => Yii::t('app', 'Длина'), 'class' => 'input is-primary'])
                                ->label(false); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Цена</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <?= $form->field($modelNostandartForm, 'total')
                                ->textInput(['id' => 'total', 'type' => 'number', 'placeholder' => Yii::t('app', 'Цена'), 'class' => 'input is-primary', 'disabled' => true,])
                                ->label(false); ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <ul>
                <?= Yii::t('app', 'Также изготавливаем') ?>:
                <li>- <?= Yii::t('app', 'матрасы круглой формы') ?>: <code
                            class="has-text-link"><?= Yii::t('app', '3.14 х радиус(м) х радиус (м) х цена 1 кв.м.') ?></code>
                </li>
                <li>- <?= Yii::t('app', 'матрасы произвольной формы') ?>: <code
                            class="has-text-link"><?= Yii::t('app', '1.2 х площадь (кв.м.) х цена 1 кв.м.') ?></code>
                </li>
            </ul>
        </footer>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="section modal product_review-modal<?= $reviewIsActive ?>">
    <div class="modal-background"></div>
    <div class="modal-card" style="overflow: scroll;">
        <header class="modal-card-head">
            <p class="modal-card-title"><?= Yii::t('app', 'Оставьте, пожайлуйста, отзыв') ?></p>
            <button class="delete ok-main-modal-delete" aria-label="close"></button>
        </header>
        <?php $form = ActiveForm::begin([
            'id' => 'review-form',
//            'enableAjaxValidation' => true,
        ]); ?>
        <section class="modal-card-body">
            <?= $form->field($modelReview, 'product_id')
                ->textInput([
                    'type' => 'hidden',
                    'value' => $product->product_id,
                ])
                ->label(false); ?>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label"><?= Yii::t('app', 'Имя') ?></label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <?= $form->field($modelReview, 'name', [
                            ])
                                ->textInput([
                                    'autofocus' => true,
                                    'placeholder' => Yii::t('app', 'Имя'),
                                    'class' => 'input is-primary',

                                ])
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label"><?= Yii::t('app', 'Отзыв') ?></label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <?= $form->field($modelReview, 'text')
                                ->textarea([
                                    'placeholder' => Yii::t('app', 'Отзыв'),
                                    'class' => 'input is-primary input-textarea',
                                    'rows' => 6,
                                ])
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Оценка</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <?= $form->field($modelReview, 'rate')
                                ->textInput([
                                    'placeholder' => 'От 1 до 5',
                                    'class' => 'input is-primary',
                                    'type' => 'number',
                                ])
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="has-text-primary has-text-weight-bold">Для получения кешбэка 50-100 грн заполните телефон и фото
                (два и более). Условия <span><a
                            class="has-text-primary has-text-weight-bold ok-underline-dotted"
                            href="<?= Url::toRoute('site/cashback-for-review') ?>">здесь</a></span>.
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Телефон</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <?= $form->field($modelReview, 'phone')
                                ->textInput([
                                    'placeholder' => 'Телефон',
                                    'class' => 'input is-primary',
                                ])
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Фото</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control">
                            <?= $form->field($modelReview, 'imageFiles[]')
                                ->fileInput([
                                    'multiple' => true, 'accept' => 'image/*',
                                ])
                                ->label(false);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <button class="button is-primary product_review__send" type="submit">Отправить</button>
        </section>
        <footer class="modal-card-foot">
            <p><?= Yii::t('app', 'Отзыв добавляется не сразу, а после модерации') ?></p>
        </footer>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php

use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <h1 class="title">Корзина</h1>
    <div class="table-container"></div>
    <table class="is-fullwidth is-hoverable is-striped table">
        <thead>
        <tr>
            <th colspan="1">Характеристика</th>
            <th colspan="1">Значение</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Серия</td>
            <td>
                <a class="has-text-link" href="/series-take-go-bamboo/s6">Take&amp;Go Bamboo</a>
                <span class="icon has-text-primary product-icon-info" data-product-id="72" data-detail-id="6"><i
                            class="" style="cursor: pointer;" data-fa-i2svg=""><svg
                                class="svg-inline--fa fa-info-circle fa-w-16 fa-lg" aria-hidden="true" focusable="false"
                                data-prefix="fas" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                             d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg></i>
                                    </span>
            </td>
        </tr>
        <tr>
            <td>Высота</td>
            <td>
                5 см
            </td>
        </tr>
        <tr>
            <td>Жесткость</td>
            <td>
                Средний
            </td>
        </tr>
        <tr>
            <td>Чехол</td>
            <td>
                Aero чехол с бамбуком <span class="icon has-text-primary product-icon-info" data-product-id="72"
                                            data-detail-id="48"><i class="" style="cursor: pointer;" data-fa-i2svg=""><svg
                                class="svg-inline--fa fa-info-circle fa-w-16 fa-lg" aria-hidden="true" focusable="false"
                                data-prefix="fas" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                             d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg></i>
                                    </span>
            </td>
        </tr>
        <tr>
            <td>Наполнитель</td>
            <td>
                Комфортная пена Effect 5D <span class="icon has-text-primary product-icon-info" data-product-id="72"
                                                data-detail-id="73"><i class="" style="cursor: pointer;"
                                                                       data-fa-i2svg=""><svg
                                class="svg-inline--fa fa-info-circle fa-w-16 fa-lg" aria-hidden="true" focusable="false"
                                data-prefix="fas" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                             d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg></i>
                                    </span>
            </td>
        </tr>
        <tr>
            <td>Упаковка</td>
            <td>
                Матрас в вакуумной упаковке <span class="icon has-text-primary product-icon-info" data-product-id="72"
                                                  data-detail-id="111"><i class="" style="cursor: pointer;"
                                                                          data-fa-i2svg=""><svg
                                class="svg-inline--fa fa-info-circle fa-w-16 fa-lg" aria-hidden="true" focusable="false"
                                data-prefix="fas" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                             d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg></i>
                                    </span>
            </td>
        </tr>
        <tr>
            <td>Распаковка</td>
            <td>
                Инструкция по распаковке <span class="icon has-text-primary product-icon-info" data-product-id="72"
                                               data-detail-id="112"><i class="" style="cursor: pointer;"
                                                                       data-fa-i2svg=""><svg
                                class="svg-inline--fa fa-info-circle fa-w-16 fa-lg" aria-hidden="true" focusable="false"
                                data-prefix="fas" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                             d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg></i>
                                    </span>
            </td>
        </tr>
        <tr>
            <td>Гарантия</td>
            <td>
                18 месяцев
            </td>
        </tr>
        </tbody>
    </table>

</section>

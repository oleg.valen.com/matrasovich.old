<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\View;

/* @var $this yii\web\View */
if (!empty($robots)) {
    $this->registerMetaTag(['name' => 'robots', 'content' => $robots]);
}

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/magiczoomplus.js', ['depends' => [AppAsset::class]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/magiczoomplus.css', ['depends' => [AppAsset::class]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/magiczoomplus.module.css', ['depends' => [AppAsset::class]]);
?>

<div class="container content">
    <section class="section columns ok-content">
        <article class="column">
            <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=so-skidkoy'])) ?>"
               title="Скидки до 30% на ортопедические матрасы">
                <img src="<?= Url::to('@web/images/banner_discount.jpg') ?>"
                     alt="Скидки до 30% на ортопедические матрасы">
            </a>
        </article>
        <article class="column">
            <a href="<?= Url::to(['site/cashback-for-review']) ?>"
               title="Получите кешбэк 100 грн. за отзыв с фото">
                <img src="<?= Url::to('@web/images/banner_cashback.jpg') ?>"
                     alt="Получите кешбэк 100 грн. за отзыв с фото">
            </a>
        </article>
        <article class="column">
            <a href="<?= Url::to(['site/dostavka-i-oplata']) ?>"
               title="Бесплатная доставка топперов на диван транспортной компанией Meest Express">
                <img src="<?= Url::to('@web/images/banner_delivery.jpg') ?>"
                     alt="Бесплатная доставка топперов на диван транспортной компанией Meest Express">
            </a>
        </article>
    </section>
    <section class="section">
        <header class="subtitle is-size-5">Тонкие матрасы на диван</header>
        <div class="columns is-multiline is-mobile">
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Недорогие</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'category=nedorogie'])) ?>"
                   title="Недорогие тонкие матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[40]->getImage()->getPath('350x350')}" ?>"
                             alt="Недорогие тонкие матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Премиум</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'category=premium'])) ?>"
                   title="Премиум тонкие матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[52]->getImage()->getPath('350x350')}" ?>"
                             alt="Премиум тонкие матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из ортопедической пены</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'fill=ergoflex'])) ?>"
                   title="Тонкие матрасы из ортопедической пены Ergoflex">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[47]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкие матрасы из ортопедической пены Ergoflex"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из мемори</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'fill=memory-foam'])) ?>"
                   title="Тонкие матрасы из memory-foam">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[46]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкие матрасы из мемори"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из кокоса</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'fill=kokosovaya-koyra'])) ?>"
                   title="Тонкие матрасы из кокосовой койры">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[48]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкие матрасы из кокосовой койры"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Мягкие</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'zhestkost=myagkiy'])) ?>"
                   title="Мягкие тонкие матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[53]->getImage()->getPath('350x350')}" ?>"
                             alt="Мягкие тонкие матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Средней жесткости</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'zhestkost=sredniy'])) ?>"
                   title="Тонкие матрасы средней жесткости">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[50]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкие матрасы средней жесткости"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Жесткие</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'zhestkost=zhestkiy'])) ?>"
                   title="Жесткие тонкие матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[41]->getImage()->getPath('350x350')}" ?>"
                             alt="Жесткие тонкие матрасы"/>
                    </figure>
                </a>
            </article>
            <div class="is-clearfix"></div>
        </div>
    </section>
    <section class="section">
        <header class="subtitle is-size-5">Ортопедические матрасы</header>
        <div class="columns is-multiline is-mobile">
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Со скидкой</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=so-skidkoy'])) ?>"
                   title="Ортопедические матрасы со скидкой">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[59]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедические матрасы со скидкой"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Недорогие</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=nedorogie'])) ?>"
                   title="Недорогие ортопедические матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[54]->getImage()->getPath('350x350')}" ?>"
                             alt="Недорогие ортопедические матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Беспружинные</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=bespruzhinnyy'])) ?>"
                   title="Беспружинные ортопедические матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[69]->getImage()->getPath('350x350')}" ?>"
                             alt="Беспружинные ортопедические матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Пружинные</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=pruzhinnyy'])) ?>"
                   title="Пружинные ортопедические матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[86]->getImage()->getPath('350x350')}" ?>"
                             alt="Пружинные ортопедические матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из мемори</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=memory-foam'])) ?>"
                   title="Ортопедические матрасы из memory-foam">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[74]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедические матрасы из мемори"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из ортопедической пены</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=ergoflex'])) ?>"
                   title="Ортопедические матрасы из пены Ergoflex">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[75]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедические матрасы из пены Ergoflex"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из кокоса</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=kokosovaya-koyra'])) ?>"
                   title="Ортопедические матрасы из кокосовой койры">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[67]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедические матрасы из кокосовой койры"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Из латекса</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=latex'])) ?>"
                   title="Ортопедические матрасы из латекса">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[81]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедические матрасы из латекса"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Мягкие</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'zhestkost=myagkiy'])) ?>"
                   title="Мягкие ортопедические матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[54]->getImage()->getPath('350x350')}" ?>"
                             alt="Мягкие ортопедические матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Жесткие</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'zhestkost=zhestkiy'])) ?>"
                   title="Жесткие ортопедические матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[76]->getImage()->getPath('350x350')}" ?>"
                             alt="Жесткие ортопедические матрасы"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Для гостиниц</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=dlya-gostinic'])) ?>"
                   title="Ортопедические матрасы для гостиниц">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[139]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедические матрасы для гостиниц"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet box has-background-white-ter">
                <p class="ok-content-p">Детские</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=detskie'])) ?>"
                   title="Детские ортопедические матрасы">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[144]->getImage()->getPath('350x350')}" ?>"
                             alt="Детские ортопедические матрасы"/>
                    </figure>
                </a>
            </article>
            <div class="is-clearfix"></div>
        </div>
    </section>

    <section class="is-hidden-mobile">
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6917.jpg') ?>"
                             alt="Боковина тонкого матраса"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6867.jpg') ?>"
                             alt="Чехол тонкого матраса"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6948.jpg') ?>"
                             alt="Аэро-чехол тонкого матраса"/>
                    </figure>
                </article>
            </div>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6951.jpg') ?>"
                             alt="Тонкий матрас в рулоне"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                             alt="Тонкий матрас - общий вид"/>
                    </figure>
                </article>
            </div>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child is-3">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6976.jpg') ?>"
                             alt="Тонкий матрас в шкаф-купе"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF7044.jpg') ?>"
                             alt="Тонкий матрас на резинках"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF7071.jpg') ?>"
                             alt="Тонкий матрас в багажнике авто"/>
                    </figure>
                </article>
            </div>
        </div>
    </section>

    <section class="section product-img-box is-hidden-tablet">
        <div class="MagicToolboxContainer selectorsBottom minWidth">
            <div class="magic-slide mt-active" data-magic-slide="zoom">
                <a id="MagicZoomPlusImage38950"
                   class="MagicZoom"
                   href="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                   title="Тонкий матрас - общий вид"
                   data-options="zoomWidth:600;zoomHeight:600;selectorTrigger:hover;lazyZoom:true;rightClick:true;zoomMode:preview;expandCaption:false;cssClass:white-bg;hint:off;"
                   data-mobile-options="zoomWidth:auto;zoomHeight:auto;lazyZoom:false;zoomMode:zoom;cssClass:;hint:off;textHoverZoomHint:Touch to zoom;textClickZoomHint:Дважды нажмите, чтобы увеличить;textExpandHint:Tap to expand;">
                    <img itemprop="image"
                         src="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                         alt="Тонкий матрас - общий вид"/>
                </a>
            </div>
            <div class="magic-slide" data-magic-slide="360"></div>
            <div class="MagicToolboxSelectorsContainer">
                <div id="MagicToolboxSelectors38950" class="">
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                       title="Тонкий матрас - общий вид"><img src="<?= Url::to('@web/images/DSCF7032_65.jpg') ?>"
                                                              alt="Тонкий матрас - общий вид"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6951.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6951.jpg') ?>" title="Тонкий матрас в рулоне"><img
                                src="<?= Url::to('@web/images/DSCF6951_65.jpg') ?>"
                                alt="Тонкий матрас в рулоне"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6867.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6867.jpg') ?>" title="Чехол тонкого матраса"><img
                                src="<?= Url::to('@web/images/DSCF6867_65.jpg') ?>"
                                alt="Чехол тонкого матраса"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF7044.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF7044.jpg') ?>"
                       title="Тонкий матрас на резинках"><img src="<?= Url::to('@web/images/DSCF7044_65.jpg') ?>"
                                                              alt="Тонкий матрас на резинках"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6948.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6948.jpg') ?>"
                       title="Аэро-чехол тонкого матраса"><img src="<?= Url::to('@web/images/DSCF6948_65.jpg') ?>"
                                                               alt="Аэро-чехол тонкого матраса"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6917.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6917.jpg') ?>" title="Боковина тонкого матраса"><img
                                src="<?= Url::to('@web/images/DSCF6917_65.jpg') ?>" alt="Боковина тонкого матраса"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF7071.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF7071.jpg') ?>"
                       title="Тонкий матрас в багажнике авто"><img
                                src="<?= Url::to('@web/images/DSCF7071_65.jpg') ?>"
                                alt="Тонкий матрас в багажнике авто"/></a>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <header><h1 class="is-size-5">Интернет-магазин матрасов на диван, ортопедических матрасов и
                спальных аксессуаров</h1></header>
        <h2 class="is-size-6">Тонкий матрас для дивана - доступный спальный аксессуар для каждого дома</h2>
        <p>Наше здоровье во многом зависит от качества сна. Ведь сон является незаменимой составляющей жизни и
            занимает 30%
            ее
            времени. Улучшить условия сна поможет ортопедический матрас на диван.
        </p>
        <h3 class="subtitle is-size-6">Для чего нужен выравнивающий матрас на диван?</h3>
        <p>
        <ul>По отзывам покупателей плюсы от его покупки очевидны:
            <li>- выравнивает поверхность старого дивана или матраса, если ваше спальное место пришло в негодность,
                стало
                неровным
                или поломались некоторые пружины
            </li>
            <li>- обеспечивает дополнительными спальными местами, когда к вам пришли друзья и их негде разместить.
                Достаньте
                тонкий матрас и стелите туда ваших знакомых
            </li>
            <li>- делает поверхность для сна мягкой и комфортной - нежный наполнитель и приятная ткань чехла
                обволакивают
                вас и
                способствуют крепкому сну
            </li>
            <li>- делает поверхность жестче при условии жесткого наполнителя - кокосовой койры</li>
            <li>- заменяет коврик или каремат на пикнике. Это идеальное решение, если вы собрались с палаткой за
                город.
                Возьмите
                тонкий матрас с ортопедическим эффектом, расстелите его в палатке - вы не почувствуете разницу между
                сном в походных условиях
                и дома
            </li>
            <li>- его используют как теплую и мягкую основу для пола в детскую комнату - ваш ребенок не простудится
                и не
                ударится на
                полу во время своих игр
            </li>
            <li>- не спешите покупать новый дорогой матрас - купить тонкий матрас на диван можно в нашем интернет
                магазине,
                цена
                которого
                доступна для большинства потребителей. Это сэкономит средства на что-то другое
            </li>
            <li>- это прекрасный и нужный презент для ваших родных и близких, который никого не оставит равнодушным
                и всегда
                пригодится
            </li>
        </ul>
        </p>
        <h3 class="subtitle is-size-6">Преимущества от покупки в нашем интернет-магазине</h3>
        <p>
        <ul>
            <li>
            <span class="icon is-large has-text-primary"><i
                        class="fas fa-lg fa-shipping-fast"></i></span>
                <a href="<?= Url::to(['site/dostavka-i-oplata']) ?>">
                        <span class="ok-underline-dotted">
                            бесплатная доставка по всей Украине</span></a> - мы обеспечиваем бесплатную доставку
                топперов на диван по всей территории Украины
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-gift"></i></span>
                сумка и ремни в подарок - дарим удобную сумку и ремни для фиксации
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-handshake"></i></span>
                оплата по факту получения - вам нет необходимости вносить какую-либо предоплату заранее. Оплатить
                заказ можно наложенным платежом при получении
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-certificate"></i></span>
                <a href="<?= Url::to(['site/about-us']) ?>">
                    <span class="ok-underline-dotted">вся продукция сертифицирована</span></a> - все изделия имеют
                сертификаты соответствия от производителя установленного образца
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-thumbs-up"></i></span>
                <a href="<?= Url::to(['site/kontrol-kachestva']) ?>">
                    <span class="ok-underline-dotted">контроль качества</span></a> - каждое изделие перед отгрузкой
                покупателю проходит несколько этапов проверки качества
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-clone"></i></span>
                большой выбор размеров: матрас на диван 160х200, другие стандартные размеры, также изготавливаем
                топперы любой формы и размеров.
            </li>
        </ul>
        </p>
        <hr>
        <h2 class="is-size-6">Ортопедические матрасы</h2>
        <p>Задача любого ортопедического матраса состоит в равномерном распределении нагрузки. Давление на любую
            точку тела
            должно быть одинаковым. В этом случае достигается максимальный ортопедический эффект.
        </p>
        <h3 class="subtitle is-size-6">Купить ортопедический матрас: пружинный или беспружинный?</h3>
        <p>Все матрасы классифицируются на <a
                    href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=pruzhinnyy'])) ?>">
                        <span class="ok-underline-dotted">
                            пружинные</span></a> с блоком пружин Bonnel или с независимым блоком Pocket Spring и
            беспружинные.
        </p>
        <p>Bonnel состоит из цельного пружинного блока, в связи с чем обеспечивает начальный ортопедический
            эффект.
            Такой вариант подойдет больницам, поликлиникам, недорогим отелям, хостелам, а также при ограниченном
            бюджете.
        </p>
        <p>Изделия с блоком независимых пружин Pocket Spring оказывают точечную и равномерную нагрузку на тело.
            Каждая пружина
            находится в отдельном чехле, сжимается независимо от других. Это дает хороший ортопедический эффект. А
            большой
            ассортимент верхних слоев и тканей чехла позволят сделать правильный выбор.
        </p>
        <p>
            <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=bespruzhinnyy'])) ?>">
                        <span class="ok-underline-dotted">
                            Матрас беспружинный</span></a> равномерно обволакивает контуры спящего, чем гарантирует
            максимальную ортопедичность. Во время
            сна тело находится в естественном положении, сосуды не пережимаются и не нарушается кровообращение.
        </p>
        <p>В любом случае, лучший выбор - это выбор с учетом ваших индивидуальных
            предпочтений и
            особенностей с учетом размера, жесткости, вида наполнителя, вашего возврата, веса и бюджета.
        </p>
        <p>Купить ортопедический матрас от производителя можно в нашем интернет-магазине. Мы предоставляем большой
            выбор односпальных и двуспальных матрасов, производство нестандартных размеров, оплату по факту
            получения, мгновенную рассрочку за 5
            минут, адресную доставку в любую
            точку Украины.
            Стоимость адресной доставки до вашего дома 150-600 грн (в зависимости от размера). Заказы свыше 5000
            грн. доставляем
            бесплатно.
        </p>
    </section>

    <section class="section">
        <header class="title is-size-5">Серии матрасов</header>
        <div class="columns is-multiline is-mobile">
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 1, 's1' => 'series-sleep-fly-silver-edition', 's2' => 's']) ?>">
                    <figure class=" image">
                        <img src="/images/series-sleep-fly-silver-edition.jpg"
                             alt="Серия матрасов Sleep&Fly Silver Edition"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 2, 's1' => 'series-sleep-fly', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly.jpg"
                             alt="Серия матрасов Sleep&Fly"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 3, 's1' => 'series-sleep-fly-fintess', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-fitness.jpg"
                             alt="Серия матрасов Sleep&Fly Silver Fitness"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 4, 's1' => 'series-sleep-fly-organic', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-organic.jpg"
                             alt="Серия матрасов Sleep&Fly Silver Organic"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 5, 's1' => 'series-take-go', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-take-go.jpg"
                             alt="Серия матрасов Take&Go"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 6, 's1' => 'series-take-go-bamboo', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-take-go-bamboo.jpg"
                             alt="Серия матрасов Take&Go Bamboo"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 7, 's1' => 'series-evolution', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-evolution.jpg"
                             alt="Серия матрасов Evolution"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 8, 's1' => 'series-doctor-health', 's2' => 's']) ?>"
                <figure class="image">
                    <img src="/images/series-doctor-health.jpg"
                         alt="Серия матрасов Doctor Health"/>
                </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 9, 's1' => 'series-herbalis-kids', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-herbalis-kids.jpg"
                             alt="Серия матрасов Herbalis Kids"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 10, 's1' => 'series-eco', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-eco.jpg"
                             alt="Серия матрасов Эко"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 15, 's1' => 'series-sleep-fly-hotel', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-hotel.jpg"
                             alt="Серия матрасов Sleep&Fly Hotel"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 11, 's1' => 'series-ortomed', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-ortomed.jpg"
                             alt="Серия матрасов Ortomed"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 13, 's1' => 'series-shans', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-shans.jpg"
                             alt="Серия матрасов Шанс"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 12, 's1' => 'series-comfort', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-comfort.jpg"
                             alt="Серия матрасов Comfort"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 14, 's1' => 'series-sleep-fly-mini', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-mini.jpg"
                             alt="Серия матрасов Sleep&Fly Mini"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 16, 's1' => 'series-day-night', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-day-night.jpg"
                             alt="Спальные аксессуары серии Day&Night"/>
                    </figure>
                </a>
            </article>
        </div>
    </section>
</div>

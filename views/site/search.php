<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <h1 class="title is-size-4"><?= Yii::t('app', 'Поиск по запросу') ?>: <?= $q ?></h1>
    <?php if (!empty($products)): ?>

        <div class="columns is-multiline">
            <?php foreach ($products as $item): ?>
                <?php $stickerDiscount = 0;
                $hasFreeShipping = false; ?>
                <div class="box column is-one-third-tablet">
                    <div class="ok-block-tag">
                        <?php if (!empty($item->productStickers)): ?>
                            <?php foreach ($item->productStickers as $productSticker): ?>
                                <?php
                                if (substr($productSticker->sticker->name, 0, 1) == '-') {
                                    $pos = strpos($productSticker->sticker->name, '%');
                                    $stickerDiscount = intval(substr($productSticker->sticker->name, 1, $pos - 1));
                                };
                                if ($productSticker->sticker->name == 'Бесплатная доставка') {
                                    $hasFreeShipping = true;
                                }
                                ?>
                                <p class="field ok-tag-field"><span
                                            class="tag <?= $productSticker->sticker->class ?> has-text-weight-bold ok-tag"><?= Yii::t('app', $productSticker->sticker->name) ?></span>
                                </p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if ($item->price > 5000 && !$hasFreeShipping): ?>
                            <p class="field ok-tag-field"><span
                                        class="tag is-danger has-text-weight-bold ok-tag"><?= Yii::t('app', 'Бесплатная доставка') ?></span>
                            </p>
                        <?php endif; ?>
                    </div>
                    <!--                            <a href="-->
                    <?php $params = ['product/index', 'product_id' => $item->product_id, 's1' => $item->seo_url, 's2' => 'p']; ?>
                    <a href="<?= Url::to($params) ?>"
                       title="<?= $item['name'] ?>">
                        <figure class="image ok-content-figure">
                            <?= Html::img("/{$item->getImage()->getPath('650x650')}", ['alt' => $item['name']]) ?>
                        </figure>
                    </a>
                    <div class="has-text-centered"><a
                                href="<?= Url::to($params) ?>"
                                title="<?= $item->name ?>"
                                class="has-text-grey-dark ok-carousel-name"><?= $item->name ?></a>
                    </div>
                    <?php if (array_key_exists($item->product_id, $reviewsStars)): ?>
                        <div class="product_reviews">
                            <a href="<?= Url::to(array_merge($params, ['#' => 'reviews'])) ?>">
                                <p class="has-text-centered">
                                    <?php foreach ($reviewsStars[$item->product_id]['rates'] as $r): ?>
                                        <span class="icon is-small has-text-primary"><i
                                                    class="<?= $r ?>"></i>
                                        </span>
                                    <?php endforeach; ?>
                                    <span><?= $reviewsStars[$item->product_id]['count'] ?></span>
                                </p>
                            </a>
                        </div>
                    <?php endif; ?>
                    <p class="has-text-centered">
                                    <span class="has-text-grey-darker has-text-weight-bold is-size-5">
                                        <?= Yii::$app->formatter->asInteger($item->price) ?> <?= Yii::t('app', 'грн.') ?></span>
                        <?php
                        if ($stickerDiscount != 0) {
                            echo '<span style="text-decoration: line-through;" class="has-text-grey">' . Yii::$app->formatter->asInteger(($item->price) / ((100 - $stickerDiscount) / 100)) . Yii::t('app', 'грн.') . '</span>';
                        }
                        ?>
                    </p>
                    <div class="has-text-centered">
                        <a class="button is-warning is-medium ok-product-compare"
                           title="<?= Yii::t('app', 'Добавить в сравнение') ?>"><span class="icon"
                                                                                      data-product-id=<?= $item->product_id; ?>><i
                                        class="fas fa-balance-scale" style="cursor: pointer;"></i></span></a>
                        <a class="button is-warning is-medium ok-product-favorite"
                           title="<?= Yii::t('app', 'Добавить в избранное') ?>"><span
                                    class="icon"
                                    data-product-id=<?= $item->product_id; ?>><i
                                        class="fas fa-heart" style="cursor: pointer;"></i></span>
                        </a>
                    </div>
                </div>
                <hr>
            <?php endforeach; ?>
        </div>

        <?php if ($pages->totalCount > 12): ?>
            <nav class="level has-background-white-bis">
                <div class="level-left"></div>
                <div class="level-right">
                    <div class="level-item">
                        <?= \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                            'prevPageLabel' => '<',
                            'nextPageLabel' => '>',
                            'maxButtonCount' => 5,
                        ]); ?>
                    </div>
                </div>
            </nav>
        <?php endif; ?>

    <?php else: ?>
        <p><?= Yii::t('app', '') ?>Нет данных по запросу!</p>
    <?php endif; ?>
</section>


<?php

use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <h1 class="title"><?= Yii::t('app', 'Избранное') ?></h1>
    <div class="columns is-multiline is-mobile ok-product-favorites"></div>
</section>

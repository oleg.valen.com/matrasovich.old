<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header>
        <h1 class="title">Про нас</h1>
        <p>Matrasovich.com.ua - інтернет-магазин тонких топперів на диван, ортопедичних матраців та спальних аксесуарів.
            Здійснюємо доставку по всій Україні з оплатою при отриманні.</p>
        <p>Ми є офіційним представником фабрики матраців ЕММ (Катеринославські меблеві майстерні). ЕММ більш
            12 років успішно працює на ринку України і по праву вважається фабрикою № 1 по виробництву матраців.</p>
    </header>
    <figure class="image is-16by9 iframe">
        <iframe class="has-ratio" width="640" height="360" src="https://www.youtube.com/embed/_zDPCL40VNM"
                frameborder="0" allowfullscreen=""></iframe>
    </figure>
    <section class="columns is-centered">
        <article class="column">
            <figure>
                <a href="/images/official-partner.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/official-partner.jpg" alt="Сертифікат офіційного партнера ЕММ"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert1.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert1.jpg" alt="Сертифікат відповідності"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert2.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert2.jpg" alt="Висновок-1"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert3.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert3.jpg" alt="Висновок-2"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert4.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert4.jpg" alt="Висновок-3"></a>
            </figure>
        </article>
    </section>
</section>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header><h1 class="title is-size-4">Як отримати кешбек за відгук?</h1></header>
    <p>Для отримання кешбека необхідно:</p>
    <ul>
        <li>- купити будь-який товар в нашому інтернет-магазині</li>
        <li>- після отримання, додати відгук із зазначенням телефону, на який було оформлено замовлення, і не менше двох
            фотографій
        </li>
    </ul>
    <p>Кешбек поширюється на різні найменування товару незалежно від розміру і кількості. наприклад:</p>
    <div class="table-container">
        <table class="is-fullwidth is-hoverable is-striped table is-size-7">
            <thead>
            <tr>
                <th>№</th>
                <th>Товар</th>
                <th>Розмір</th>
                <th>Кількість</th>
                <th>Кешбэк</th>
                <th>Коментар</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Матрац топпер Sleep&Fly Mini Flex Mini Жаккард</td>
                <td>70x190</td>
                <td>2</td>
                <td>100 грн</td>
                <td>це одне найменування</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Матрац топпер Sleep&Fly Mini Flex Mini Жаккард</td>
                <td>120x190</td>
                <td>1</td>
                <td>0 грн</td>
                <td>це найменування вже є в першому рядку</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Ортопедичний матрац Sleep&Fly Standart Жаккард</td>
                <td>140x200</td>
                <td>3</td>
                <td>100 грн</td>
                <td></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Подушка Day&Night Wellness</td>
                <td>45x50</td>
                <td>1</td>
                <td>50 грн</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="has-text-weight-bold">Разом:</td>
                <td></td>
                <td></td>
                <td class="has-text-weight-bold">250 грн</td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
    <p>Розмір кешбека:</p>
    <ul>
        <li>- матраци на диван, ортопедичні матраци - 100 грн.</li>
        <li>- наматрацники, подушки, ковдри - 50 грн.</li>
    </ul>
    <p>Протягом одного робочого дня з вами зв'яжеться менеджер магазину для уточнення даних по отриманню кешбека (на картку).</p>
    <p>Отримання кешбека проводиться на наступний день після додавання відгуку.</p>
    <p>За відгук без вказівки телефону і не менше двох фотографій кешбек не передбачений.</p>
</section>

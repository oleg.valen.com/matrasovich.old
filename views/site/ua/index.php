<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\View;

/* @var $this yii\web\View */
if (!empty($robots)) {
    $this->registerMetaTag(['name' => 'robots', 'content' => $robots]);
}

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/magiczoomplus.js', ['depends' => [AppAsset::class]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/magiczoomplus.css', ['depends' => [AppAsset::class]]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/magiczoomplus.module.css', ['depends' => [AppAsset::class]]);
?>

<div class="container content">


    <section class="section columns">
        <article class="column">
            <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=so-skidkoy'])) ?>"
               title="Знижки до 30% на ортопедичні матраци">
                <img src="<?= Url::to('@web/images/banner_discount.jpg') ?>"
                     alt="Знижки до 30% на ортопедичні матраци">
            </a>
        </article>
        <article class="column">
            <a href="<?= Url::to(['site/dostavka-i-oplata']) ?>"
               title="Безкоштовна доставка топперів на диван транспортною компанією Meest Express">
                <img src="<?= Url::to('@web/images/banner_delivery.jpg') ?>"
                     alt="Безкоштовна доставка топперів на диван транспортною компанією Meest Express">
            </a>
        </article>
        <article class="column">
            <a href="<?= Url::to(['site/dostavka-i-oplata', '#' => 'on-credit']) ?>"
               title="Оформіть миттєвий кредит за 5 хвилин">
                <img src="<?= Url::to('@web/images/banner_on_credit.jpg') ?>"
                     alt="Оформіть миттєвий кредит за 5 хвилин">
            </a>
        </article>
    </section>

    <section class="section">
        <header class="subtitle is-size-5">Тонкі матраци на диван</header>
        <div class="columns is-multiline is-mobile">
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Недорогі</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'category=nedorogie'])) ?>"
                   title="Недорогі тонкі матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[40]->getImage()->getPath('350x350')}" ?>"
                             alt="Недорогі тонкі матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Преміум</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'category=premium'])) ?>"
                   title="Преміум тонкі матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[52]->getImage()->getPath('350x350')}" ?>"
                             alt="Преміум тонкі матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З ортопедичної піни</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'fill=ergoflex'])) ?>"
                   title="Тонкі матраци з ортопедичною піною Ergoflex">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[47]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкі матраци з ортопедичною піною Ergoflex"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З мемори</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'fill=memory-foam'])) ?>"
                   title="Тонкі матраци з меморі">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[46]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкі матраци з меморі"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З кокоса</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'fill=kokosovaya-koyra'])) ?>"
                   title="Тонкі матраци з кокосової койри">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[48]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкі матраци з кокосової койри"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">М'які</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'zhestkost=myagkiy'])) ?>"
                   title="М'які тонкі матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[53]->getImage()->getPath('350x350')}" ?>"
                             alt="М'які тонкі матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Середньої жорсткості</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'zhestkost=sredniy'])) ?>"
                   title="Тонкі матраци середньої жорсткості">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[50]->getImage()->getPath('350x350')}" ?>"
                             alt="Тонкі матраци середньої жорсткості"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Жорсткі</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 2, 's1' => 'toppery', 's2' => 'c', 's3' => 'zhestkost=zhestkiy'])) ?>"
                   title="Жорсткі тонкі матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[41]->getImage()->getPath('350x350')}" ?>"
                             alt="Жорсткі тонкі матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
        </div>
    </section>
    <hr class="is-hidden-mobile">
    <section class="section">
        <header class="subtitle is-size-5">Ортопедичні матраци</header>
        <div class="columns is-multiline is-mobile">
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Зі знижкою</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=so-skidkoy'])) ?>"
                   title="Ортопедичні матраци зі знижкою">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[59]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедичні матраци зі знижкою"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Недорогі</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=nedorogie'])) ?>"
                   title="Недорогі ортопедичні матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[54]->getImage()->getPath('350x350')}" ?>"
                             alt="Недорогі ортопедичні матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Безпружинні</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=bespruzhinnyy'])) ?>"
                   title="Безпружинні ортопедичні матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[69]->getImage()->getPath('350x350')}" ?>"
                             alt="Безпружинні ортопедичні матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Пружинні</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=pruzhinnyy'])) ?>"
                   title="Пружинні ортопедичні матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[86]->getImage()->getPath('350x350')}" ?>"
                             alt="Пружинні ортопедичні матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З мемори</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=memory-foam'])) ?>"
                   title="Ортопедичні матраци з меморі">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[74]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедичні матраци з меморі"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З ортопедичної піни</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=ergoflex'])) ?>"
                   title="Ортопедичні матраци з піни Ergoflex">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[75]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедичні матраци з піни Ergoflex"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З кокоса</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=kokosovaya-koyra'])) ?>"
                   title="Ортопедичні матраци з кокосової койри">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[67]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедичні матраци з кокосової койри"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">З латексу</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'fill=latex'])) ?>"
                   title="Ортопедичні матраци з латексу">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[81]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедичні матраци з латексу"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">М'які</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'zhestkost=myagkiy'])) ?>"
                   title="М'які ортопедичні матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[54]->getImage()->getPath('350x350')}" ?>"
                             alt="М'які ортопедичні матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Жорсткі</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'zhestkost=zhestkiy'])) ?>"
                   title="Жорсткі ортопедичні матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[76]->getImage()->getPath('350x350')}" ?>"
                             alt="Жесткие ортопедические матрасы"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Для готелів</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=dlya-gostinic'])) ?>"
                   title="Ортопедичні матраци для готелів">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[139]->getImage()->getPath('350x350')}" ?>"
                             alt="Ортопедичні матраци для готелів"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <p class="ok-content-p">Дитячі</p>
                <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'category=detskie'])) ?>"
                   title="Дитячі ортопедичні матраци">
                    <figure class="image ok-content-figure">
                        <img src="<?= "/{$products[144]->getImage()->getPath('350x350')}" ?>"
                             alt="Дитячі ортопедичні матраци"/>
                    </figure>
                </a>
                <hr class="is-hidden-tablet">
            </article>
        </div>
    </section>
    <section class="is-hidden-mobile">
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6917.jpg') ?>"
                             alt="Боковина тонкого матраца"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6867.jpg') ?>"
                             alt="Чохол тонкого матраца"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6948.jpg') ?>"
                             alt="Аеро-чохол тонкого матраца"/>
                    </figure>
                </article>
            </div>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6951.jpg') ?>"
                             alt="Тонкий матрац в рулоні"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                             alt="Тонкий матрац - загальний вигляд"/>
                    </figure>
                </article>
            </div>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent">
                <article class="tile is-child is-3">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF6976.jpg') ?>"
                             alt="Тонкий матрац в шафу-купе"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF7044.jpg') ?>"
                             alt="Тонкий матрац на резинках"/>
                    </figure>
                </article>
                <article class="tile is-child">
                    <figure class="image">
                        <img src="<?= Url::to('@web/images/DSCF7071.jpg') ?>"
                             alt="Тонкий матрац в багажнику авто"/>
                    </figure>
                </article>
            </div>
        </div>
    </section>

    <section class="section product-img-box is-hidden-tablet">
        <div class="MagicToolboxContainer selectorsBottom minWidth">
            <div class="magic-slide mt-active" data-magic-slide="zoom">
                <a id="MagicZoomPlusImage38950"
                   class="MagicZoom"
                   href="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                   title="Тонкий матрас - общий вид"
                   data-options="zoomWidth:600;zoomHeight:600;selectorTrigger:hover;lazyZoom:true;rightClick:true;zoomMode:preview;expandCaption:false;cssClass:white-bg;hint:off;"
                   data-mobile-options="zoomWidth:auto;zoomHeight:auto;lazyZoom:false;zoomMode:zoom;cssClass:;hint:off;textHoverZoomHint:Touch to zoom;textClickZoomHint:Дважды нажмите, чтобы увеличить;textExpandHint:Tap to expand;">
                    <img itemprop="image"
                         src="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                         alt="Тонкий матрас - общий вид"/>
                </a>
            </div>
            <div class="magic-slide" data-magic-slide="360"></div>
            <div class="MagicToolboxSelectorsContainer">
                <div id="MagicToolboxSelectors38950" class="">
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF7032.jpg') ?>"
                       title="Тонкий матрац - загальний вид"><img src="<?= Url::to('@web/images/DSCF7032_65.jpg') ?>"
                                                                  alt="Тонкий матрац - загальний вид"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6951.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6951.jpg') ?>" title="Тонкий матрац в рулоні"><img
                                src="<?= Url::to('@web/images/DSCF6951_65.jpg') ?>"
                                alt="Тонкий матрац в рулоні"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6867.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6867.jpg') ?>" title="Чохол тонкого матраца"><img
                                src="<?= Url::to('@web/images/DSCF6867_65.jpg') ?>"
                                alt="Чохол тонкого матраца"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF7044.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF7044.jpg') ?>"
                       title="Тонкий матрац на резинках"><img src="<?= Url::to('@web/images/DSCF7044_65.jpg') ?>"
                                                              alt="Тонкий матрац на резинках"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6948.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6948.jpg') ?>"
                       title="Аеро-чохол тонкого матраца"><img src="<?= Url::to('@web/images/DSCF6948_65.jpg') ?>"
                                                               alt="Аеро-чохол тонкого матраца"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF6917.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF6917.jpg') ?>" title="Боковина тонкого матраца"><img
                                src="<?= Url::to('@web/images/DSCF6917_65.jpg') ?>" alt="Боковина тонкого матраца"/></a>
                    <a data-magic-slide-id="zoom" data-zoom-id="MagicZoomPlusImage38950"
                       href="<?= Url::to('@web/images/DSCF7071.jpg') ?>"
                       data-image="<?= Url::to('@web/images/DSCF7071.jpg') ?>"
                       title="Тонкий матрац в багажнику авто"><img
                                src="<?= Url::to('@web/images/DSCF7071_65.jpg') ?>"
                                alt="Тонкий матрац в багажнику авто"/></a>
                </div>
            </div>
        </div>
    </section>

    <section class="section">


        <header><h1 class="is-size-5">Інтернет-магазин матраців на диван, ортопедичних матраців та спальних
                аксесуарів</h1></header>
        <h2 class="is-size-6">Тонкий матрац для дивана - доступний спальний аксесуар для кожного будинку</h2>
        <p>Наше здоров'я багато в чому залежить від якості сну. Адже сон є незамінною складовою життя і займає 30% її
            часу. Поліпшити умови сну допоможе ортопедичний матрац на диван.
        </p>
        <h3 class="subtitle is-size-6">Для чого потрібен вирівнюючий матрац на диван?</h3>
        <p>
        <ul>За відгуками покупців плюси від його покупки очевидні:
            <li>- вирівнює поверхню старого дивана або матраца, якщо ваше спальне місце прийшло в непридатність, стало
                нерівним або поламалися деякі пружини
            </li>
            <li>- забезпечує додатковими спальними місцями, коли до вас прийшли друзі і їх ніде розмістити. Дістаньте
                тонкий
                матрац і стелите туди ваших знайомих
            </li>
            <li>- робить поверхню для сну м'якою і комфортною - ніжний наповнювач і приємна тканина чохла обволікають
                вас і
                сприяють міцному сну
            </li>
            <li>- робить поверхню жорсткіше за умови жорсткого наповнювача - кокосової койри</li>
            <li>- замінює килимок або каремат на пікніку. Це ідеальне рішення, якщо ви зібралися з наметом за місто.
                Візьміть тонкий матрац з ортопедичним ефектом, розстеліть його в наметі - ви не відчуєте різницю між
                сном в
                похідних умовах і вдома
            </li>
            <li>- його використовують як теплу і м'яку основу для підлоги в дитячу кімнату - ваша дитина не простудиться
                і
                не вдариться на підлозі під час своїх ігор
            </li>
            <li>- не поспішайте купувати новий дорогий матрац - купити тонкий матрац на диван можна в нашому інтернет
                магазині, ціна якого доступна для більшості споживачів. Це заощадить кошти на щось інше
            </li>
            <li>- це прекрасний і потрібний презент для ваших рідних і близьких, який нікого не залишить байдужим і
                завжди
                стане в нагоді
            </li>
        </ul>
        </p>
        <h3 class="subtitle is-size-6">Переваги від покупки в нашому інтернет-магазині</h3>
        <p>
        <ul>
            <li>
            <span class="icon is-large has-text-primary"><i
                        class="fas fa-lg fa-shipping-fast"></i></span>
                <a href="<?= Url::to(['site/dostavka-i-oplata']) ?>">
                        <span class="ok-underline-dotted">
                            безкоштовна доставка по всій Україні</span></a> - ми забезпечуємо безкоштовну доставку
                топперів на диван до під'їзду вашого будинку по всій території України
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-gift"></i></span>
                сумка і ремені в подарунок - даруємо зручну сумку і ремені для фіксації
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-handshake"></i></span>
                оплата за фактом отримання - вам немає необхідності вносити будь-яку передоплату заздалегідь. Оплатити
                замовлення можна післяплатою при отриманні
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-certificate"></i></span>
                <a href="<?= Url::to(['site/about-us']) ?>">
                    <span class="ok-underline-dotted">вся продукція сертифікована</span></a> - все вироби мають
                сертифікати відповідності від виробника встановленого зразка
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-thumbs-up"></i></span>
                <a href="<?= Url::to(['site/kontrol-kachestva']) ?>">
                    <span class="ok-underline-dotted">контроль якості</span></a> - кожен виріб перед відвантаженням
                покупцю проходить кілька етапів перевірки якості
            </li>
            <li><span class="icon is-large has-text-primary"><i
                            class="fas fa-lg fa-clone"></i></span>
                великий вибір розмірів: матрац на диван 160х200, інші стандартні розміри, також виготовляємо топпери
                будь-якої
                форми і розмірів.
            </li>
        </ul>
        </p>
        <hr>
        <h2 class="is-size-6">Ортопедичні матраци</h2>
        <p>Завдання будь-якого ортопедичного матраца складається в рівномірному розподілі навантаження. Тиск на будь-яку
            точку тіла має бути однаковим. У цьому випадку досягається максимальний ортопедичний ефект.</p>
        <h3 class="subtitle is-size-6">Купити ортопедичний матрац: пружинний або безпружинний?</h3>
        <p>Всі матраци класифікуються на <a
                    href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=pruzhinnyy'])) ?>">
                        <span class="ok-underline-dotted">
                            пружинні</span></a> з блоком пружин Bonnel або з незалежним блоком Pocket Spring і
            безпружинні.
        </p>
        <p>Bonnel складається з цільного пружинного блоку, в зв'язку з чим забезпечує початковий ортопедичний ефект.
            Такий варіант підійде лікарням, поліклінікам, недорогим готелям, хостелам, а також при обмеженому
            бюджеті.</p>
        <p>Вироби з блоком незалежних пружин Pocket Spring надають точкове і рівномірне навантаження на тіло. Кожна
            пружина знаходиться в окремому чохлі, стискається незалежно від інших. Це дає хороший ортопедичний ефект. А
            великий асортимент верхніх шарів і тканин чохла дозволять зробити правильний вибір.</p>
        <p>
            <a href="<?= urldecode(Url::to(['category/index', 'category_id' => 4, 's1' => 'matrasy', 's2' => 'c', 's3' => 'type=bespruzhinnyy'])) ?>">
                        <span class="ok-underline-dotted">
                            Матрац безпружинний</span></a> рівномірно обволікає контури сплячого, чим гарантує
            максимальну ортопедичність. Під час сну
            тіло знаходиться в природному положенні, судини не перетискаються і не порушується кровообіг.
        </p>
        <p>У будь-якому випадку, кращий вибір - це вибір з урахуванням ваших індивідуальних переваг і особливостей з
            урахуванням розміру, жорсткості, виду наповнювача, вашого повернення, ваги і бюджету.</p>
        <p> Купити ортопедичний матрац від виробника можна в нашому інтернет-магазині. Ми надаємо великий вибір
            односпальних
            і двоспальних матраців, виробництво нестандартних розмірів, оплату за фактом отримання, миттєву розстрочку
            за 5
            хвилин, адресну доставку в будь-яку точку України. Вартість адресної доставки до вашого будинку 150-600 грн
            (в
            залежності від розміру). Замовлення понад 5000 грн. доставляємо безкоштовно.</p>
    </section>

    <section class="section">
        <header class="title is-size-5">Серии матрасов</header>
        <div class="columns is-multiline is-mobile">
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 1, 's1' => 'series-sleep-fly-silver-edition', 's2' => 's']) ?>">
                    <figure class=" image">
                        <img src="/images/series-sleep-fly-silver-edition.jpg"
                             alt="Серія матраців Sleep&Fly Silver Edition"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 2, 's1' => 'series-sleep-fly', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly.jpg"
                             alt="Серія матраців Sleep&Fly"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 3, 's1' => 'series-sleep-fly-fintess', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-fitness.jpg"
                             alt="Серія матраців Sleep&Fly Silver Fitness"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 4, 's1' => 'series-sleep-fly-organic', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-organic.jpg"
                             alt="Серія матраців Sleep&Fly Silver Organic"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 5, 's1' => 'series-take-go', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-take-go.jpg"
                             alt="Серія матраців Take&Go"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 6, 's1' => 'series-take-go-bamboo', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-take-go-bamboo.jpg"
                             alt="ССерія матраців Take&Go Bamboo"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 7, 's1' => 'series-evolution', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-evolution.jpg"
                             alt="Серія матраців Evolution"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 8, 's1' => 'series-doctor-health', 's2' => 's']) ?>"
                <figure class="image">
                    <img src="/images/series-doctor-health.jpg"
                         alt="Серія матраців Doctor Health"/>
                </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 9, 's1' => 'series-herbalis-kids', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-herbalis-kids.jpg"
                             alt="Серія матраців Herbalis Kids"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 10, 's1' => 'series-eco', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-eco.jpg"
                             alt="Серія матраців Еко"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 15, 's1' => 'series-sleep-fly-hotel', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-hotel.jpg"
                             alt="Серія матраців Sleep&Fly Hotel"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 11, 's1' => 'series-ortomed', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-ortomed.jpg"
                             alt="Серія матраців Ortomed"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 13, 's1' => 'series-shans', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-shans.jpg"
                             alt="Серія матраців Шанс"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 12, 's1' => 'series-comfort', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-comfort.jpg"
                             alt="Серія матраців Comfort"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 14, 's1' => 'series-sleep-fly-mini', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-sleep-fly-mini.jpg"
                             alt="Серія матраців Sleep&Fly Mini"/>
                    </figure>
                </a>
            </article>
            <article class="column is-half-mobile is-one-quarter-tablet">
                <a href="<?= Url::to(['serie/index', 'serie_id' => 16, 's1' => 'series-day-night', 's2' => 's']) ?>">
                    <figure class="image">
                        <img src="/images/series-day-night.jpg"
                             alt="Спальные аксессуары серии Day&Night"/>
                    </figure>
                </a>
            </article>
        </div>
    </section>
</div>
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header><h1 class="is-size-4">Доставка і оплата</h1></header>
    <h2 class="is-size-5">Доставка</h2>
    <div class="table-container">
        <table class="is-fullwidth is-hoverable is-striped table">
            <thead>
            <tr>
                <th colspan="1"></th>
                <th colspan="1">
                    <figure class="image is-64x64">
                        <img src="/images/meest-express-logo.jpg"
                             alt="Meest Express"></figure>
                </th>
                <th colspan="1">
                    <figure class="image is-64x64">
                        <img src="/images/nova-poshta-logo.jpg"
                             alt="Нова Пошта"></figure>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Тип доставки</td>
                <td colspan="2" class="has-text-centered">Адресна доставка до під'їзду або на відділення</td>
            </tr>
            <tr>
                <td>Топпери на диван</td>
                <td class="has-text-centered">Безкоштовно</td>
                <td class="has-text-centered">100 грн.</td>
            </tr>
            <tr>
                <td>Ортопедичні матраци до 5000 грн.</td>
                <td colspan="2" class="has-text-centered">150-600 грн (в залежності від розміру)</td>
            </tr>
            <tr>
                <td>Ортопедичні матраци понад 5000 грн.</td>
                <td colspan="2" class="has-text-centered">Безкоштовно</td>
            </tr>
            <tr>
                <td>Дитячі матраци</td>
                <td colspan="2" class="has-text-centered">За тарифами перевізника</td>
            </tr>
            <tr>
                <td>Наматрацники</td>
                <td colspan="2" class="has-text-centered">За тарифами перевізника</td>
            </tr>
            <tr>
                <td>Подушки</td>
                <td colspan="2" class="has-text-centered">За тарифами перевізника</td>
            </tr>
            <tr>
                <td>Оплата за накладений платіж</td>
                <td colspan="2" class="has-text-centered">20 грн + 2% (від суми грошового переказу)</td>
            </tr>
            <tr>
                <td>Термін доставки (з адреси на адресу або відділення)</td>
                <td class="has-text-centered"><a href="https://www.meest-express.com.ua/ua/kalkulyator"
                                                 title="Срок доставки по Meest Express"
                                                 class="has-text-link">Калькулятор Meest Express</a></td>
                <td class="has-text-centered"><a href="https://novaposhta.ua/ru/onlineorder/estimatedate"
                                                 title="Срок доставки по Новой Почте"
                                                 class="has-text-link">Калькулятор Нової Пошти</a></td>
            </tr>
            </tbody>
        </table>
    </div>

    <h2 class="is-size-5">Оплата</h2>
    <ul>
        <li>1. Післяплатою при отриманні</li>
        <li>2. На банківську карту</li>
        <li>3. Безготівковим переказом на розрахунковий банківський рахунок</li>
        <li><a id="on-credit"></a>4. Миттєвою розстрочкою за 5 хвилин від Приватбанку. Оформити кредит можна зі сторінки товару.
        </li>
        <li>
            5. Через мобільний додаток Приват24, використовуючи QR-код
            <ul>
                <li>5.1. Завантажте програму Приват24</li>
                <li>5.2. Увійдіть в Приват24 і натисніть кнопку для сканування QR-коду</li>
                <li>5.3. Відскануте QR-код з картинки нижче</li>
                <li>5.4. Виберіть карту для оплати</li>
            </ul>
            <figure class="image has-text-left">
                <img src="/images/privat24-qr-code.jpg"
                     alt="Приват24 QR-код" style="height: 516px; width: 243px;"></figure>
        </li>
    </ul>

    <h2 class="is-size-5">Інші умови</h2>
    <p>Доставка здійснюється без підняття на поверх (по домовленості з водієм).</p>
    <p>При отриманні уважно перевіряйте товар на цілісність.</p>
    <p>У разі нестандартного розміру передбачається передоплата 100%.</p>

</section>

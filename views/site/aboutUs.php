<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header>
        <h1 class="title">О нас</h1>
        <p>Matrasovich.com.ua - интернет-магазин тонких топперов на диван, ортопедических матрасов и спальных
            аксессуаров.
            Осуществляем доставку по всей Украине с оплатой при получении.</p>
        <p>Мы являемся официальным представителем фабрики матрасов ЕММ (Екатеринославские Мебельные Мастерские). ЕММ
            более
            12 лет успешно работает на рынке Украины и по праву считается фабрикой № 1 по производству матрасов.</p>
    </header>
    <figure class="image is-16by9 iframe">
        <iframe class="has-ratio" width="640" height="360" src="https://www.youtube.com/embed/_zDPCL40VNM"
                frameborder="0" allowfullscreen=""></iframe>
    </figure>
    <section class="columns is-centered">
        <article class="column">
            <figure>
                <a href="/images/official-partner.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/official-partner.jpg" alt="Сертификат официального партнера ЕММ"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert1.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert1.jpg" alt="Сертификат соответствия"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert2.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert2.jpg" alt="Заключение-1"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert3.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert3.jpg" alt="Заключение-2"></a>
            </figure>
        </article>
        <article class="column">
            <figure>
                <a href="/images/sert4.pdf" target="_blank"
                   rel="noopener">
                    <img src="/images/sert4.jpg" alt="Заключение-3"></a>
            </figure>
        </article>
    </section>
</section>

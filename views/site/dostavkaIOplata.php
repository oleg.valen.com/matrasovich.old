<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header><h1 class="is-size-4">Доставка и оплата</h1></header>
    <h2 class="is-size-5">Доставка</h2>
    <div class="table-container">
        <table class="is-fullwidth is-hoverable is-striped table">
            <thead>
            <tr>
                <th colspan="1"></th>
                <th>
                    <figure class="image is-64x64">
                        <img src="/images/nova-poshta-logo.jpg"
                             alt="Новая Почта"></figure>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Топперы на диван</td>
                <td class="has-text-centered">бесплатно на отделение; 100 грн на адрес</td>
            </tr>
            <tr>
                <td>Ортопедические матрасы до 6000 грн.</td>
                <td class="has-text-centered">200-600 грн (в зависимости от размера)</td>
            </tr>
            <tr>
                <td>Ортопедические матрасы свыше 6000 грн</td>
                <td class="has-text-centered">Бесплатно</td>
            </tr>
            <tr>
                <td>Детские матрасы</td>
                <td class="has-text-centered">50 грн на отделение; 70 грн на адрес</td>
            </tr>
            <tr>
                <td>Наматрасники</td>
                <td class="has-text-centered">50 грн на отделение; 70 грн на адрес</td>
            </tr>
            <tr>
                <td>Подушки</td>
                <td class="has-text-centered">50 грн на отделение; 70 грн на адрес</td>
            </tr>
            <tr>
                <td>Оплата за наложенный платеж</td>
                <td class="has-text-centered">20 грн + 2% (от суммы денежного перевода)</td>
            </tr>
            <tr>
                <td>Срок доставки (с адреса на адрес или отделение)</td>
                <td class="has-text-centered"><a href="https://novaposhta.ua/ru/onlineorder/estimatedate"
                                                 title="Срок доставки по Новой Почте"
                                                 class="has-text-link">Калькулятор Новой Почты</a></td>
            </tr>
            </tbody>
        </table>
    </div>

    <h2 class="is-size-5">Оплата</h2>
    <ul>
        <li>1. Наложенным платежом при получении</li>
        <li>2. На банковскую карту</li>
        <li>3. Безналичным переводом на расчетный банковский счет</li>
        <li>
            4. Через мобильное приложение Приват24, используя QR-код
            <ul>
                <li>4.1. Загрузите приложение Приват24</li>
                <li>4.2. Войдите в Приват24 и нажмите кнопку для сканирования QR-кода</li>
                <li>4.3. Отсканируйте QR-код с картинки ниже</li>
                <li>4.4. Выберите карту для оплаты</li>
            </ul>
            <figure class="image has-text-left">
                <img src="/images/privat24-qr-code.jpg"
                     alt="Приват24 QR-код" style="height: 516px; width: 243px;"></figure>
        </li>
    </ul>

    <h2 class="is-size-5">Другие условия</h2>
    <p>Доставка осуществляется без поднятия на этаж (по договоренности с водителем).</p>
    <p>При получении внимательно проверяйте товар на целостность.</p>
    <p>В случае нестандартного размера предусматривается предоплата 100%.</p>

</section>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header><h1 class="title is-size-4">Как получить кешбэк за отзыв?</h1></header>
    <p>Для получения кешбэка необходимо:</p>
    <ul>
        <li>- купить любой товар в нашем интернет-магазине</li>
        <li>- после получения, добавить отзыв с указанием телефона, на который был оформлен заказ, и не менее двух
            фотографий
        </li>
    </ul>
    <p>Кешбэк распространяется на различные наименования товара независимо от размера и количества.
        Например:</p>
    <div class="table-container">
        <table class="is-fullwidth is-hoverable is-striped table is-size-7">
            <thead>
            <tr>
                <th>№</th>
                <th>Товар</th>
                <th>Размер</th>
                <th>Количество</th>
                <th>Кешбэк</th>
                <th>Комментарий</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Матрас топпер Sleep&Fly Mini Flex Mini Жаккард</td>
                <td>70x190</td>
                <td>2</td>
                <td>100 грн</td>
                <td>это одно наименование</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Матрас топпер Sleep&Fly Mini Flex Mini Жаккард</td>
                <td>120x190</td>
                <td>1</td>
                <td>0 грн</td>
                <td>это наименование уже есть в первой строке</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Ортопедический матрас Sleep&Fly Standart Жаккард</td>
                <td>140x200</td>
                <td>3</td>
                <td>100 грн</td>
                <td></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Подушка Day&Night Wellness</td>
                <td>45x50</td>
                <td>1</td>
                <td>50 грн</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="has-text-weight-bold">Итого:</td>
                <td></td>
                <td></td>
                <td class="has-text-weight-bold">250 грн</td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
    <p>Размер кешбека:</p>
    <ul>
        <li>- матрасы на диван, ортопедические матрасы - 100 грн.</li>
        <li>- наматрасники, подушки, одеяла - 50 грн.</li>
    </ul>
    <p>В течении одного рабочего дня с вами свяжется менеджер магазина для уточнения данных по получению кешбэка (на
        карту).</p>
    <p>Получение кешбэка производится на следующий день после добавления отзыва.</p>
    <p>За отзыв без указания телефона и не менее двух фотографий кешбэк не предусмотрен.</p>
</section>

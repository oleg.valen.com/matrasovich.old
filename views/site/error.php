<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Страница не найдена :(';
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>
<section class="section">
    <div class="container">
        <h1 class="title"><?= Html::encode($this->title) ?></h1>
        <p>
            Вероятнее всего, страница удалена или Вы допустили ошибку в написании адреса.
        </p>
    </div>
</section>

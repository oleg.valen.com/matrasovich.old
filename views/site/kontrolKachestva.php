<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<section class="section container content">
    <header>
        <h1 class="title is-size-4">Контроль качества продукции и материалов</h1>
        <p>Каждый матрас, произведенный ЕММ, проходит проверку лабораторией качества, не имеющей аналогов в Украине, и
            поступает к покупателю только после прохождения всех циклов контроля: входной, промежуточный и выходной. На
            входном контроле проверяется качество сырья и материалов на соответствие физико-механических свойств.
            Промежуточный контроль подразумевает проверку качества полуфабрикатов, из которых будет произведено изделие.
            В выходную проверку входит заключительный контроль как с помощью автоматизированной техники, так и
            визуальный осмотр специалистами завода. Все виды контроля проводятся на современном европейском
            оборудовании.</p>
    </header>
    <article>
        <h2 class="subtitle is-size-5">Проверка матраса на долговечность</h2>
        <div class="columns">
            <div class="column is-one-third">
                <figure>
                    <img src="/images/proverka-matrasa-na-dolgovechnost.jpg" alt="Проверка матраса на долговечность">
                </figure>
            </div>
            <div class="column">
                <p>Используемое оборудование: DRS-tester в соответствии с европейским стандартом качества EN1957.</p>
                <p>Описание теста: на изделие опускается барабан весом 140 кг, который поворачивается, имитируя движения
                    человека во время сна. Таким способом определяется рекомендованный срок эксплуатации. Матрасы ЕММ
                    выдерживают 29 000 оборотов барабана, что соответствует 10 годам эксплуатации.</p>
            </div>
        </div>
    </article>
    <hr>
    <article>
        <h2 class="subtitle is-size-5">Проверка жесткости матраса</h2>
        <div class="columns">
            <div class="column is-one-third">
                <figure>
                    <img src="/images/proverka-zhestkosti-matrasa.jpg" alt="Проверка жесткости матраса">
                </figure>
            </div>
            <div class="column">
                <p>Используемый тест: FDS-tester.</p>
                <p>Описание теста: На горизонтально расположенный матрас прикладывается нагрузка весом от 3 до 200 кг.
                    Система учитывает жесткость, несущую способность и остаточную деформацию внутренних слоев. Такая
                    проверка необходима для определения допустимой нагрузки на одно спальное место, а также учитывается
                    при разработке
                    новых серий матрасов.</p>
            </div>
        </div>
    </article>
    <hr>
    <article>
        <h2 class="subtitle is-size-5">Проверка характеристик проволоки и пружин</h2>
        <div class="columns">
            <div class="column is-one-third">
                <figure>
                    <img src="/images/proverka-provoloki-matrasa.jpg" alt="Проверка характеристик проволоки и пружин">
                </figure>
            </div>
            <div class="column">
                <p>Используемое оборудование: машина двойного действия S&W-analyzer.</p>
                <p>Описание теста: с помощью этого контроля определяется жесткость, усадка, несущая способность,
                    деформация
                    и прочность на
                    разрыв металлических пружин.</p>
            </div>
        </div>
    </article>
    <hr>
    <h2 class="subtitle is-size-5">Лаборатория качества ортопедической пены</h2>
    <p>В 2017 компания ЕММ внедрила в производственные мощности собственный цех по производству ортопедической пены.
        Задачей лаборатории качества по этому направлению стала проверка свойств пены по основным показателям. Благодаря
        европейскому оборудованию достигается 100 % гарантия качества этого материала в продукции: матрасах и подушках.
    </p>
    <hr>
    <article>
        <h2 class="subtitle is-size-5">Проверка материала на долговечность</h2>
        <div class="columns">
            <div class="column is-one-third">
                <figure>
                    <img src="/images/proverka-materiala-na-dolgovechnost.jpg"
                         alt="Проверка материала на долговечность">
                </figure>
            </div>
            <div class="column">
                <p>Используемый тест: AР-tester.</p>
                <p>Описание теста: это тестирование помогает определить показатель нагрузки, который может выдержать
                    материал во время эксплуатации. Проверка происходит путем сдавливания образца на 50 % его высоты 250
                    000
                    раз
                    и определения коэффициента потери высоты и жесткости после испытания.</p>
            </div>
        </div>
    </article>
    <hr>
    <article>
        <h2 class="subtitle is-size-5">Проверка материала на остаточную деформацию</h2>
        <div class="columns">
            <div class="column is-one-third">
                <figure>
                    <img src="/images/proverka-materiala-na-ostatochnuyu-deformaciyu.jpg"
                         alt="Проверка материала на остаточную деформацию">
                </figure>
            </div>
            <div class="column">
                <p>Используемый тест: TNG-tester.</p>
                <p>Описание теста: во время тестирования испытуемый образец подвергают сжатию между пластинами на 50 %
                    высоты и оставляют на 72 часа. После этого определяют потерю высоты и рассчитывают показатель
                    остаточной
                    деформации.</p>
            </div>
        </div>
    </article>
    <hr>
    <article>
        <h2 class="subtitle is-size-5">Проверка материала на эластичность</h2>
        <div class="columns">
            <div class="column is-one-third">
                <figure>
                    <img src="/images/proverka-materiala-na-elastichnost.jpg"
                         alt="Проверка материала на эластичность">
                </figure>
            </div>
            <div class="column">
                <p>Используемый тест: RB-tester.</p>
                <p>Описание теста: это оборудование определяет пружинные свойства матраса. Чем выше этот показатель, те
                    больший уровень комфорта и удобства в готовых изделиях.</p>
            </div>
        </div>
    </article>
    <hr>
    <article>
        <h2 class="subtitle is-size-5">Проверка материала на удлинение и прочность при разрыве</h2>
        <p>Используемое оборудование: универсальная измерительная машина.</p>
        <p>Описание теста: измеряют показатели образцов ортопедической пены на удлинение и прочность при разрыве. Это,
            в свою очередь, определяет прочность и эксплуатационные характеристики материала в изделии.</p>
        <hr>
    </article>
    <article>
        <h2 class="subtitle is-size-5">Проверка материала на комфортность</h2>
        <p>Описание теста: контроль также проходит на универсальной машине. В ходе тестирования определяется
            сопротивление
            материала при нагрузке и разгрузке, что влияет на уровень комфорта и ортопедичности.</p>
        <hr>
    </article>
    <p>Уровень оборудования, разнообразие тестов, контроля, методик, испытаний и квалифицированные специалисты
        лаборатории качества ЕММ гарантируют выпуск высококачественных, надежных, комфортных и долговечных матрасов
        и
        подушек, которые обеспечивают здоровый и полноценный сон.</p>
</section>

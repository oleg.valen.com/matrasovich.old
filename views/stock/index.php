<?php
$this->registerMetaTag(['name' => 'description', 'content' => 'Остатки тонких матрасов на диван и ортопедических матрасов. Отправка в день заказа.']);
$this->registerMetaTag(['name' => 'keywords', 'content' => 'остатки тонкие матрасы, остатки ортопедические матрасы, наличие тонкие матрасы, наличие ортопедические матрасы']);
$this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow']);
$h1 = 'Наличие на складе';
$this->title = 'Остатки на склада. Отправка в день заказа';
?>

<section class="section container content ok-content">
    <header><h1 class="title is-size-4">Наличие на складе (отправка в день заказа)</h1></header>
    <?php foreach ($products as $categoryId => $prods): ?>
        <h2><?= $categories[$categoryId]['name'] ?></h2>
        <?php foreach ($prods as $p): ?>
            <?php foreach ($p as $a): ?>
                <div class="columns is-vcentered product-list">
                    <div class="column">
                        <div class="columns is-vcentered is-mobile">
                            <div class="column is-one-quarter">
                                <a href="<?= $a['href'] ?>" title="<?= $a['pName'] ?>">
                                    <figure class="image ok-content-figure"><?= $a['img'] ?></figure>
                                </a>
                            </div>
                            <div class="column"><a href="<?= $a['href'] ?>"
                                                   title="<?= $a['pName'] ?>"><?= $a['pName'] ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="columns is-mobile is-vcentered">
                            <div class="column"><?= $a['aName'] ?></div>
                            <div class="column"><?= $a['price'] ?></div>
                            <div class="column">
                                <a class="button is-warning has-text-weight-bold ok-product-cart"
                                   title="<?= Yii::t('app', 'Купить') ?>"
                                   data-product-id=<?= $a['productId'] ?> data-attribute-id="<?= $a['attrId'] ?>"><?= Yii::t('app', 'Купить') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
</section>
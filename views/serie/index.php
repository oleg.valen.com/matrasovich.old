<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->registerMetaTag(['name' => 'description', 'content' => $serie->serieDescriptions->description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $serie->serieDescriptions->keyword]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'index, follow']);
$this->title = $serie->serieDescriptions->title;
?>

<?php if (!empty($products) && count($products['products']) > 1): ?>
    <section class="section container content">
        <header><h1 class="title is-size-4"><?= $serie->serieDescriptions->h1 ?></h1></header>
        <div class="table-container">
            <table class="is-fullwidth is-hoverable is-striped table is-size-7">
                <thead>
                <tr>
                    <th></th>
                    <?php foreach ($products['products'] as $item): ?>
                        <th>
                            <a href="<?= Url::to("/{$item->seo_url}/p{$item->product_id}"); ?>"
                               title="<?= $item['name'] ?>"><img
                                        src="<?= "/{$item->getImage()->getPath('250x250')}" ?>"
                                        alt="<?= $item->name ?>"/></a>
                        </th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Наименование</th>
                    <?php foreach ($products['products'] as $item): ?>
                        <td>

                            <a class="has-text-link"
                               href="<?= Url::to("/{$item->seo_url}/p{$item->product_id}") ?>"
                               title="<?= $item['name'] ?>"><?= $item['name'] ?></a>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <?php foreach ($products['details'] as $key => $details): ?>
                    <tr>
                        <th><?= $key; ?></th>
                        <?php foreach ($details as $id => $detail): ?>
                            <td>
                                <?php
                                end($detail);
                                $lastElementKey = key($detail);
                                foreach ($detail as $k => $v) {
                                    echo $v;
                                    if ($k !== $lastElementKey) {
                                        echo '<br>';
                                    }
                                }
                                ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
<?php endif; ?>





<?php if ($serie->serieDescriptions->text_description): ?>
    <section class="container content section">
        <?= $serie->serieDescriptions->text_description ?>
    </section>
<?php endif; ?>

<?php

namespace app\components;

use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $language = Yii::$app->request->cookies->getValue('_language', 'ru');
        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'language', 'value' => $language]));
//        Yii::$app->language = $language;
        $app->language = $language;
    }
}
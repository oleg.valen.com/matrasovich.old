<?php

namespace app\components;

use app\models\Category;
use app\models\Product;
use yii\web\UrlRuleInterface;
use app\models\Blog;
use Yii;
use yii\helpers\Url;
use app\models\Serie;

class MyUrlRule implements UrlRuleInterface
{

    public function createUrl($manager, $route, $params)
    {
        if (!$route) return false;
        $parts = explode('/', $route);
        if (count($parts) == 2) {
            if ($parts[0] == 'category') {
                if ($parts[1] == 'index') {
                    if (!$params) return false;
//                     filter
                    $url = $params['category'] . '/c' . $params['category_id'];
                    $filter = '';
                    if (isset($params['filter'])) {
                        foreach ($params as $key => $value) {
                            if (substr_compare($key, 'Filter', -6) === 0) {
                                $filter .= substr($key, 0, -6) . "={$value};";
                            }
                        }
                        $filter = '/' . rtrim($filter, ';');
                    }

                    // params
                    $urlParams = '';
                    $flag = true;
                    if (isset($params['sort'], $params['page'])) {
                        $params_keys = array_keys($params);
                        if (array_search('sort', $params_keys) > array_search('page', $params_keys)) {
                            $urlParams = "?sort={$params['sort']}";
                            $flag = false;
                        }
                    }
                    if ($flag) {
                        foreach ($params as $key => $value) {
                            if (!($key == 'sort' or $key == 'page')) continue;
                            if (isset($value))
                                $urlParams .= "$key=$value&";
                        }
                        $urlParams = empty($urlParams) ? '' : '?' . rtrim($urlParams, '&');
                    }

                    return "{$url}{$filter}{$urlParams}";
                }
            } elseif ($parts[0] == 'product' && $parts[1] == 'index') {
                if (!$params) return false;
                $model = Product::findOne($params['product_id']);
                return $model->seo_url . '/p' . $model->product_id . ($params['isSized'] ? '?s=' . $params['s'] : '');
            } elseif ($parts[0] == 'blog' && $parts[1] = 'view') {
                if (!$params) return false;
                $model = Blog::findOne($params['blog_id']);
                return $model->seo_url . '/b' . $model->blog_id;
            }
        }
        $model = Category::findOne($parts[1]);
        return $model->seo_url . '/c' . $model->category_id;

    }


    public function parseRequest($manager, $request)
    {
        ///toppery/filter/category=premium,nedorogie;size=90x190

        $pathInfo = $request->getPathInfo();
        //$data = htmlspecialchars($data, ENT_COMPAT, 'UTF-8');

        $parts = explode('/', $pathInfo);

        if (count($parts) == 1) {
            //one word
            $count = Category::find()->where(['seo_url' => $parts[0]])->count();
            if ($count) {
                return ['category/index', array('category' => $parts[0])];
            }
            $count = Product::find()->where(['seo_url' => $parts[0]])->count();
            if ($count) {
                return ['product/index', array('product' => $parts[0])];
            }
        } elseif (count($parts) == 2) {
//            return ['blog/blog', array('blog' => $parts[1])];
            $letter = strtolower(substr($parts[1], 0, 1));
            $id = substr($parts[1], 1);
            if ($letter == 'p') {
                $item = Product::findOne($id);
                if ($item) {
                    return ['product/index', ['product_id' => $id]];
                }
            } elseif ($letter == 'c') {
                $item = Category::findOne($id);
                if ($item) {
                    return ['category/index', ['category' => $item->seo_url, 'category_id' => $id]];
                }
            } elseif ($letter == 'b') {
                $item = Blog::findOne($id);
                if ($item) {
                    return ['blog/blog', ['blog' => $item->seo_url, 'blog_id' => $id]];
                }
            } elseif ($letter == 's') {
                $item = Serie::findOne($id);
                if ($item) {
                    return ['serie/index', ['serie' => $item->seo_url, 'serie_id' => $id]];
                }
            }
//        } elseif ($parts[2] == 'filter') {
        } elseif (count($parts) == 3 && strpos($parts[2], '=') !== false && strpos($parts[1], 'filter') === false) {
//            if (Yii::$app->request->referrer == Url::home(true)) {
//                $cookies = Yii::$app->response->cookies;
//                $cookies->add(new \yii\web\Cookie([
//                    'name' => 'btn-filter',
//                    'value' => 'filter',
//                ]));
//            }
            $id = substr($parts[1], 1);
            $params = ['category' => $parts[0], 'category_id' => $id, 'filter' => ''];
            $filters = explode(';', $parts[2]);
            foreach ($filters as $filter) {
                $attributeGroup = explode('=', $filter);
                $params[$attributeGroup[0] . 'Filter'] = $attributeGroup[1];
            }
            return ['category/index', $params];
        }

        if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
            // Ищем совпадения $matches[1] и $matches[3]
            // с данными manufacturer и model в базе данных
            // Если нашли, устанавливаем $params['manufacturer'] и/или $params['model']
            // и возвращаем ['car/index', $params]
        }
        return false;  // данное правило не применимо
    }
}
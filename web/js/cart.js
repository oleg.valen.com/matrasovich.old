(function () {
    document.addEventListener('DOMContentLoaded', function () {

        //................................................initial.......................................................

        //................................................toggle type service...........................................
        let form = document.querySelector('#cart-form');
        let inputs = form.querySelectorAll('input[name="CartForm[serviceType]"]:not([type="hidden"])');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('change', function (e) {
                let typeService = e.target.value;
                let items = document.querySelectorAll('.cart-form .cart-form__item');
                for (let j = 0; j < items.length; j++) {
                    items[j].classList.toggle('active');
                }
            });
        }

        //................................................focus, blur inputs by hand....................................
        const inputsByHand = document.querySelectorAll('.cart-form .cart-form__by-hand');

        for (let i = 0; i < inputsByHand.length; i++) {
            inputsByHand[i].addEventListener('focusout', function (e) {
                let typeService = document.querySelectorAll('input[name="CartForm[serviceType]"]:not([type="hidden"]):checked')[0].defaultValue;
                if (e.currentTarget.querySelector('input').value !== ''
                    && ((typeService == 'DoorsWarehouse' && e.currentTarget.classList.contains('cart-form__warehouse')) ||
                        (typeService == 'DoorsDoors' && e.currentTarget.classList.contains('cart-form__doors')))) {
                    e.currentTarget.querySelector('p').textContent = '';
                }
            });
        }

        //.......................................................city-search............................................
        let handlers = document.querySelectorAll('.column[data-search-handler]');
        let searchContainer = document.querySelector('.search__container');

        function openLetterSearch() {
            let html = '';
            let div = document.createElement('div');
            let input = this.querySelector('input');
            // let coords = getCoords(self);

            let attr = this.getAttribute('data-search-handler');
            if (attr == 'city') {
                if (input.value.length <= 2)
                    return;
                searchContainer.querySelector('.search__warehouse').value = '';
                searchContainer.querySelector('.search__street').value = '';
                let data = '_csrf=' + yii.getCsrfToken() +
                    '&city=' + encodeURIComponent(input.value);
                // searchContainer.querySelector('.column[data-search-handler="city"] .control').classList.add('is-loading');
                this.querySelector('.control').classList.add('is-loading');
                div.className = 'letter-search letter-search__city';
                ajax.call(this, 'POST', '/cart/get-cities', data, function (response) {
                    this.querySelector('.control').classList.remove('is-loading');
                    deleteLetterSearch();
                    if (response.success === true && Object.keys(response.data).length) {
                        for (let area in response.data) {
                            html += `<div class="letter-search__area">${area} обл.</div>`;
                            let item = response.data[area];
                            for (let j = 0; j <= item.length - 1; j++) {
                                html += `<div class="letter-search__city-item" data-ref="${item[j].ref}">${item[j].description} ${item[j].type}</div>`;
                            }
                        }
                    } else {
                        html += '<div class="letter-search__city-item letter-search__not-found">По запросу ничего не найдено</div>';
                        div.style.overflowY = 'unset';
                    }
                    div.innerHTML = html;
                    // div.style.width = self.offsetWidth - 24 + 'px';
                    div.style.display = 'block';
                    this.append(div);
                });
            } else if (attr == 'warehouse') {
                let data = '_csrf=' + yii.getCsrfToken() +
                    '&cityRef=' + encodeURIComponent(searchContainer.querySelector('input.search__city').dataset.ref);
                this.querySelector('.control').classList.add('is-loading');
                deleteLetterSearch();
                div.className = 'letter-search letter-search__warehouse';
                ajax.call(this, 'POST', '/cart/get-warehouses', data, function (response) {
                    this.querySelector('.control').classList.remove('is-loading');
                    if (response.success === true) {
                        if (response.data.length) {
                            for (let i = 0; i <= response.data.length - 1; i++) {
                                html += `<div class="letter-search__warehouse-item" data-ref="${response.data[i].ref}">${response.data[i].description}</div>`;
                            }
                        } else {
                            html += '<div class="letter-search__warehouse-item letter-search__not-found">Не найдены грузовые отделения</div>';
                            div.style.overflowY = 'unset';
                        }
                    } else {
                        html += '<div class="letter-search__warehouse-item letter-search__not-found">По запросу ничего не найдено</div>';
                        div.style.overflowY = 'unset';
                    }
                    div.innerHTML = html;
                    // div.style.width = self.offsetWidth - 24 + 'px';
                    div.style.display = 'block';
                    this.append(div);
                });
            } else if (attr == 'street') {
                if (input.value.length <= 2)
                    return;
                // searchContainer.querySelector('.search__warehouse').value = '';
                // searchContainer.querySelector('.search__street').value = '';
                let data = '_csrf=' + yii.getCsrfToken() +
                    '&cityRef=' + encodeURIComponent(searchContainer.querySelector('input.search__city').dataset.ref) +
                    '&street=' + encodeURIComponent(input.value);
                this.querySelector('.control').classList.add('is-loading');
                div.className = 'letter-search letter-search__street';
                ajax.call(this, 'POST', '/cart/get-streets', data, function (response) {
                    this.querySelector('.control').classList.remove('is-loading');
                    deleteLetterSearch();
                    if (response.success === true) {
                        if (response.data.length) {
                            for (let i = 0; i <= response.data.length - 1; i++) {
                                html += `<div class="letter-search__street-item" data-ref="${response.data[i].ref}">${response.data[i].description}</div>`;
                            }
                        }
                    } else {
                        html += '<div class="letter-search__street-item letter-search__not-found">По запросу ничего не найдено</div>';
                        div.style.overflowY = 'unset';
                    }
                    div.innerHTML = html;
                    // div.style.width = self.offsetWidth - 24 + 'px';
                    div.style.display = 'block';
                    this.append(div);
                });
            }
        }

        function hiddenSearch(e) {
            let letterSearch = document.querySelector('.letter-search');
            if (letterSearch == null) return;
            for (let i = 0; i < handlers.length; i++) {
                if (!handlers[i].contains(e.target) && (!letterSearch.contains(e.target))) {
                    letterSearch.style.display = 'none';
                    //     for (let i = 0; i < producersItem.length; i++) {
                    //         producersItem[i].classList.remove('active');
                    //     }
                }
            }
        }

        for (let i = 0; i < handlers.length; i++) {
            handlers[i].addEventListener('input', function () {
                setTimeout(openLetterSearch.bind(this), 1000);
            });
        }

        document.addEventListener("mouseup", hiddenSearch.bind(this));

        document.addEventListener('click', function (e) {

            let input;
            if (e.target && !e.target.classList.contains('letter-search__not-found')) {
                let item = e.target;
                if (e.target.classList.contains('letter-search__city-item')) {
                    input = searchContainer.querySelector('input.search__city');
                    input.value = e.target.textContent;
                    input.dataset.ref = e.target.dataset.ref;
                    deleteLetterSearch();
                    openLetterSearch.call(document.querySelector('.search__container .column[data-search-handler="warehouse"]'));
                } else if (e.target.classList.contains('letter-search__warehouse-item')) {
                    input = searchContainer.querySelector('input.search__warehouse');
                    input.value = e.target.textContent;
                    input.dataset.ref = e.target.dataset.ref;
                    deleteLetterSearch();
                } else if (e.target.classList.contains('letter-search__street-item')) {
                    input = searchContainer.querySelector('input.search__street');
                    input.value = e.target.textContent;
                    input.dataset.ref = e.target.dataset.ref;
                    deleteLetterSearch();
                }
            }
        });

        searchContainer.querySelector('.search__warehouse').addEventListener('focus', function () {
            openLetterSearch.call(document.querySelector('.search__container .column[data-search-handler="warehouse"]'));
        });

        //.......................................................common.................................................
        function getCoords(elem) {
            let box = elem.getBoundingClientRect();

            return {
                top: box.top + pageYOffset,
                left: box.left + pageXOffset
            };
        }

        function deleteLetterSearch() {
            let letterSearch = searchContainer.querySelector('.letter-search');
            if (letterSearch !== null) {
                letterSearch.remove();
            }
        }


    });
})();


//города компании
// {
//     "modelName": "Address",
//     "calledMethod": "getCities",
//     "methodProperties": {
//     "FindByString": "бровари"
// },
//     "apiKey": "7d26a9bf0a844feb6ceb52dbc10cafdd"
// }
//бровари db5c88d7-391c-11dd-90d9-001a92567626

//улицы компании
// {
//     "modelName": "Address",
//     "calledMethod": "getStreet",
//     "methodProperties": {
//     "CityRef": "db5c88d7-391c-11dd-90d9-001a92567626",
//         "FindByString": "незалежн"
// },
//     "apiKey": "7d26a9bf0a844feb6ceb52dbc10cafdd"
// }

//отделения
// {
//     "modelName": "AddressGeneral",
//     "calledMethod": "getWarehouses",
//     "methodProperties": {
//     "CityRef": "db5c88d7-391c-11dd-90d9-001a92567626"
// },
//     "apiKey": "7d26a9bf0a844feb6ceb52dbc10cafdd"
// }
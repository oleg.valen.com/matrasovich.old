$(document).ready(function () {
        "use strict";

        function changeSizeSelect(attributeId) {
            $("select[id^='size-option']").val(attributeId);
            let optionSelected = $('option:selected', this);
            var priceText = optionSelected.attr('data-price');
            var price = parseInt($('option:selected', this).attr('data-price').replace(/\s/g, ''));
            var discount = $('.ok-product-old-price').data('sticker-discount');
            $('.ok-product-price').text(priceText);
            if ($('.ok-product-old-price').length) {
                $('.ok-product-old-price').text(Math.round(price / ((100 - discount) / 100)).toLocaleString('ru') + ' грн.');
            }

            let extra = $(document).find('.product_right__extra');
            extra.find('.delivery-warehouse').text(optionSelected.attr('data-delivery-warehouse'));
            extra.find('.delivery-doors').text(optionSelected.attr('data-delivery-doors'));
        }

        $(document).ready(function () {
            var attributeId = searchParams('s');
            if (attributeId) {
                changeSizeSelect(attributeId);
            }
        });

        $(document).ready(function () {
            // Check for click events on the navbar burger icon
            $(".navbar-burger-a").click(function () {
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger-a").toggleClass("is-active");
                $(".navbar-menu-a").toggleClass("is-active");
            });
        });

        $(document).ready(function () {
            // Check for click events on the navbar burger icon
            $(".navbar-burger-b").click(function () {
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger-b").toggleClass("is-active");
                $(".navbar-menu-b").toggleClass("is-active");
            });
        });

        $('.ok-main-btn-search').on('click', function (e) {
            e.preventDefault();
            document.location.href = '/search';
        });

        $(".hero-foot ul li").hover(function () {
            $(".ok-main-tile").addClass("is-active");
        }, function () {
            $(".ok-main-tile").removeClass("is-active");
        });

        $(document).ready(function () {
            // Configure/customize these variables.
            var showChar = 200;  // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "Читать еще...";
            var lesstext = "Скрыть";


            $('.more').each(function () {
                var content = $(this).html();
                if (content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);
                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink has-text-link">' + moretext + '</a></span>';
                    $(this).html(html);
                }

            });

            $(".morelink").click(function () {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });

        /******************************
         BOTTOM SCROLL TOP BUTTON
         ******************************/

        var scrollTop = $(".scroll-top");
        $(window).scroll(function () {
            var topPos = $(this).scrollTop();
            // if user scrolls down - show scroll to top button
            if (topPos > 100) {
                $(scrollTop).css("opacity", "1");
            } else {
                $(scrollTop).css("opacity", "0");
            }
        });

        $(scrollTop).click(function () {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $(document).on('click', 'button.delete', function (e) {
            $(this).closest('.notification').hide();
        });

        $("select[id^='size-option']").on('change', function () {
            changeSizeSelect($('option:selected', this).val());
        });

        $(".modal .delete").click(function () {
            $('.modal').removeClass("is-active");
        });

        $(".ok-product-nostandart").click(function () {
            $(".modal.product-nostandart-modal").addClass("is-active");
        });

        $('#width').bind('input', function () {
            $('#total').val(Number(($('#price').val() * $(this).val() * $('#length').val() / 10000).toFixed(2)));
        });

        $('#length').bind('input', function () {
            $('#total').val(Number(($('#price').val() * $(this).val() * $('#width').val() / 10000).toFixed(2)));
        });

        $(document).click(function (e) {
            var doc = $(document);
            var modal = doc.find('.modal');
            if (modal.hasClass('is-active')) {
                if (!$(e.target).closest(".modal-card,.product-icon-info,.ok-product-nostandart,.product_review-give-review").length) {
                    modal.removeClass("is-active");
                }
            }
        });

        $('.ok-cart-delete-all').click(function () {
            localStorage.removeItem('cart');
            navigateToUrl(window.location.origin.concat('/cart'));
        });

        $('.ok-main-btn-filter').click(function () {
            var self = $(this);
            $('.ok-main-filter').slideToggle('slow', function () {
                var icon = self.find('svg');
                var iconAttr = icon.attr('data-icon');
                if (iconAttr === "times") {
                    icon.attr('data-icon', 'filter');
                    setCookie('btn-filter', 'filter', 30);
                } else {
                    icon.attr('data-icon', 'times');
                    setCookie('btn-filter', 'times', 30);
                    $('.ok-main-filter').removeClass('is-hidden-mobile');
                }
            });
        });

        $(".size input[type='radio']").click(function () {
            var elements = $(".size input[type='radio']").not($(this));
            elements.each(function () {
                $(this).prop('checked', false);
            });
        });

        $(".sort input[type='radio']").click(function () {
            var elements = $(".sort input[type='radio']").not($(this));
            elements.each(function () {
                $(this).prop('checked', false);
            });
        });

        // $(document).ready(function () {
        $(".selection select").on('change', function () {
            $('form').append("<input type='hidden' name='category-id' value='" + $('option:selected', $(this)).val() + "' />");
            $('.selection.button').submit();
        });
        // });

        $('.product-icon-info').on('click', function (e) {
            var modal = $(document).find('.modal.product-info-modal');
            var card = modal.find('.modal-card-body');
            var self = $(this);
            var product_id = self.data('product-id');
            var detail_id = self.data('detail-id');
            var html = '';

            $.ajax({
                url: '/product/info',
                data: {product_id: product_id, detail_id: detail_id},
                type: 'POST',
                dataType: 'json',
                success: function (e) {
                    modal.find('.modal-card-title').text(e.key);
                    if (e.imageSrc !== '') {
                        html = "<figure class='has-text-centered'><img src='" + e.imageSrc + "' alt='" + e.imageAlt + "'></figure>";
                    }
                    if (e.text !== '') {
                        html += "<div class='text'>" + e.text + "</div>";
                    }
                    card.html(html);
                    modal.addClass('is-active');
                }
            });
        });

        $('.ok-product-on-credit').on('click', function (e) {
            e.preventDefault();
            var doc = $(document);
            var monthCount = parseInt(doc.find('#month-count').val()) + 1;
            var price = $('.ok-product-price').html().replace(' ', '');
            price = parseInt(price.substr(0, price.length - 5));
            var name = doc.find('h1').text();
            $.ajax({
                url: '/product/oncredit',
                data: {monthCount: monthCount, price: price, name: name},
                type: 'POST',
                // dataType: 'json',
                success: function (e) {
                }
            });
        });

        $('#month-count').bind('input', function () {

            var monthCount = parseInt($(this).val());
            var price = $('.ok-product-price').html().replace(' ', '');
            price = parseInt(price.substr(0, price.length - 5));
            var resCalc = PP_CALCULATOR.calculatePhys(monthCount, price);
            $('#month-count-calculated').text(monthCount <= 0 ? '' : resCalc.payCount);
            $('#amount-calculated').text(monthCount <= 0 ? '' : resCalc.ipValue);
        });

        // $('.ok-product-compare,.ok-product-favorite').on('click', function (e) {
        $(document).on('click', '.ok-product-compare,.ok-product-favorite', function (e) {
            var self = $(this);
            var origin = window.location.origin;
            let language = document.querySelector('input[name="_language"]').value;
            language = (language == 'ru' ? '' : '/' + language);

            if (self.hasClass('ok-product-compare')) {
                var url = 'compare';
                var storageName = 'comparisons';
            } else if (self.hasClass('ok-product-favorite')) {
                var url = 'favorites';
                var storageName = 'favorites';
            } else {
                return;
            }

            if (self.hasClass('is-primary')) {
                e.preventDefault();
                navigateToUrl(origin.concat(language + '/' + url));
                return;
            }

            var product_id = self.find('span').data('product-id');
            var storage = localStorage.getItem(storageName);
            if (storage == null)
                storage = '';

            self.toggleClass('is-warning is-primary');

            storage = JSON.parse("[" + storage + "]");
            const index = storage.indexOf(product_id);
            if (index > -1) {
                storage.splice(index, 1);
            } else {
                storage.push(product_id);
            }
            localStorage.setItem(storageName, storage.toString());
        });

        $(document).on('click', '.ok-product-cart', function (e) {
            var self = $(this);
            var origin = window.location.origin;
            var url = '/cart';
            var storageName = 'cart';

            if (self.hasClass('is-primary')) {
                e.preventDefault();
                navigateToUrl(origin.concat(url));
                return;
            }

            var product_id = self.data('product-id');
            var attribute_id = $('option:selected', $('.select')).val();
            if (attribute_id == undefined)
                attribute_id = self.data('attribute-id');

            var storage = localStorage.getItem(storageName);
            if (storage == null) {
                storage = {};
            } else {
                storage = JSON.parse(storage);
            }

            self.toggleClass('is-warning is-primary');
            self.text('В корзину');

            if (storage[product_id]) {
                if (storage[product_id][attribute_id]) {
                    storage[product_id][attribute_id] += 1;
                } else {
                    storage[product_id][attribute_id] = 1;
                }
            } else {
                storage[product_id] = {};
                storage[product_id][attribute_id] = 1;
            }

            localStorage.setItem(storageName, JSON.stringify(storage));

        });

        $(document).on('click', '.ok-cart-delete', function (e) {
            var self = $(this);
            var origin = window.location.origin;
            var url = '/cart';
            var storageName = 'cart';
            var product_id = self.find('a').data('product-id');
            var attribute_id = self.find('a').data('attribute-id');

            var storage = localStorage.getItem(storageName);
            if (storage == null) {
                return;
            } else {
                storage = JSON.parse(storage);
            }

            if (storage[product_id]) {
                if (storage[product_id][attribute_id]) {
                    delete storage[product_id][attribute_id];
                    if (isEmpty(storage[product_id])) {
                        delete storage[product_id];
                    }
                }
            }

            localStorage.setItem(storageName, JSON.stringify(storage));
            navigateToUrl(origin.concat(url));
        });

        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    return false;
                }
            }

            return JSON.stringify(obj) === JSON.stringify({});
        }

        $(document).on('click', '.ok-cart-minus', function (e) {
            var self = $(this);
            var addons = self.parent().parent().find('.ok-cart-addons');
            var product_id = addons.data('product-id');
            var attribute_id = addons.data('attribute-id');
            var input = self.parent().find('input');
            var val = parseInt(input.val());
            if (val - 1 > 0) {
                input.val(val - 1);
                calcCartStorage(product_id, attribute_id, -1);
                calcCart();
            }
        });

        $(document).on('click', '.ok-cart-plus', function (e) {
            var self = $(this);
            var addons = self.parent().parent().find('.ok-cart-addons');
            var product_id = addons.data('product-id');
            var attribute_id = addons.data('attribute-id');
            var input = self.parent().find('input');
            var val = parseInt(input.val());
            input.val(val + 1);
            calcCartStorage(product_id, attribute_id, 1);
            calcCart();
        });

        $(document).on('focusin', '.ok-cart-input', function () {
            $(this).data('prev-value', $(this).val());
        }).on('change', '.ok-cart-input', function () {
            var self = $(this);
            var prevValue = self.data('prev-value');
            var addons = self.parent().parent().parent().find('.ok-cart-addons');
            var product_id = addons.data('product-id');
            var attribute_id = addons.data('attribute-id');
            var input = self.parent().find('input');
            var val = parseInt(input.val());
            if (val <= 0) {
                input.val(prevValue);
                return;
            }
            ;
            input.val(val);
            calcCartStorage(product_id, attribute_id, val, true);
            calcCart();
        });

        function calcCartStorage(product_id, attribute_id, amount, isNew = false) {
            var storage = localStorage.getItem('cart');
            if (storage == null) {
                return;
            } else {
                storage = JSON.parse(storage);
            }
            if (storage[product_id]) {
                if (storage[product_id][attribute_id]) {
                    storage[product_id][attribute_id] = !isNew ? storage[product_id][attribute_id] + amount : amount;
                }
                localStorage.setItem('cart', JSON.stringify(storage));
            }
        }

        function calcCart() {

            var doc = $(document);
            var amount = 0;
            var sum = 0;
            $(".ok-cart-row").each(function (index) {
                var self = $(this);
                var rowAmount = parseInt(self.find('.ok-cart-input').val());
                var rowPrice = parseInt(self.find('.ok-cart-row-price').text());
                var rowSum = rowAmount * rowPrice;
                self.find('.ok-cart-row-sum').text(rowSum);
                amount += rowAmount;
                sum += rowSum;
            });
            doc.find('.ok-cart-amount').text(amount);
            doc.find('.ok-cart-sum').text(sum);
        }

        // function updateComparisons() {
//
//     if ($('.ok-product-compare').length == 0) {
//         return;
//     }
//
//     var comparisons = localStorage.getItem('comparisons');
//     if (comparisons == null)
//         comparisons = '';
//     comparisons = JSON.parse("[" + comparisons + "]");
//
//     $(".ok-product-compare").each(function (index) {
//         var self = $(this);
//         var product_id = self.find('span').data('product-id');
//         if (comparisons.indexOf(product_id) != -1) {
//             self.removeClass('is-warning').addClass('is-primary');
//         }
//     });
// }

        function updateComparisonsFavorites(className, storageName) {

            if ($(className).length == 0) {
                return;
            }

            var storage = localStorage.getItem(storageName);
            if (storage == null)
                storage = '';
            storage = JSON.parse("[" + storage + "]");

            $(className).each(function (index) {
                var self = $(this);
                var product_id = self.find('span').data('product-id');
                if (storage.indexOf(product_id) != -1) {
                    self.removeClass('is-warning').addClass('is-primary');
                }
            });
        }

        function navigateToUrl(url) {
            var f = document.createElement("form");
            f.action = url;

            var indexQM = url.indexOf("?");
            if (indexQM >= 0) {
                // the URL has parameters => convert them to hidden form inputs
                var params = url.substring(indexQM + 1).split("&");
                for (var i = 0; i < params.length; i++) {
                    var keyValuePair = params[i].split("=");
                    var input = document.createElement("input");
                    input.type = "hidden";
                    input.name = keyValuePair[0];
                    input.value = keyValuePair[1];
                    f.appendChild(input);
                }
            }

            document.body.appendChild(f);
            f.submit();
        }

        function onLoadCompare() {

            if (!window.location.href.includes('/compare')) {
                return;
            }

            var comparisons = localStorage.getItem('comparisons');
            if (comparisons == null)
                return;

            $.ajax({
                url: '/site/compare',
                data: {comparisons: comparisons},
                type: 'POST',
                success: function (e) {
                    $('.table-container').append(e);
                    updateComparisonsFavorites('.ok-product-favorite', 'favorites');
                }
            });
        }

        function onLoadFavorites() {

            if (!window.location.href.includes('/favorites')) {
                return;
            }

            var favorites = localStorage.getItem('favorites');
            if (favorites == null)
                return;

            $.ajax({
                url: '/site/favorites',
                data: {favorites: favorites},
                type: 'POST',
                success: function (e) {
                    $('.ok-product-favorites').append(e);
                    updateComparisonsFavorites('.ok-product-compare', 'comparisons');
                },
                error: function (request, status, error) {
                },
            });
        }

        function onLoadCart() {

            if (!window.location.href.includes('/cart')) {
                return;
            }

            var cart = localStorage.getItem('cart');
            if (cart == null)
                return;

            $.ajax({
                url: '/cart',
                // data: {cart: "{\"72\":{\"28\":1, \"29\":2}}"},
                data: {cart: cart},
                type: 'POST',
                success: function (e) {
                    $('.table__cart').append(e);
                    // updateComparisonsFavorites('.ok-product-compare', 'comparisons');
                },
                error: function (request, status, error) {
                },
            });

        }

        $(document).on('click', '.ok-cart-order', function (e) {

            e.preventDefault();
            var cart = localStorage.getItem('cart');
            // if (cart == null)
            //     return;

            var doc = $(document);
            var modal = doc.find('.ok-modal-cart');
            var phone = doc.find('#cartform-telephone').val();
            let form = doc.find('#cart-form');

            $.ajax({
                url: '/cart/order',
                data: {form: form.serialize(), cart: cart},
                type: 'POST',
                success: function (e) {
                    const response = JSON.parse(e);
                    if (response.success === false && !isEmpty(response)) {
                        for (let prop in response.data) {
                            const input = form.find('input[name="CartForm[' + prop + ']"]');
                            const help = input.find('~ p');
                            help.text(response.data[prop]);
                        }
                        return;
                    }
                    modal.addClass('is-active');
                    localStorage.removeItem('cart');
                    setTimeout(function () {
                        modal.removeClass('is-active');
                        window.location.reload(false);
                    }, 3000);
                },
                error: function (request, status, error) {
                },
            });
        });

        function onLoadPageLastSeenProducts() {

            var storage = localStorage.getItem('lastSeenProducts');
            if (storage == null)
                storage = '';

            $.ajax({
                url: '/product/getproducts',
                data: {lastSeenProducts: storage},
                type: 'POST',
                success: function (e) {
                    $('.ok-last-seen-products').append(e);
                },
                error: function (request, status, error) {
                },
            });

            var href = window.location.href;
            var regExp = new RegExp('\/p[0-9]+');
            if (regExp.test(href)) {
                var product_id = $(document).find('.ok-product-compare span').data('product-id');
                var storage = localStorage.getItem('lastSeenProducts');
                if (storage == null)
                    storage = '';
                storage = JSON.parse("[" + storage + "]");

                if (!storage.includes(product_id)) {
                    if (storage.length >= 12) {
                        storage.splice(-1, storage.length - 11);
                    }
                    storage.unshift(product_id);
                    localStorage.setItem('lastSeenProducts', storage.toString());
                }
            }
        }

        function onLoadPageCompareFavoritesCart() {

            let length = 0;
            // let el = 0;
            const favoritesElems = document.querySelectorAll('.icons-header__favorites');
            const compareElems = document.querySelectorAll('.icons-header__compare');
            const cartElems = document.querySelectorAll('.icons-header__cart');
            const favorites = localStorage.getItem('favorites');
            const comparisons = localStorage.getItem('comparisons');
            let cart = localStorage.getItem('cart');

            if (favorites !== null) {
                length = JSON.parse('[' + localStorage.favorites + ']').length;
                if (length !== 0) {
                    favoritesElems.forEach(el => {
                        el.setAttribute('data-badge', length);
                    });
                }
            }

            if (comparisons !== null) {
                length = JSON.parse('[' + localStorage.comparisons + ']').length;
                if (length !== 0) {
                    compareElems.forEach(el => {
                        el.setAttribute('data-badge', length);
                    });
                }
            }

            length = 0;
            if (cart !== null) {
                cart = JSON.parse('[' + localStorage.cart + ']');
                for (let product in cart) {
                    for (let option in cart[product]) {
                        for (let amount in cart[product][option]) {
                            length += cart[product][option][amount];
                        }
                    }
                }
                if (length !== 0) {
                    cartElems.forEach(el => {
                        el.setAttribute('data-badge', length);
                    });
                }
            }
        }

        $(document).on('click', '.ok-compare-delete,.ok-favorite-delete', function (e) {

            var self = $(this);
            var origin = window.location.origin;
            var product_id = self.data('product-id');

            if (self.hasClass('ok-compare-delete')) {
                var url = 'compare';
                var storageName = 'comparisons';
            } else if (self.hasClass('ok-favorite-delete')) {
                var url = 'favorites';
                var storageName = 'favorites';
            } else {
                return;
            }

            var storage = localStorage.getItem(storageName);

            if (storage == null)
                storage = '';

            storage = JSON.parse("[" + storage + "]");
            const index = storage.indexOf(product_id);
            if (index > -1) {
                storage.splice(index, 1);
                // } else {
                //     comparisons.push(product_id);
            }
            localStorage.setItem(storageName, storage.toString());
            navigateToUrl(origin.concat('/' + url));
        });

        const giveReview = document.querySelector('.product_review-give-review');
        if (giveReview) {
            giveReview.addEventListener('click', function () {
                const modal = document.querySelector('.product_review-modal');
                modal.classList.add('is-active');
            });
        }

        const reviewImages = document.querySelectorAll('.product_review__image');
        if (reviewImages) {
            reviewImages.forEach(function (elem) {
                elem.addEventListener('click', handleReviewImages);
            });
        }

        function handleReviewImages() {
            const request = new XMLHttpRequest();
            const url = "/product/get-review-images";
            const params = "data=" + JSON.stringify({reviewId: this.dataset.reviewId});
            const modal = document.querySelector('.product-info-modal');
            // request.responseType = "text";
            request.open("POST", url, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            request.send(params);
            request.onload = function (e) {
                const response = JSON.parse(this.responseText);
                modal.querySelector('.modal-card-title').textContent = response.name + ', ' + response.created_at;
                modal.querySelector('.modal-card-body').innerHTML = response.images;
                modal.classList.add('is-active');
            };

        }

        const reviewSend = document.querySelector('.product_review__send');
        if (reviewSend) {
            reviewSend.addEventListener('click', function () {
                const files = document.querySelector('#reviewform-imagefiles');
                if (files.files.length !== 0) {
                    this.classList.add('is-loading');
                }
            });
        }

        onLoadPageCompareFavoritesCart();
        onLoadPageLastSeenProducts();
        updateComparisonsFavorites('.ok-product-compare', 'comparisons');
        updateComparisonsFavorites('.ok-product-favorite', 'favorites');
        onLoadCompare();
        onLoadFavorites();
        onLoadCart();

    }
);

document.addEventListener('DOMContentLoaded', function () {
    "use strict";

    // Dropdowns
    var $dropdowns = getAll('.dropdown:not(.is-hoverable)');

    if ($dropdowns.length > 0) {
        $dropdowns.forEach(function ($el) {
            $el.addEventListener('click', function (event) {
                event.stopPropagation();
                $el.classList.toggle('is-active');
            });
        });

        document.addEventListener('click', function (event) {
            closeDropdowns();
        });
    }

    function closeDropdowns() {
        $dropdowns.forEach(function ($el) {
            $el.classList.remove('is-active');
        });
    }

    // Close dropdowns if ESC pressed
    document.addEventListener('keydown', function (event) {
        var e = event || window.event;
        if (e.keyCode === 27) {
            closeDropdowns();
        }
    });

    // Functions
    function getAll(selector) {
        return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
    }

});


function setCookie(name, value, days) {
    $.ajax({
        url: '/site/setcookie',
        data: {name: name, value: value, days},
        type: 'GET',
    });
}

function searchParams(param) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var size = url.searchParams.get(param);
    return size;
}

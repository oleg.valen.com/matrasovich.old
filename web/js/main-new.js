// (function () {
//     document.addEventListener('DOMContentLoaded', function () {

const csrfToken = document.querySelector('meta[name="csrf-token"]').content;

function ajax(method, url, data, func) {
    let self = this;
    let request = new XMLHttpRequest();
    request.open(method, url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    request.responseText = 'json';
    request.send(data);
    request.onload = function (e) {
        func.call(self, JSON.parse(request.response));
    };
}

// });
// })();
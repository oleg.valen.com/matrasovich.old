<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\yii2admin\models\AttributeGroup */

$this->title = 'View Attribute Group: ' . $model->attribute_group_id;
?>
<div id="breadcrumbs">
    <div class="container is-size-6">
        <?= $breadcrumbs ?>
    </div>
</div>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table is-striped is-hoverable is-fullwidth">
        <thead>
        <tr>
            <th colspan="1" class="has-text-centered">Id</th>
            <th colspan="1" class="has-text-centered">Sort_order</th>
            <th colspan="1" class="has-text-centered">Option</th>
            <th colspan="1" class="has-text-centered">Filter</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="has-text-centered"><?= $model->attribute_group_id ?></td>
            <td class="has-text-centered"><?= $model->sort_order ?></td>
            <td class="has-text-centered"><?= $model->option ?></td>
            <td class="has-text-centered"><?= $model->filter ?></td>
        </tr>
        </tbody>
    </table>
    <hr>

    <table class="table is-striped is-hoverable is-fullwidth">
        <thead>
        <tr>
            <th colspan="1" class="has-text-centered">Language_id</th>
            <th colspan="1" class="has-text-centered">Name</th>
            <th colspan="1" class="has-text-centered">Seo_url</th>
            <th colspan="1" class="has-text-centered">Title</th>
            <th colspan="1" class="has-text-centered">Description</th>
            <th colspan="1" class="has-text-centered">Keyword</th>
            <th colspan="1" class="has-text-centered">H1</th>
            <th colspan="1" class="has-text-centered">Nofollow</th>
            <th colspan="1" class="has-text-centered">Noindex</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="has-text-centered"><?= $modelAgd->language_id ?></td>
            <td class="has-text-centered"><?= $modelAgd->name ?></td>
            <td class="has-text-centered"><?= $modelAgd->seo_url ?></td>
            <td class="has-text-centered"><?= $modelAgd->title ?></td>
            <td class="has-text-centered"><?= $modelAgd->description ?></td>
            <td class="has-text-centered"><?= $modelAgd->keyword ?></td>
            <td class="has-text-centered"><?= $modelAgd->h1 ?></td>
            <td class="has-text-centered"><?= $modelAgd->nofollow ?></td>
            <td class="has-text-centered"><?= $modelAgd->noindex ?></td>
        </tr>
        </tbody>
    </table>

</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\yii2admin\models\AttributeGroup */

$this->title = 'Update Attribute Group: ' . $model->attribute_group_id;
?>
<div id="breadcrumbs">
    <div class="container is-size-6">
        <?= $breadcrumbs ?>
    </div>
</div>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('form', [
        'model' => $model,
        'modelAgd' => $modelAgd,
        'nameButton' => 'Изменить',
    ]) ?>
</div>
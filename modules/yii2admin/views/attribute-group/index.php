<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attribute Groups';
?>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <a href="<?= Url::to(['attribute-group/create']) ?>" class="button is-primary is-medium ag-create"
       title="Создать">Создать</a>
    <table class="table is-striped is-hoverable is-fullwidth">
        <thead>
        <tr>
            <th colspan="1" class="has-text-centered">Id</th>
            <th colspan="1" class="has-text-centered">Name</th>
            <th colspan="1" class="has-text-centered">Sort_order</th>
            <th colspan="1" class="has-text-centered">Option</th>
            <th colspan="1" class="has-text-centered">Filter</th>
            <th colspan="1" class="has-text-centered">Update</th>
            <th colspan="1" class="has-text-centered">Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['ag'] as $ag): ?>
            <tr>
                <td class="has-text-centered"><?= $ag->attribute_group_id ?></td>
                <td class="has-text-centered"><?= $ag->attributeGroupDescription->name ?></td>
                <td class="has-text-centered"><?= $ag->sort_order ?></td>
                <td class="has-text-centered"><?= $ag->option ?></td>
                <td class="has-text-centered"><?= $ag->filter ?></td>
                <td class="has-text-centered">
                    <a href="<?= Url::to(['attribute-group/update', 'id' => $ag->attribute_group_id]) ?>"
                       title="Изменить" data-id="<?= $ag->attribute_group_id ?>">
                        <span class="icon has-text-primary"><i class="fas fa-lg fa-edit"></i></span>
                    </a>
                </td>
                <td class="has-text-centered">
                    <a href="<?= Url::to(['attribute-group/delete', 'id' => $ag->attribute_group_id]) ?>"
                       title="Удалить" data-id="<?= $ag->attribute_group_id ?>"
                       class="btn-remove" data-method="post">
                        <span class="icon has-text-primary"><i class="fas fa-lg fa-times-circle"></i></span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

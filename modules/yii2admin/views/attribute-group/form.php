<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\yii2admin\models\AttributeGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="columns">
        <div class="column is-half">

            <div class="field">
                <div class="control">
                    <?= $form->field($model, 'sort_order')->textInput(['autofocus' => true, 'class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($model, 'option')->checkbox(['class' => 'checkbox']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($model, 'filter')->checkbox(['class' => 'checkbox']) ?>
                </div>
            </div>
            <hr>

            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'language_id')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'name')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'seo_url')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'title')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'description')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'keyword')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'h1')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'nofollow')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <?= $form->field($modelAgd, 'noindex')->textInput(['class' => 'input primary']) ?>
                </div>
            </div>


            <div class="field">
                <div class="control">
                    <?= Html::submitButton($nameButton, ['class' => 'button is-primary is-medium']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

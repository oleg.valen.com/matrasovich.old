<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
?>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <a href="<?= Url::to(['product/create']) ?>" class="button is-primary is-medium ag-create"
       title="Создать">Создать</a>
    <table class="table is-striped is-hoverable is-fullwidth">
        <thead>
        <tr>
            <th colspan="1" class="has-text-centered">Id</th>
            <th colspan="1" class="has-text-centered">Category</th>
            <th colspan="1" class="has-text-centered">Name</th>
            <th colspan="1" class="has-text-centered">Image</th>
            <th colspan="1" class="has-text-centered">Price</th>
            <th colspan="1" class="has-text-centered">Sort_order</th>
            <th colspan="1" class="has-text-centered">Status</th>
            <th colspan="1" class="has-text-centered">Seo_url</th>
            <th colspan="1" class="has-text-centered">Copy</th>
            <th colspan="1" class="has-text-centered">Update</th>
            <th colspan="1" class="has-text-centered">Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['p'] as $p): ?>
            <tr>
                <td class="has-text-centered"><?= $p->product_id ?></td>
                <td class="has-text-centered"><?= $p->category->name ?></td>
                <td class="has-text-centered"><?= $p->name ?></td>
                <td class="has-text-centered"><?= $p->image ?></td>
                <td class="has-text-centered"><?= $p->price ?></td>
                <td class="has-text-centered"><?= $p->sort_order ?></td>
                <td class="has-text-centered"><?= $p->status ?></td>
                <td class="has-text-centered"><?= $p->seo_url ?></td>
                <td class="has-text-centered">
                    <a href="<?= Url::to(['product/create', 'id' => $p->product_id]) ?>"
                       title="Копировать" data-id="<?= $p->product_id ?>">
                        <span class="icon has-text-primary"><i class="fas fa-lg fa-copy"></i></span>
                    </a>
                </td>
                <td class="has-text-centered">
                    <a href="<?= Url::to(['product/update', 'id' => $p->product_id]) ?>"
                       title="Изменить" data-id="<?= $p->product_id ?>">
                        <span class="icon has-text-primary"><i class="fas fa-lg fa-edit"></i></span>
                    </a>
                </td>
                <td class="has-text-centered">
                    <a href="<?= Url::to(['product/delete', 'id' => $p->product_id]) ?>"
                       title="Удалить" data-id="<?= $p->product_id ?>"
                       class="btn-remove" data-method="post">
                        <span class="icon has-text-primary"><i class="fas fa-lg fa-times-circle"></i></span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

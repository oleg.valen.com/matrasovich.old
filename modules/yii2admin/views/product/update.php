<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\yii2admin\models\AttributeGroup */

$this->title = 'Update Product: ' . $model->product_id;
?>
<div id="breadcrumbs">
    <div class="container is-size-6">
        <?= $breadcrumbs ?>
    </div>
</div>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('form', [
        'model' => $model,
        'modelPd' => $modelPd,
        'modelPa' => $modelPa,
        'modelPo' => $modelPo,
        'modelPs' => $modelPs,
        'categories' => $categories,
        'nameButton' => 'Изменить',
    ]) ?>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\yii2admin\models\AttributeGroup */

$this->title = 'View Product: ' . $model->product_id;
?>
<div id="breadcrumbs">
    <div class="container is-size-6">
        <?= $breadcrumbs ?>
    </div>
</div>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table is-striped is-hoverable is-fullwidth">
        <thead>
        <tr>
            <th colspan="1" class="has-text-centered">Id</th>
            <th colspan="1" class="has-text-centered">Category_id</th>
            <th colspan="1" class="has-text-centered">Name</th>
            <th colspan="1" class="has-text-centered">Image</th>
            <th colspan="1" class="has-text-centered">Price</th>
            <th colspan="1" class="has-text-centered">Sort_order</th>
            <th colspan="1" class="has-text-centered">Status</th>
            <th colspan="1" class="has-text-centered">Seo_url</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="has-text-centered"><?= $model->product_id ?></td>
            <td class="has-text-centered"><?= $model->category_id ?></td>
            <td class="has-text-centered"><?= $model->name ?></td>
            <td class="has-text-centered"><?= $model->image ?></td>
            <td class="has-text-centered"><?= $model->price ?></td>
            <td class="has-text-centered"><?= $model->sort_order ?></td>
            <td class="has-text-centered"><?= $model->status ?></td>
            <td class="has-text-centered"><?= $model->seo_url ?></td>
        </tr>
        </tbody>
    </table>

</div>
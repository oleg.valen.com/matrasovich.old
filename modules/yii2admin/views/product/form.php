<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\ckeditor\CKEditor;

//use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\yii2admin\models\AttributeGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="columns">
        <div class="column is-two-thirds">

            <div class="box">
                <p class="subtitle has-text-weight-bold">Images</p>
                <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
            </div>

            <div class="field">
                <div class="control">
                    <?= Html::submitButton($nameButton, ['class' => 'button is-primary is-medium']) ?>
                </div>
            </div>

            <div class="box">
                <p class="subtitle has-text-weight-bold">Product</p>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'category_id', ['template' => "{label}\n<div class=\"select is-primary\" style='display: grid'>{input}</div>\n{hint}\n{error}"])->dropDownList($categories)->label('Category'); ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'name')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'image')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'price')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'sort_order')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'status')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($model, 'seo_url')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
            </div>

            <div class="box">
                <p class="subtitle has-text-weight-bold">Product Description</p>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'language_id')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'name')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'text_description')->widget(CKEditor::className(), [
                            'editorOptions' => [
                                'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                'inline' => false, //по умолчанию false
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'title')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'h1')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'description')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <?= $form->field($modelPd, 'keyword')->textInput(['class' => 'input primary']) ?>
                    </div>
                </div>
            </div>

            <div class="box">
                <p class="subtitle has-text-weight-bold">Product Attribute</p>
                <?php foreach ($modelPa as $index => $pa): ?>
                    <div class="field">
                        <div class="control">
                            <?= $form->field($pa, "[$index]value")->checkBox([
                            ])->label($pa['myAttribute']['attributeDescription']['name']) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="box">
                <p class="subtitle has-text-weight-bold">Product Sticker</p>
                <?php foreach ($modelPs as $index => $ps): ?>
                    <div class="field">
                        <div class="control">
                            <?= $form->field($ps, "[$index]value")->checkBox([
                            ])->label($ps['sticker']['name']) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="box">
                <p class="subtitle has-text-weight-bold">Product Option</p>
                <?php foreach ($modelPo as $index => $po): ?>
                    <div class="field">
                        <div class="control">
                            <?= $form->field($po, "[$index]value")->textInput([
                                'class' => 'input primary',
                                'template' => '{label}{input}\n{hint}\n{error}', //работает, только стили bulma перекрывают.. оставлю так
                            ])->label($po['attributeDescription']['name']) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>


        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\Yii2AdminAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\SubscribeForm;

Yii2AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="<?= Url::to('@web/images/fav.png') ?>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?= Url::to('@web/images/fav.png') ?>"
          type="image/x-icon"/>
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
    <?php $this->head() ?>
</head>

<body>

<section class="hero">
    <div class="hero-head has-background-white-ter">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand has-background-white">
                    <a class="navbar-item has-text-grey ok-has-brand is-hidden-mobile" href="<?= Url::to(['/']) ?>">
                        <span class="icon has-text-primary is-large"><i class="fas fa-home"></i></span>
                    </a>
                    <a class="navbar-item has-text-grey ok-has-brand is-hidden-tablet" href="<?= Url::to(['/']) ?>"
                       title="Интернет-магазин тонких матрасов на диван, ортопедических матрасов и спальных аксессуаров">
                        <img src="<?= Url::to('@web/images/logo.png') ?>"
                             alt="Интернет-магазин тонких матрасов на диван, ортопедических матрасов и спальных аксессуаров"></a>
                    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div id="navbarMenuHeroA" class="navbar-menu">
                    <div class="navbar-end">
                        <a class="navbar-item has-text-grey" href="<?= Url::to(['site/dostavka-i-oplata']) ?>"
                           title="Доставка и оплата">Доставка и оплата</a>
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <a class="navbar-item has-text-grey" href="<?= Url::to(['site/account']) ?>"
                               title="Личный кабинет, <?= Yii::$app->user->identity->name ?>">Личный
                                кабинет, <?= Yii::$app->user->identity->name ?></a>
                        <?php else: ?>
                            <a class="navbar-item has-text-grey" href="<?= Url::to(['site/account']) ?>"
                               title="Личный кабинет">Личный кабинет</a>
                        <?php endif; ?>
                        <a class="navbar-item has-text-grey" href="<?= Url::to(['cart/index']) ?>"
                           title="Корзина">
                            <div class="ok-main-badge-cart">
                                <?php $cartQty = \app\models\Cart::showCartQty() ?>
                                <?php if ($cartQty): ?>
                                    <span class="badge"
                                          data-badge="<?= $cartQty ?>">Корзина</span>
                                <?php else: ?>
                                    Корзина
                                <?php endif; ?>
                            </div>
                        </a>
                        <a class="navbar-item has-text-grey" href="<?= Url::to(['wishlist/index']) ?>"
                           title="Избранное">
                            <div class="ok-main-badge-wishlist">
                                <?php if (isset($_SESSION['wishlist.qty'])): ?>
                                    <span class="badge"
                                          data-badge="<?= $_SESSION['wishlist.qty'] ?>">Избранное</span>
                                <?php else: ?>Избранное
                                <?php endif; ?>
                            </div>
                        </a>
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <a class="navbar-item has-text-grey" href="<?= Url::to(['site/logout']) ?>"
                               title="Выход">Выход</a>
                        <?php else: ?>
                            <a class="navbar-item has-text-grey" href="<?= Url::to(['site/login']) ?>"
                               title="Войти">Войти</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
    </div>
    </nav>
    </div>

    <!-- Hero content: will be in the middle -->
    <div class="hero-body is-hidden-mobile">
        <div class="container">
            <div class="level">
                <div class="level-item has-text-centered">
                    <div class="is-hidden-mobile">
                        <a href="<?= Url::to(['/']) ?>"
                           title="Интернет-магазин тонких матрасов на диван, ортопедических матрасов и спальных аксессуаров">
                            <img src="<?= Url::to('@web/images/logo.png') ?>" style="height: 3.5em"
                                 alt="Интернет-магазин тонких матрасов на диван, ортопедических матрасов и спальных аксессуаров"></a>
                    </div>
                </div>
                <div class="level-item has-text-centered">
                    <div class="level">
                        <div class="level-item has-text-centered ok-main-benefit">
                            <div>
                                <p class="is-size-5">Непревзойденное качество</p>
                                <p class="is-size-6">Произведены для<br>американского рынка</p>
                            </div>
                        </div>
                        <div class="level-item has-text-centered ok-main-benefit">
                            <div>
                                <p class="is-size-5">Преимущества</p>
                                <p class="is-size-6">Смягчают спальное место,<br>выравнивают поверхность матраса</p>
                            </div>
                        </div>
                        <div class="level-item has-text-centered ok-main-benefit">
                            <div>
                                <p class="is-size-5">Универсальность</p>
                                <p class="is-size-6">На диван, на кровать, на пол,<br>на дачу, в палатку</p>
                            </div>
                        </div>
                        <div class="level-item has-text-centered ok-main-benefit">
                            <div>
                                <p class="is-size-5">Удобство и комфорт</p>
                                <p class="is-size-6">Повышенный уровень комфорта -<br>как будто Вы спите на облаке</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Hero footer: will stick at the bottom -->
    <div class="hero-foot has-background-black-ter">
        <nav class="tabs">
            <div class="container">
                <ul>
                    <li><a href="<?= Url::to(['category/2']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4">Топперы</a>
                    </li>
                    <li><a href="<?= Url::to(['category/3']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4">Футоны</a>
                    </li>
                    <li><a href="<?= Url::to(['category/4']) ?>"
                           class="has-text-white has-background-grey-dark ok-main-btn-category is-size-4">Матрасы</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</section>

<?php $this->beginBody() ?>
<?= $content ?>
<div class="scroll-top">
    <span class="icon has-text-primary"><i class="fas fa-chevron-circle-up"></i></span>
</div>
<section class="section is-clearfix">
    <div class="container">
        <div class="level">
            <div class="level-item is-pulled-left">
                <span class="icon has-text-primary is-large ok-main-contact-icons"><i
                            class="fas fa-envelope"></i></span>
                <?php $form = ActiveForm::begin([
                    'action' => 'site/subscribe',
                    'method' => 'post',
                ]); ?>
                <div class="field has-addons">
                    <div class="control">
                        <?= $form->field(new SubscribeForm(), 'email', [
                            'options' => []])
                            ->textInput()
                            ->input('email', ['placeholder' => 'Ваш e-mail', 'class' => 'input is-shadowless ok-main-input-subscribe'])
                            ->label(false) ?>
                    </div>
                    <div class="control">
                        <?= Html::submitButton('Подписаться', ['class' => 'button is-primary ok-main-btn-subscribe', 'type' => 'submit']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="level-item is-pulled-left">
                <span class="icon has-text-primary is-large ok-main-contact-icons"><i
                            class="fas fa-phone-square"></i></span>
                <div>
                    <a href="tel:098-682-36-17" class="is-size-4"><strong
                                class="has-text-primary">098-682-36-17</strong></a></br>
                </div>
            </div>
            <div class="level-item is-pulled-left">
                <span class="icon has-text-primary is-large ok-main-contact-icons"><i
                            class="fas fa-pen"></i></span>
                <a class="ok-main-connect-icons is-hidden-touch" title="Viber" href="viber://chat?number=380986823617">
                    <span class="icon is-medium" style="color: #59267C"><i
                                class="fab fa-viber fa-2x"></i></span></a>
                <a class="ok-main-connect-icons is-hidden-desktop" title="Viber"
                   href="viber://add?number=380986823617">
                    <span class="icon is-medium" style="color: #59267C"><i
                                class="fab fa-viber fa-2x"></i></span></a>
                <a class="ok-main-connect-icons" title="Telegram" href="tg://resolve?domain=matrasovich"><span
                            class="icon has-text-info is-medium" style="color: #55acee"><i
                                class="fab fa-telegram fa-2x"></i></span></a>
            </div>
        </div>
    </div>
</section>

<section class="hero">
    <div class="hero-head has-background-black-ter">
        <section class="section">
            <div class="container">
                <div class="columns">
                    <div class="column is-one-quarter">
                        <span class="subtitle has-text-grey-lighter">Информация</span>
                        <ul>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['site/dostavka-i-oplata']) ?>">Доставка
                                    и оплата</a></li>
                        </ul>
                        <ul>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['blog/index']) ?>">Блог</a></li>
                        </ul>
                    </div>
                    <div class="column is-one-quarter">
                        <span class="subtitle has-text-grey-lighter">Личные данные</span>
                        <ul>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['site/account']) ?>">Личный
                                    кабинет</a></li>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['cart/index']) ?>">Корзина</a></li>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['wishlist/index']) ?>">Избранное</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column is-one-quarter">
                        <span class="subtitle has-text-grey-lighter">Разное</span>
                        <ul>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['site/about-us']) ?>">О нас</a>
                            </li>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['site/privacy']) ?>">Политика
                                    безопасности</a></li>
                            <li><a class="has-text-grey-light" href="<?= Url::to(['site/terms']) ?>">Условия
                                    соглашения</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Админка';
?>
<div class="section container content">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="buttons">
        <a href="<?= Url::to(['product/index']) ?>" class="button is-medium is-primary" title="Products">Products</a>
        <a href="<?= Url::to(['attribute-group/index']) ?>" class="button is-medium is-primary"
           title="Attribute Groups">Attribute
            Groups</a>
    </div>


</div>

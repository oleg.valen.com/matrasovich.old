<?php

namespace app\modules\yii2admin\models;

use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property int $option
 * @property int $filter
 */
class Category extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'category';
    }

    public function rules()
    {
        return [
            [['category_id', 'name', 'parent_id'], 'required'],
            [['category_id', 'parent_id', 'sort_order', 'status'], 'integer'],
            [['image', 'sort_order', 'status', 'date_added', 'date_modified', 'seo_url'], 'safe'],
        ];
    }

}

<?php

namespace app\modules\yii2admin\models;

use Yii;


class Image extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'image';
    }

    public function rules()
    {
        return [
            [['id', 'filePath'], 'required'],
            [['id', 'itemId', 'isMain'], 'integer'],
            [['itemId', 'isMain', 'modelName', 'urlAlias', 'name'], 'safe'],
        ];
    }

}

<?php

namespace app\modules\yii2admin\models;

use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property int $option
 * @property int $filter
 */
class ProductAttribute extends \yii\db\ActiveRecord
{

    public $value = 0;

    public function getAttributeDescription()
    {
        return $this->hasOne(AttributeDescription::className(), ['attribute_id' => 'attribute_id'])->
        andOnCondition(['language_id' => Yii::$app->language]);
    }

    public function getMyAttribute()
    {
        return $this->hasOne(Attribute::className(), ['attribute_id' => 'attribute_id']);
    }

//    public function getAttributeDescriptions()
//    {
//        return $this->hasMany(AttributeDescription::className(), ['attribute_id' => 'attribute_id'])->
//        andOnCondition(['language_id' => 1]);
//    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute_id'], 'required'],
            [['product_id', 'attribute_id'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'sort_order' => 'Сортировка',
//            'option' => 'Опция',
//            'filter' => 'Фильтр',
//        ];
//    }

//    public static function getAttributeGroups()
//    {
//        return AttributeGroup::find()->
//        joinWith([
//            'attributeGroupDescription' => function ($query) {
//                $query->onCondition(['attribute_group_description.language_id' => 1]);
//            }
//        ])->
//        all();
//    }

}

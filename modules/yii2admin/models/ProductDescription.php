<?php

namespace app\modules\yii2admin\models;

//use app\models\AttributeGroupDescription;
use Yii;

class ProductDescription extends \yii\db\ActiveRecord
{

//    public function getAttributeGroupDescription()
//    {
//        return $this->hasOne(AttributeGroupDescription::className(), ['attribute_group_id' => 'attribute_group_id'])->
//        andOnCondition(['language_id' => 1]);
//    }

    /**
     * {@inheritdoc}
     */
//    public static function tableName()
//    {
//        return 'attribute_group_description';
//    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'language_id', 'name'], 'required'],
            [['product_id'], 'integer'],
            [['text_description', 'title', 'h1', 'description', 'keyword'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'sort_order' => 'Сортировка',
//            'option' => 'Опция',
//            'filter' => 'Фильтр',
//        ];
//    }

//    public static function getAttributeGroups()
//    {
//        return AttributeGroup::find()->
//        joinWith([
//            'attributeGroupDescription' => function ($query) {
//                $query->onCondition(['attribute_group_description.language_id' => 1]);
//            }
//        ])->
//        all();
//    }

}

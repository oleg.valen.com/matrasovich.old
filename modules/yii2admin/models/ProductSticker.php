<?php

namespace app\modules\yii2admin\models;

use Yii;

class ProductSticker extends \yii\db\ActiveRecord
{

    public $value = 0;

    public function getSticker()
    {
        return $this->hasOne(Sticker::className(), ['sticker_id' => 'sticker_id']);
    }

    public static function tableName()
    {
        return 'product_sticker';
    }

    public function rules()
    {
        return [
            [['product_id', 'sticker_id'], 'required'],
            [['product_id', 'sticker_id'], 'integer'],
            [['value'], 'safe'],
        ];
    }

}

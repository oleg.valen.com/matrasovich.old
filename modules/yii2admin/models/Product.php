<?php

namespace app\modules\yii2admin\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property int $option
 * @property int $filter
 */
class Product extends \yii\db\ActiveRecord
{

    public $images;

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public function getProductDescription()
    {
        return $this->hasOne(ProductDescription::className(), ['product_id' => 'product_id'])->
        andOnCondition(['language_id' => Yii::$app->language]);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['category_id', 'sort_order', 'viewed', 'product_delivery_status_id'], 'integer'],
            [['image', 'price', 'sort_order', 'status', 'viewed', 'date_added', 'date_modified', 'seo_url', 'product_delivery_status_id'], 'safe'],
//            [['images'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 20],
            [['images'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'sort_order' => 'Сортировка',
//            'option' => 'Опция',
//            'filter' => 'Фильтр',
//        ];
//    }

    public static function getProducts()
    {
        return Product::find()->
        joinWith('category')->
        orderBy(['product_id' => SORT_DESC])->
        all();
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->images as $key => $file) {
                $path = 'images/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $this->attachImage($path, $key == 0 ? true : false);
//                @unlink($path);
            }
            return true;
        }
        return false;
    }

}

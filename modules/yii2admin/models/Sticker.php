<?php

namespace app\modules\yii2admin\models;

use app\models\AttributeGroupDescription;
use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property int $option
 * @property int $filter
 */
class Sticker extends \yii\db\ActiveRecord
{

//    public function getSticker()
//    {
//        return $this->hasOne(Sticker::className(), ['sticker_id' => 'sticker_id']);
//    }

    public function getProductSticker()
    {
        return $this->hasOne(ProductSticker::className(), ['sticker_id' => 'sticker_id']);
    }

    public static function tableName()
    {
        return 'sticker';
    }

    public function rules()
    {
        return [
            [['sticker_id', 'name'], 'required'],
            [['sticker_id', 'sort_order'], 'integer'],
            [['sort_order', 'class'], 'safe'],
        ];
    }

}

<?php

namespace app\modules\yii2admin\models;

//use app\models\AttributeGroupDescription;
use Yii;

class ProductOption extends \yii\db\ActiveRecord
{

//    public $value = 0;

    public function getAttributeDescription()
    {
        return $this->hasOne(AttributeDescription::className(), ['attribute_id' => 'attribute_id'])->
        andOnCondition(['language_id' => Yii::$app->language]);
    }

    public function getMyAttribute()
    {
        return $this->hasOne(Attribute::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * {@inheritdoc}
     */
//    public static function tableName()
//    {
//        return 'attribute_group_description';
//    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute_id'], 'required'],
            [['product_id', 'attribute_id'], 'integer'],
            [['value'], 'safe'], // 1. update - заполняем с формы; 2. create - заполняем, если <> 0
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'sort_order' => 'Сортировка',
//            'option' => 'Опция',
//            'filter' => 'Фильтр',
//        ];
//    }

//    public static function getAttributeGroups()
//    {
//        return AttributeGroup::find()->
//        joinWith([
//            'attributeGroupDescription' => function ($query) {
//                $query->onCondition(['attribute_group_description.language_id' => 1]);
//            }
//        ])->
//        all();
//    }

}

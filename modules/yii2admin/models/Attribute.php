<?php

namespace app\modules\yii2admin\models;

use app\models\AttributeGroupDescription;
use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property int $option
 * @property int $filter
 */
class Attribute extends \yii\db\ActiveRecord
{

//    public function getAttributeGroupDescription()
//    {
//        return $this->hasOne(AttributeGroupDescription::className(), ['attribute_group_id' => 'attribute_group_id'])->
//        andOnCondition(['language_id' => 1]);
//    }

    public function getMyAttribute()
    {
        return $this->hasOne(Attribute::className(), ['attribute_id' => 'attribute_id']);
    }

    public function getAttributeDescription()
    {
        return $this->hasOne(AttributeDescription::className(), ['attribute_id' => 'attribute_id'])->
        andOnCondition(['language_id' => Yii::$app->language]);
    }

    public function getAttributeDescriptions()
    {
        return $this->hasMany(AttributeDescription::className(), ['attribute_id' => 'attribute_id'])->
        andOnCondition(['language_id' => Yii::$app->language]);
    }

    public function getProductAttribute()
    {
        return $this->hasOne(ProductAttribute::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_group_id', 'attribute_id'], 'required'],
            [['attribute_group_id', 'attribute_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'sort_order' => 'Сортировка',
//            'option' => 'Опция',
//            'filter' => 'Фильтр',
//        ];
//    }

//    public static function getAttributeGroups()
//    {
//        return AttributeGroup::find()->
//        joinWith([
//            'attributeGroupDescription' => function ($query) {
//                $query->onCondition(['attribute_group_description.language_id' => 1]);
//            }
//        ])->
//        all();
//    }

}

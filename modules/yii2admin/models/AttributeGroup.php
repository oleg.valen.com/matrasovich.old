<?php

namespace app\modules\yii2admin\models;

use app\models\AttributeGroupDescription;
use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property int $option
 * @property int $filter
 */
class AttributeGroup extends \yii\db\ActiveRecord
{

    const OPTION_ID = 2;

    public function getAttributeGroupDescription()
    {
        return $this->hasOne(AttributeGroupDescription::className(), ['attribute_group_id' => 'attribute_group_id'])->
        andOnCondition(['language_id' => Yii::$app->language]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort_order', 'option', 'filter'], 'required'],
            [['sort_order', 'option', 'filter'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'sort_order' => 'Сортировка',
//            'option' => 'Опция',
//            'filter' => 'Фильтр',
//        ];
//    }

    public static function getAttributeGroups()
    {
        return AttributeGroup::find()->
        joinWith([
            'attributeGroupDescription' => function ($query) {
                $query->onCondition(['attribute_group_description.language_id' => Yii::$app->language]);
            }
        ])->
        all();
    }

}

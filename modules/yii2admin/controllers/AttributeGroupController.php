<?php

namespace app\modules\yii2admin\controllers;

use app\modules\yii2admin\models\AttributeGroupDescription;
use Yii;
use app\modules\yii2admin\models\AttributeGroup;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

/**
 * AttributeGroupController implements the CRUD actions for AttributeGroup model.
 */
class AttributeGroupController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AttributeGroup models.
     * @return mixed
     */
    public function actionIndex()
    {

        $data['ag'] = AttributeGroup::getAttributeGroups();

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    /**
     * Displays a single AttributeGroup model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $links = [];
        $links[] = ['label' => 'Attribute Group', 'url' => Url::to(['index'])];
        $links[] = 'View';

        $breadcrumbs = Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Url::to(['default/index']),
            ],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelAgd' => $this->findModelAgd($id),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Creates a new AttributeGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttributeGroup();
        $modelAgd = new AttributeGroupDescription();

        $links = [];
        $links[] = ['label' => 'Attribute Group', 'url' => Url::to(['index'])];
        $links[] = 'Create';

        $breadcrumbs = Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Url::to(['default/index']),
            ],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelAgd->attribute_group_id = Yii::$app->db->getLastInsertID();
            if ($modelAgd->load(Yii::$app->request->post()) && $modelAgd->save()) {
                return $this->redirect(['view', 'id' => $model->attribute_group_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelAgd' => $modelAgd,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Updates an existing AttributeGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAgd = $this->findModelAgd($id);

        $links = [];
        $links[] = ['label' => 'Attribute Group', 'url' => Url::to(['index'])];
        $links[] = 'Update';

        $breadcrumbs = Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Url::to(['default/index']),
            ],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save() && $modelAgd->load(Yii::$app->request->post()) && $modelAgd->save()) {
            return $this->redirect(['view', 'id' => $model->attribute_group_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelAgd' => $modelAgd,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Deletes an existing AttributeGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->findModelAgd($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AttributeGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttributeGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttributeGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelAgd($id)
    {
        if (($model = AttributeGroupDescription::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

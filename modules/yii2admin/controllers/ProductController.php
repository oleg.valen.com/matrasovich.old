<?php

namespace app\modules\yii2admin\controllers;

use app\modules\yii2admin\models\Attribute;
use app\modules\yii2admin\models\AttributeGroupDescription;
use app\modules\yii2admin\models\Category;
use app\modules\yii2admin\models\Image;
use app\modules\yii2admin\models\Product;
use app\modules\yii2admin\models\ProductAttribute;
use app\modules\yii2admin\models\ProductDescription;
use app\modules\yii2admin\models\ProductOption;
use app\modules\yii2admin\models\ProductSticker;
use app\modules\yii2admin\models\Sticker;
use Yii;
use app\modules\yii2admin\models\AttributeGroup;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\base\Model;
use app\modules\yii2admin\models\UploadForm;
use yii\web\UploadedFile;

/**
 * AttributeGroupController implements the CRUD actions for AttributeGroup model.
 */
class ProductController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AttributeGroup models.
     * @return mixed
     */
    public function actionIndex()
    {

        $data['p'] = Product::getProducts();

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    /**
     * Displays a single AttributeGroup model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $links = [];
        $links[] = ['label' => 'Product', 'url' => Url::to(['index'])];
        $links[] = 'View';

        $breadcrumbs = Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Url::to(['default/index']),
            ],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelPd' => $this->findModelPd($id),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Creates a new AttributeGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = 0)
    {
        $model = new Product();
        $model->status = 1;
        $model->viewed = 0;
        $model->date_added = date('Y-m-d H:i:s');
        $model->date_modified = date('Y-m-d H:i:s');

        $modelPd = new ProductDescription();
        $modelPa = [];
        $modelPo = [];
        $modelPs = [];
        $links = [];
        $post = Yii::$app->request->post();

        $copy = $id != 0;

        if ($copy) {
            $modelCopy = $this->findModel($id);
            $model->category_id = $modelCopy->category_id;
            $model->name = $modelCopy->name;
            $model->seo_url = $modelCopy->seo_url;

            $modelPdCopy = $this->findModelPd($id);
            $modelPd->language_id = $modelPdCopy->language_id;
            $modelPd->name = $modelPdCopy->name;
            $modelPd->text_description = $modelPdCopy->text_description;
            $modelPd->title = $modelPdCopy->title;
            $modelPd->h1 = $modelPdCopy->h1;
            $modelPd->description = $modelPdCopy->description;
            $modelPd->keyword = $modelPdCopy->keyword;
        }

        $arrayPo = $this->createModelPo();
        foreach ($arrayPo as $key => $po) {
            $modelPo[$key] = new ProductOption();
            $modelPo[$key]->attribute_id = $key;
        }

        $arrayPa = $this->createModelPa();
        foreach ($arrayPa as $key => $pa) {
            $modelPa[$key] = new ProductAttribute();
            $modelPa[$key]->attribute_id = $key;
        }

        $arrayPs = $this->createModelPs();
        foreach ($arrayPs as $key => $s) {
            $modelPs[$key] = new ProductSticker();
            $modelPs[$key]->sticker_id = $key;
        }

        $categories = $this->findCategories();

        $links[] = ['label' => 'Product', 'url' => Url::to(['index']), 'id' => '4'];
        $links[] = 'Create';

        $breadcrumbs = Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Url::to(['default/index']),
            ],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

        $ok = $model->load($post) && $model->save();
        if ($ok) {
            $lastInsertID = Yii::$app->db->getLastInsertID();

            $modelPd->product_id = $lastInsertID;
            $ok = $modelPd->load($post) && $modelPd->save();

            if ($ok) {
                foreach ($modelPa as $pa) {
                    $pa->product_id = $lastInsertID;
                }
                $ok = Model::loadMultiple($modelPa, $post) && Model::validateMultiple($modelPa);
                if ($ok) {
                    foreach ($modelPa as $pa) {
                        if (empty($pa->value)) continue;
                        $pa->save(false);
                    }
                }
            }

            if ($ok) {
                foreach ($modelPo as $po) {
                    $po->product_id = $lastInsertID;

                }
                $ok = Model::loadMultiple($modelPo, $post) && Model::validateMultiple($modelPo);
                if ($ok) {
                    foreach ($modelPo as $po) {
                        if (empty($po->value)) continue;
                        $po->save(false);
                    }
                }
            }

            if ($ok) {
                foreach ($modelPs as $ps) {
                    $ps->product_id = $lastInsertID;
                }
                $ok = Model::loadMultiple($modelPs, $post) && Model::validateMultiple($modelPs);
                if ($ok) {
                    foreach ($modelPs as $ps) {
                        if (empty($ps->value)) continue;
                        $ps->save(false);
                    }
                }
            }

            if ($ok) {
                if (Yii::$app->request->isPost) {
                    $model->images = UploadedFile::getInstances($model, 'images');
                    if ($model->upload()) {
                        // file is uploaded successfully

                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelPa' => $modelPa,
            'modelPo' => $modelPo,
            'modelPd' => $modelPd,
            'modelPs' => $modelPs,
            'categories' => $categories,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Updates an existing AttributeGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public
    function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->date_modified = date('Y-m-d H:i:s');
        $modelPd = $this->findModelPd($id);
        $modelPa = [];
        $links = [];
        $modelPo = $this->findModelPo($id);
        $modelPs = [];
        $modelImages = new Image();
        $categories = $this->findCategories();
        $post = Yii::$app->request->post();

        $arrayPa = $this->findModelPa($id);
        foreach ($arrayPa as $key => $pa) {
            if (is_null($pa->productAttribute)) {
                $modelPa[$key] = new ProductAttribute();
                $modelPa[$key]->product_id = $id;
                $modelPa[$key]->attribute_id = $pa->attribute_id;
            } else {
                $modelOnePa = $this->findOneModelPa($id, $pa->attribute_id);
                $modelOnePa->value = 1;
                $modelPa[$key] = $modelOnePa;
            }
        }

        $arrayPs = $this->findModelPs($id);
        foreach ($arrayPs as $key => $ps) {
            if (is_null($ps->productSticker)) {
                $modelPs[$key] = new ProductSticker();
                $modelPs[$key]->product_id = $id;
                $modelPs[$key]->sticker_id = $ps->sticker_id;
            } else {
                $modelOnePs = $this->findOneModelPs($id, $ps->sticker_id);
                $modelOnePs->value = 1;
                $modelPs[$key] = $modelOnePs;
            }
        }

        $links[] = ['label' => 'Product', 'url' => Url::to(['index'])];
        $links[] = 'Update';

        $breadcrumbs = Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Url::to(['default/index']),
            ],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

//        $ok = $model->load($post) && $model->save() && $modelPd->load($post) && $modelPd->save();
        $ok = $model->load($post) && $model->save();
        if ($ok) {

            if ($ok) {
                $ok = Model::loadMultiple($modelPa, $post) && Model::validateMultiple($modelPa);
                if ($ok) {
                    foreach ($modelPa as $pa) {
                        if ($pa->value) {
                            $pa->save(false);
                        } else {
                            $modelOnePa = $this->findOneModelPa($pa->product_id, $pa->attribute_id);
                            if ($modelOnePa) {
                                $modelOnePa->delete();
                            }
                        }
                    }
                }
            }

            if ($ok) {
                $ok = Model::loadMultiple($modelPs, $post) && Model::validateMultiple($modelPs);
                if ($ok) {
                    foreach ($modelPs as $ps) {
                        if ($ps->value) {
                            $ps->save(false);
                        } else {
                            $modelOnePs = $this->findOneModelPs($ps->product_id, $ps->sticker_id);
                            if ($modelOnePs) {
                                $modelOnePs->delete();
                            }
                        }
                    }
                }
            }

            if ($ok) {
                $ok = Model::loadMultiple($modelPo, $post) && Model::validateMultiple($modelPo);
                if ($ok) {
                    foreach ($modelPo as $po) {
                        $po->save(false);
                    }
                }
            }

            if (Yii::$app->request->isPost) {
                $model->images = UploadedFile::getInstances($model, 'images');
                if ($model->upload()) {
                    // file is uploaded successfully

                }
            }

            return $this->redirect(['view', 'id' => $model->product_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelPo' => $modelPo,
            'modelPa' => $modelPa,
            'modelPd' => $modelPd,
            'modelPs' => $modelPs,
            'modelImages' => $modelImages,
            'categories' => $categories,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Deletes an existing AttributeGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->removeImages();
        $model->delete();
        $this->findModelPd($id)->delete();
        foreach ($this->findAllModelPa($id) as $pa) {
            $pa->delete();
        }
        foreach ($this->findAllModelPs($id) as $ps) {
            $ps->delete();
        }
        foreach ($this->findModelPo($id) as $po) {
            $po->delete();
        }


        return $this->redirect(['index']);
    }

    /**
     * Finds the AttributeGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttributeGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelPd($id)
    {
        if (($model = ProductDescription::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelPo($id)
    {

        $model = ProductOption::find()->
        indexBy('attribute_id')->
        joinWith('attributeDescription')->
        joinWith('myAttribute')->
        where(['product_id' => $id])->
        orderBy('sort_order')->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelPa($id)
    {

        $model = Attribute::find()->
        indexBy('attribute_id')->
        joinWith('attributeDescription')->
        joinWith([
            'productAttribute' => function ($query) use ($id) {
                $query->onCondition(['product_id' => $id]);
            }
        ])->
        orderBy(['attribute_group_id' => SORT_ASC, 'sort_order' => SORT_ASC])->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelPs($id)
    {

        $model = Sticker::find()->
        indexBy('sticker_id')->
        joinWith([
            'productSticker' => function ($query) use ($id) {
                $query->onCondition(['product_id' => $id]);
            }
        ])->
        orderBy(['sort_order' => SORT_ASC])->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findAllModelPa($id)
    {

        $model = ProductAttribute::find()->
        where(['product_id' => $id])->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findAllModelPs($id)
    {

        $model = ProductSticker::find()->
        where(['product_id' => $id])->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findOneModelPa($product_id, $attribute_id)
    {

        $model = ProductAttribute::findOne(['product_id' => $product_id, 'attribute_id' => $attribute_id]);

        if ($model !== null) {
            return $model;
        }

        return null;
    }

    protected function findOneModelPs($product_id, $sticker_id)
    {

        $model = ProductSticker::findOne(['product_id' => $product_id, 'sticker_id' => $sticker_id]);

        if ($model !== null) {
            return $model;
        }

        return null;
    }

    protected function createModelPo()
    {

        $model = Attribute::find()->
        indexBy('attribute_id')->
        joinWith('attributeDescription')->
        where(['attribute_group_id' => AttributeGroup::OPTION_ID])->
        orderBy('sort_order')->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function copyModelPo($id)
    {

        $model = ProductOption::find()->
        indexBy('attribute_id')->
        joinWith('myAttribute')->
        joinWith('attributeDescription')->
        where(['product_id' => $id])->
        orderBy('sort_order')->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function createModelPa()
    {

        $model = Attribute::find()->
        indexBy('attribute_id')->
        joinWith('attributeDescriptions')->
        orderBy('attribute_group_id, sort_order')->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function createModelPs()
    {

        $model = Sticker::find()->
        indexBy('sticker_id')->
        orderBy('sort_order')->
        all();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findCategories()
    {

        $newArray = [];
        $array = Category::find()->indexBy('category_id')->orderBy('sort_order')->where(['!=', 'category_id', 1])->asArray()->all();
        foreach ($array as $key => $item) {
            $newArray[$key] = $item['name'];
        }
        return $newArray;
    }

}

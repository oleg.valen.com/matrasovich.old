<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class Stock extends ActiveRecord
{
    public function getProductOption()
    {
        return $this->hasOne(ProductOption::class, ['product_id' => 'product_id', 'attribute_id' => 'attr_id']);
    }

}
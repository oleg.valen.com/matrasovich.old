<?php

namespace app\models;

use yii\db\ActiveRecord;

class ProductSticker extends ActiveRecord
{
    public function getSticker()
    {
        return $this->hasOne(Sticker::class, ['sticker_id' => 'sticker_id']);
    }

}
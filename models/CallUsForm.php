<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CallUsForm extends Model
{
    public $writeUs;
    public $telephone;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['telephone', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'writeUs' => 'Обратный звонок',
            'telephone' => 'Телефон',
        ];
    }
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class NostandartForm extends Model
{
    public $price;
    public $width;
    public $length;
    public $total;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['price,width,length,total', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price' => 'Цена за 1 кв.м.',
            'width' => 'Ширина (см)',
            'length' => 'Длина (см)',
            'total' => 'Цена',
        ];
    }
}
<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use Yii;

class Serie extends ActiveRecord
{

    public function getSerieDescriptions()
    {
        return $this->hasOne(SerieDescription::class, ['serie_id' => 'serie_id']);
    }

    public static function getSerie($serie_id)
    {
        return Serie::find()
            ->joinWith([
                'serieDescriptions as sd' => function ($query) {
                    $query->onCondition(['sd.language_id' => Yii::$app->language]);
                }])
            ->where('serie.serie_id=:serie_id', [':serie_id' => $serie_id])->
            limit(1)->
            one();
    }

    public static function getProductIds($detail_id)
    {
        $data = [];
        $pda = ProductDetailAttribute::find()->
        select('product_id')->
        where(['detail_id' => $detail_id])->
        asArray()->
        all();
        foreach ($pda as $k => $v) {
            $data[] = $v['product_id'];
        }
        return $data;
    }

    public static function getSerieHref($detail_id)
    {
        $serie = Serie::find()->
        where(['detail_id' => $detail_id])->
        limit(1)->
        one();
//        return '/' . $serie['seo_url'] . '/s' . $serie['serie_id'];
        return Url::to(['serie/index', 'serie_id' => $serie->serie_id, 's1' => $serie->seo_url, 's2' => 's']);
    }

}
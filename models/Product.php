<?php

namespace app\models;

use simialbi\yii2\schemaorg\models\MonetaryAmount;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\Url;

class Product extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttribute::class, ['product_id' => 'product_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::class, ['category_id' => 'category_id']);
    }

    public function getProductDescription()
    {
        return $this->hasOne(ProductDescription::class, ['product_id' => 'product_id'])
            ->onCondition(['language_id' => Yii::$app->language]);
    }

    public function getProductDescriptionLangCookies()
    {
        return $this->hasOne(ProductDescription::class, ['product_id' => 'product_id'])
            ->onCondition(['language_id' => Yii::$app->request->cookies->getValue('_language', 'ru')]);
    }

    public function getProductStickers()
    {
        return $this->hasMany(ProductSticker::class, ['product_id' => 'product_id'])->
        joinWith('sticker s')->
        orderBy('s.sort_order');
    }

    public function getProductOption()
    {
        return $this->hasOne(ProductOption::class, ['product_id' => 'product_id']);
    }

    public static function getProductsByIds($ids)
    {
//        return Product::find()->where(['product_id' => $ids])->indexBy('product_id')->all();
        return Product::find()->where(['product_id' => $ids])->indexBy('product_id')->all();
    }

    public static function getProductsQueryByCategory($categoryId, $productAttributeIds, $isSized, $attributeId)
    {

        $k = 0;

        if (!$isSized) {
            $productQuery = Product::find()
                ->where(['category_id' => $categoryId, 'status' => 1])
                ->joinWith('productDescription')
                ->with('productDescription');
            foreach ($productAttributeIds as $ids) {
                if ($k++ == 0) {
                    $productQuery->innerJoinWith("productAttributes pa{$k}")->where(["product.category_id" => $categoryId, "pa{$k}.attribute_id" => $ids, 'product.status' => 1]);
                } else {
                    $productQuery->innerJoinWith("productAttributes pa{$k}")->andWhere(["product.category_id" => $categoryId, "pa{$k}.attribute_id" => $ids, 'product.status' => 1]);
                }
            }
        } else {
            $productQuery = Product::find()->where(['category_id' => $categoryId, 'status' => 1]);
            foreach ($productAttributeIds as $ids) {
                if ($k++ == 0) {
                    $productQuery->innerJoinWith("productAttributes pa{$k}")->where(["product.category_id" => $categoryId, "pa{$k}.attribute_id" => $ids, 'product.status' => 1]);
//                    ->joinWith([
//                        'productOption po' => function ($query) use ($attributeId) {
//                            $query->onCondition(['po.attribute_id' => $attributeId]);
////                            $query->onCondition('po.attribute_id=:attribute_id', [':attribute_id' => $attributeId]);
//                        }]);
                } else {
                    $productQuery->innerJoinWith("productAttributes pa{$k}")->andWhere(["product.category_id" => $categoryId, "pa{$k}.attribute_id" => $ids, 'product.status' => 1]);
//                    ->joinWith([
//                        'productOption po' => function ($query) use ($attributeId) {
//                            $query->onCondition(['po.attribute_id' => $attributeId]);
//                        }]);
                }
            }
            $productQuery->
            joinWith([
                'productOption po' => function ($query) use ($attributeId) {
//                        $query->onCondition(['po.attribute_id' => $attributeId]);
                    $query->onCondition('po.attribute_id=:attribute_id', [':attribute_id' => $attributeId]);
                }]);
        }
        return $productQuery->distinct();
    }

    public static function getProductQueryById($product_id)
    {
        return Product::find()
            ->joinWith('category c')
            ->joinWith([
                'productDescription pd' => function ($query) {
                    $query->onCondition(['pd.language_id' => Yii::$app->language]);
                }])
//            ->where(['product.product_id' => $product_id])->limit(1);
            ->where('product.product_id=:product_id', [':product_id' => $product_id])
            ->limit(1);
    }

    public static function getProductOptions($product_id)
    {
        return ProductOption::find()
            ->joinWith('myAttribute a')
            ->joinWith(
                ['attributeDescription ad' => function ($query) {
                    $query->onCondition(['ad.language_id' => Yii::$app->language]);
                }])
            ->where(['product_option.product_id' => $product_id])
            ->orderBy('a.sort_order')
            ->all();
    }

    public static function getProductDetailAttributes($product_id)
    {
        $pdas = ProductDetailAttribute::find()
            ->joinWith([
                'detailAttributeDescriptions dad' => function ($query) {
                    $query->onCondition(['dad.language_id' => Yii::$app->language])
                        ->joinWith('detailAttribute da');
                }
            ])
            ->where('product_detail_attribute.product_id=:product_id', [':product_id' => $product_id])
            ->orderBy('da.sort_order')
            ->asArray()
            ->all();
        foreach ($pdas as $key => &$pda) {
            if ($pda['detailAttributeDescriptions'][0]['text'] !== '' || $pda['detailAttributeDescriptions'][0]['detailAttribute']['image'] !== '') {
                $pda['hasDetailed'] = true;
            }
            if ($pda['detailAttributeDescriptions'][0]['key'] == 'Серия' || $pda['detailAttributeDescriptions'][0]['key'] == 'Серія') {
                $pda['href'] = Serie::getSerieHref($pda['detailAttributeDescriptions'][0]['detail_id']);
            }
        }
        unset($pda);
        return $pdas;
    }

    public static function getProductDetailDescription($product_id, $detail_id)
    {
        return ProductDetailAttribute::find()
            ->joinWith([
                'detailAttributeDescriptions dad' => function ($query) {
                    $query->onCondition(['dad.language_id' => Yii::$app->language])
                        ->joinWith('detailAttribute da');
                }
            ])
            ->where('product_detail_attribute.product_id=:product_id', [':product_id' => $product_id])
            ->andWhere('product_detail_attribute.detail_id=:detail_id', ['detail_id' => $detail_id])
            ->orderBy('da.sort_order')
            ->asArray()
            ->all();
    }

    public static function getProductsQueryBySearchQuery($q)
    {

        $query = Product::find()
            ->where('product_id=:product_id', [':product_id' => $q])
            ->andWhere(['status' => 1]);
        if (!$query->count())
            return Product::find()
                ->where(['like', 'name', $q])
                ->andWhere(['status' => 1]);
        return $query;
    }

    public static function getProductSimilar($get, $product_id, $product_ids = [])
    {

        $array = $detail = $products = [];

        $isSimilar = empty($product_ids);

        if ($isSimilar) {
            //product similar
            $product_ids = self::getProductSimilarIds($product_id);
            array_unshift($product_ids, $product_id);
        } else {
            //compare
            $product_ids = explode(',', $product_ids);
        }
        if (empty($product_ids)) return [];

        $pdas = ProductDetailAttribute::find()
            ->joinWith([
                'detailAttributeDescriptions dad' => function ($query) {
                    $query->onCondition(['dad.language_id' => Yii::$app->request->cookies->getValue('_language', 'ru')])
                        ->joinWith('detailAttribute da');
                }
            ])
            ->where(['in', 'product_detail_attribute.product_id', $product_ids])
            ->orderBy('da.sort_order')
            ->asArray()
            ->all();
        foreach ($pdas as $pda) {
            $array[$pda['detailAttributeDescriptions'][0]['key']][$pda['product_id']][] = $pda['detailAttributeDescriptions'][0]['value'];
        }

        foreach ($array as $key => $ars) {
            foreach ($product_ids as $id) {
                if (array_key_exists($id, $ars)) {
                    $detail[$key][$id] = $ars[$id];
                } else {
                    $detail[$key][$id][] = '';
                }
            }
        }

        $array2 = Product::find()->
        where(['in', 'product.product_id', $product_ids])->
        indexBy('product_id')->
        with('productDescriptionLangCookies')->
        all();
        foreach ($product_ids as $id) {
            if (array_key_exists($id, $array2)) {
                $products[$id] = $array2[$id];
            }
        }

        foreach ($products as $product) {
            if (isset($get['s'])) {
                $price = ProductOption::getPrice($product->product_id, $get['s']);
                $detail['Цена'][$product->product_id] = [$price ? $price . ' грн.' : ''];
            } else {
                $detail['Цена'][$product->product_id] = [$product->price . ' грн.'];
            }
        }

        return ['details' => $detail, 'products' => $products];
    }

    public static function getProductSimilarIds($product_id)
    {
        return ProductSimilar::find()->select('product_similar_id')->where(['product_id' => $product_id])->asArray()->column();
    }

    public static function getProductsByOrderLastSeen($product_ids = [])
    {

        $newProducts = [];
        if (empty($product_ids)) {
            $post = Yii::$app->request->post();
            $product_ids = explode(',', $post['lastSeenProducts']);
        }
        $products = Product::find()->where(['in', 'product.product_id', $product_ids])->indexBy('product_id')->all();
        foreach ($product_ids as $value) {
            $newProducts[$value] = $products[$value];
        }
        return $newProducts;
    }

    public static function getProductsOptions($cart)
    {
        $data = [];
        $data['sum'] = 0;
        $data['amount'] = 0;
        $productIds = [];
        $cart = json_decode($cart, true);
        foreach ($cart as $key => $value) {
            $productIds[] = $key;
        }

        $products = Product::find()->where(['in', 'product.product_id', $productIds])
            ->with('productDescriptionLangCookies')
            ->joinWith([
                'productDescription pd' => function ($q) {
                    $q->onCondition(['pd.language_id' => Yii::$app->request->cookies->getValue('_language', 'ru')]);
                }
            ])
            ->joinWith(['productAttributes pa' => function ($q) {
                $q->indexBy('attribute_id')
                    ->joinWith(['attributeDescription ad' => function ($q) {
                        $q->onCondition(['ad.language_id' => Yii::$app->request->cookies->getValue('_language', 'ru')]);
                    }])
                    ->joinWith('productOption');
            }])
            ->indexBy('product_id')
            ->all();
        foreach ($cart as $productId => $pad) {
            if (array_key_exists($productId, $products)) {
                foreach ($pad as $paId => $amount) {
                    if (array_key_exists($paId, $products[$productId]->productAttributes)) {
                        $data['products'][$productId][$paId] = [
                            'product' => $products[$productId],
                            'pdName' => $products[$productId]->productDescription->name,
                            'paName' => $products[$productId]->productAttributes[$paId]->attributeDescription->name,
                            'amount' => $amount,
                            'price' => intval($products[$productId]->productAttributes[$paId]->productOption->value),
                            'sum' => $amount * intval($products[$productId]->productAttributes[$paId]->productOption->value),
                        ];
                        $data['amount'] += $amount;
                        $data['sum'] += $amount * intval($products[$productId]->productAttributes[$paId]->productOption->value);
                    }
                }
            }
        }

        return $data;
    }

    public static function getDelivery($category_id, $attr, $price)
    {
        $sizes = explode('x', $attr);
        $width = $sizes[0];
        if ($price >= 6000) {
//            return ['warehouse' => '<span class="has-text-danger has-text-weight-bold">0</span> грн', 'doors' => '<span class="has-text-danger has-text-weight-bold">0</span> грн'];
            return ['warehouse' => 0, 'doors' => 0];
        }
        if (in_array($category_id, [2])) { //топперы
            return ['warehouse' => 0, 'doors' => 100];
        }
        if (in_array($category_id, [5, 6, 7])) { //наматрасники, подушки, одеяла
            return ['warehouse' => 50, 'doors' => 70];
        }
        switch ($width) {
            case '70':
                return ['warehouse' => 200, 'doors' => 250];
            case '80':
                return ['warehouse' => 200, 'doors' => 250];
            case '90':
                return ['warehouse' => 250, 'doors' => 300];
            case '120':
                return ['warehouse' => 350, 'doors' => 450];
            case '140':
                return ['warehouse' => 400, 'doors' => 500];
            case '150':
                return ['warehouse' => 400, 'doors' => 500];
            case '160':
                return ['warehouse' => 400, 'doors' => 500];
            case '180':
                return ['warehouse' => 500, 'doors' => 600];
            default:
                return ['warehouse' => 'уточняйте у менеджера', 'doors' => 'уточняйте у менеджера'];
        }
    }

    public static function getMonetaryAmount($categoryId)
    {
        $monetaryAmount = new MonetaryAmount();
        $monetaryAmount->currency = 'UAH';
        if (in_array($categoryId, [2])) { //топперы
            $monetaryAmount->value = 0;
        } elseif (in_array($categoryId, [5, 6, 7])) {
            $monetaryAmount->value = 'по тарифам перевозчика';
        } else {
            $monetaryAmount->minValue = 200;
            $monetaryAmount->maxValue = 600;
        }
        $monetaryAmount->name = 'Доставка НовойПочтой';
        $monetaryAmount->url = Url::to(['site/dostavka-i-oplata']);
        return $monetaryAmount;
    }

}
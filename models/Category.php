<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class Category extends ActiveRecord
{

    public function getAttributeGroups()
    {
        return $this->hasMany(AttributeGroup::class, ['attribute_group_id' => 'attribute_group_id'])
            ->viaTable('category_attribute_group', ['category_id' => 'category_id']);

    }

    public function getCategoryDescriptions()
    {
        return $this->hasOne(CategoryDescription::class, ['category_id' => 'category_id']);
    }

    public static function getCategoryQueryById($category_id)
    {
        return Category::find()
            ->joinWith([
                'categoryDescriptions as cd' => function ($query) {
                    $query->onCondition(['cd.language_id' => Yii::$app->language]);
                }])
            ->where('category.category_id=:category_id', [':category_id' => $category_id])->limit(1);
    }

    public static function getFilter($categoryAQ, $get)
    {

        $filter = $attributeIds = $productIds = $adIds = $seoUlrs = [];
        $k = 0;

        $category = $categoryAQ
            ->joinWith(
                [
                    'attributeGroups ag' => function ($query) {
                        $query->onCondition(['ag.filter' => AttributeGroup::FILTER_ON])->orderBy('ag.sort_order')
                            ->joinWith(
                                ['attributeGroupDescriptions agd' => function ($query) {
                                    $query->onCondition(['agd.language_id' => Yii::$app->language]);
                                }]);
                    }])
            ->one();

        foreach ($get as $key => $value) {
            if (substr_compare($key, 'Filter', -6) === 0) {
                $tmp = explode(',', $value);
                foreach ($tmp as $item) {
                    $seoUlrs[substr($key, 0, strlen($key) - 6)][] = $item;
                }
            }
        }

        $agds = AttributeGroupDescription::find()->where(['attribute_group_description.language_id' => Yii::$app->language])->all();
        foreach ($agds as $agd) {
            if (key_exists($agd->seo_url, $seoUlrs)) {
                $as = Attribute::find()
                    ->joinWith('attributeDescriptions')
                    ->where(['attribute.attribute_group_id' => $agd->attribute_group_id])
                    ->all();
                foreach ($as as $a) {
                    if (in_array($a->attributeDescriptions->seo_url, $seoUlrs[$agd->seo_url])) {
                        $adIds[] = $a->attribute_id;
                    }
                }
            }
        }

        foreach ($category->attributeGroups as $ag) {
            $attributes = $ag->getMyAttributes()
                ->joinWith([
                    'attributeDescriptions ad' => function ($query) {
                        $query->onCondition(['ad.language_id' => Yii::$app->language]);
                    }])
                ->orderBy('sort_order')
                ->all();

            $attrs = [];
            foreach ($attributes as $a) {
                $adIds2 = $adIds;
                $adIds2[] = $a->attribute_id;
                $pa = ProductAttribute::find();
                foreach ($adIds2 as $id) {
                    $k++;
                    $pa->innerJoinWith([
                        "productAttributes pa{$k}" => function ($query) use ($k, $id) {
                            $query->onCondition(["pa{$k}.attribute_id" => $id]);
                        }
                    ]);
                }
                $pa = $pa
                    ->joinWith('products')
                    ->andWhere(['product.category_id' => $category->category_id])
                    ->limit(1)
                    ->one();
                if ($pa == null) {
                    continue;
                }
                $attrs[] = [
                    'attribute_id' => $a->attributeDescriptions->attribute_id,
                    'name' => $a->attributeDescriptions->name,
                    'seo_url' => $a->attributeDescriptions->seo_url,
                    'title' => $a->attributeDescriptions->title,
                    'description' => $a->attributeDescriptions->description,
                    'keyword' => $a->attributeDescriptions->keyword,
                    'h1' => $a->attributeDescriptions->h1,
                    'text_description' => $a->attributeDescriptions->text_description,
                ];
            }
            if (!empty($attrs)) {
                $filter[] = [
                    'attribute_group_id' => $ag->attributeGroupDescriptions->attribute_group_id,
                    'name' => $ag->attributeGroupDescriptions->name,
                    'seo_url' => $ag->attributeGroupDescriptions->seo_url,
                    'title' => $ag->attributeGroupDescriptions->title,
                    'description' => $ag->attributeGroupDescriptions->description,
                    'keyword' => $ag->attributeGroupDescriptions->keyword,
                    'h1' => $ag->attributeGroupDescriptions->h1,
                    'nofollow' => $ag->attributeGroupDescriptions->nofollow,
                    'noindex' => $ag->attributeGroupDescriptions->noindex,
                    'attrs' => $attrs,
                ];
            }
        }
        return $filter;
    }

    public
    static function getModelsAd($category, $modelsAd, $modelsAdContent)
    {

        $attribute_ids = [];
        $category = $category
            ->joinWith(
                [
                    'attributeGroups ag' => function ($query) {
                        $query->onCondition(['ag.filter' => AttributeGroup::FILTER_ON])->orderBy('ag.sort_order')
                            ->joinWith(
                                ['attributeGroupDescriptions agd' => function ($query) {
                                    $query->onCondition(['agd.language_id' => Yii::$app->language]);
                                }]);
                    }])
            ->one();

        foreach ($category->attributeGroups as $ag) {
            if (!(mb_stripos('Производитель,Скидка', $ag->attributeGroupDescriptions->name) === false))
                continue;
            $array = ProductAttribute::find()->
            select(['product_attribute.attribute_id'])->
            joinWith([
                'products' => function ($query) {
                    $query->joinWith('category');
                }
            ])->
            joinWith('myAttributes')->
            where(['category.category_id' => $category->category_id, 'attribute.attribute_group_id' => $ag->attribute_group_id])->
            distinct()->
            indexBy('attribute_id')->
            orderBy('attribute_id')->
            all();
            foreach ($array as $value) {
                $attribute_ids[] = $value->attribute_id;
            }
            $attributes = $ag->getMyAttributes()
                ->joinWith([
                    'attributeDescriptions ad' => function ($query) {
                        $query->onCondition(['ad.language_id' => Yii::$app->language]);
                    }])
                ->where(['in', 'attribute.attribute_id', $attribute_ids])
                ->indexBy('attribute_id')
                ->orderBy('sort_order')
                ->all();

            $attrs = [];
            foreach ($attributes as $key => $ad) {
                $modelsAd[$key] = $ad;
                $modelsAd[$key]['agdName'] = $ag->attributeGroupDescriptions->name;
                $modelsAd[$key]['adName'] = $ad->attributeDescriptions['name'];
                $attrs[$key] = [
                    'name' => $ad->attributeDescriptions['name'],
                    'ad' => $ad,
                ];
            }
            $modelsAdContent[$ag->attributeGroupDescriptions->name] = $attrs;
        }
        return [$modelsAd, $modelsAdContent];
    }

    public
    static function getCategoryParents($category_id)
    {
        return CategoryPath::find()
            ->joinWith('category c')
            ->where(['category_path.category_id' => $category_id])
            ->orderBy('category_path.path_id')
            ->all();
    }

}
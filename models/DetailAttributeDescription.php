<?php

namespace app\models;

use yii\db\ActiveRecord;

class DetailAttributeDescription extends ActiveRecord
{
    public function getDetailAttribute()
    {
        return $this->hasOne(DetailAttribute::class, ['detail_id' => 'detail_id']);
    }
}
<?php

namespace app\models;

use yii\db\ActiveRecord;

class Attribute extends ActiveRecord
{
    public $value;
    public $agdName;
    public $adName;

    const ATTRIBUTE_ID_NOSTANDART = 58;

    public function rules()
    {
        return [
            [['value'], 'safe'],
        ];
    }

    public function getAttributeDescriptions()
    {
        return $this->hasOne(AttributeDescription::class, ['attribute_id' => 'attribute_id']);
    }

}
<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CartForm extends Model
{
    const DOORSDOORS = 'DoorsDoors';
    const DOORSWAREHOUSE = 'DoorsWarehouse';

    public $telephone;
    public $lastName; //фамилия
    public $firstName; //имя
    public $middleName; //отчество
    public $serviceType; //DoorsDoors, DoorsWarehouse

    public $city;

    public $street;
    public $building;
    public $flat;

    public $warehouse;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['telephone', 'lastName', 'firstName', 'middleName', 'serviceType', 'city'], 'required'],
            ['warehouse', 'validateWarehouse', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['street', 'validateStreet', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['building', 'validateBuilding', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'telephone' => 'Телефон',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'middleName' => 'Отчество',
            'serviceType' => 'Тип доставки',
            'city' => 'Нас. пункт',
            'street' => 'Улица',
            'building' => 'Дом',
            'flat' => 'Квартира',
            'warehouse' => 'Отделение',
        ];
    }

    public function validateWarehouse($attribute, $params)
    {
        $field = trim($this->warehouse);
        if ($this->serviceType == self::DOORSWAREHOUSE && (isset($field) === true && $field === '')) {
            $this->addError($attribute, 'Необходимо заполнить «Отделение».');
        }
    }

    public function validateStreet($attribute, $params)
    {
        $field = trim($this->street);
        if ($this->serviceType == self::DOORSDOORS && (isset($field) === true && $field === '')) {
            $this->addError($attribute, 'Необходимо заполнить «Улицу».');
        }
    }

    public function validateBuilding($attribute, $params)
    {
        $field = trim($this->building);
        if ($this->serviceType == self::DOORSDOORS && (isset($field) === true && $field === '')) {
            $this->addError($attribute, 'Необходимо заполнить «Дом».');
        }
    }
}
<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class OnCreditForm extends Model
{
    public $monthCount;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['monthCount', 'required', 'message' => Yii::t('app', 'Необходимо заполнить "Срок кредита в месяцах') . '!'],
            ['monthCount', 'number', 'min' => 1, 'max' => 24,],
        ];
    }

    public function attributeLabels()
    {
        return [
            'monthCount' => Yii::t('app', 'Срок кредита в месяцах') . ':',
        ];
    }
}
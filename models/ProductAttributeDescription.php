<?php

namespace app\models;

use yii\db\ActiveRecord;

class ProductAttribute extends ActiveRecord
{

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['product_id' => 'product_id']);
    }

    public function getMyAttributes()
    {
        return $this->hasMany(Attribute::class, ['attribute_id' => 'attribute_id']);
    }

    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttribute::class, ['product_id' => 'product_id']);
    }
}
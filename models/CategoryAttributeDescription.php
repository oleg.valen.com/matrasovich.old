<?php

namespace app\models;

use yii\db\ActiveRecord;

class CategoryAttributeDescription extends ActiveRecord
{

    public static function getCategoryAttributeDescription($category_id, $attribute_id, $language_id)
    {

        return CategoryAttributeDescription::find()
            ->where(['category_id' => $category_id, 'attribute_id' => $attribute_id, 'language_id' => $language_id])
            ->one();

    }

}

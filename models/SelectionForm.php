<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SelectionForm extends Model
{
    public $sortAsc;
    public $sortDesc;
    public $categoryId;

    public function rules()
    {
        return [
            [['sortAsc', 'sortDesc', 'categoryId'], 'safe'],
        ];
    }
}
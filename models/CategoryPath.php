<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class CategoryPath extends ActiveRecord
{
    public function getCategory()
    {
        return $this->hasOne(CategoryDescription::class, ['category_id' => 'path_id'])
            ->onCondition(['language_id' => Yii::$app->language]);
    }
}
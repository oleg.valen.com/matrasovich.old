<?php

namespace app\models;

use rico\yii2images\models\Image;
use Yii;
use yii\base\Model;
use yii\validators\FileValidator;
use yii\web\UploadedFile;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ReviewForm extends Model
{
    public $product_id;
    public $name;
    public $text;
    public $rate;
    public $phone;
    public $imageFiles;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'text', 'rate'], 'required'],
            ['phone', 'string', 'max' => 13],
            ['rate', 'integer', 'min' => 1, 'max' => 5],
            ['phone', 'safe'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'minFiles' => 2, 'maxFiles' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'text' => 'Отзыв',
            'phone' => 'Телефон',
            'rate' => 'Оценка',
            'imageFiles' => 'Фото',
        ];
    }

    public function upload($review)
    {
        $first = true;
        foreach ($this->imageFiles as $file) {
            $filePath = Yii::$app->runtimePath . DIRECTORY_SEPARATOR . $file->baseName . '.' . $file->extension;
            $file->saveAs($filePath, true);
            $review->attachImage($filePath, $first);
            unlink($filePath);
            $first = false;
        }
    }

}
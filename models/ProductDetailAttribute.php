<?php

namespace app\models;

use yii\db\ActiveRecord;

class ProductDetailAttribute extends ActiveRecord
{
    public function getDetailAttributeDescriptions()
    {
        return $this->hasMany(DetailAttributeDescription::class, ['detail_id' => 'detail_id']);
    }
}
<?php

namespace app\models;

use rico\yii2images\models\Image;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;

class Review extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public function rules()
    {
        return [
            [['review_id', 'product_id', 'text', 'created_at', 'rate', 'phone'], 'required'],
        ];
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->created_at = time();
        }
        parent::init();
    }

    public static function getReviewsStars($productIds)
    {
        //fas fa-star - 1
        //fas fa-star-half-alt - 0.5
        //far fa-star - 0
        $reviews = Review::find()
            ->select(['product_id', 'COUNT(*) as count', 'SUM(rate) as rate', 'AVG(rate) as avgrate'])
            ->where(['in', 'product_id', $productIds])
            ->andWhere(['is not', 'status', null])
            ->groupBy(['product_id'])
            ->indexBy('product_id')
            ->asArray()
            ->all();
        foreach ($reviews as &$r) {
            $r['rates'] = self::getRateClasses($r['rate'], $r['count']);
        }
        return $reviews;
    }

    public static function getReviews($productId)
    {
        //fas fa-star - 1
        //fas fa-star-half-alt - 0.5
        //far fa-star - 0
        $reviews = [];
        $models = Review::find()
            ->where(['product_id' => $productId])
            ->andWhere(['is not', 'status', null])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        foreach ($models as $m) {
            $reviews[] = [
                'review' => $m,
                'review_id' => $m->review_id,
                'rates' => self::getRateClasses($m->rate, 1),
            ];
        }
        return $reviews;
    }

    public static function getRateClasses($rate, $count)
    {
        $rates = [];
        $left = 5;
        $rate = round(2 * ($rate / $count)) / 2;
        foreach (range(1, floor($rate)) as $i) {
            $rates[] = 'fas fa-star';
            $left--;
        }
        if ($rate - floor($rate) == 0.5) {
            $rates[] = 'fas fa-star-half-alt';
            $left--;
        }
        for ($i = 1; $i <= $left; $i++) {
            $rates[] = 'far fa-star';
        }

        return $rates;
    }

    public static function reviewsHasImages($reviewIds)
    {
        $model = Image::find()
            ->where(['in', 'itemId', $reviewIds])
            ->andWhere(['modelName' => 'Review'])
            ->limit(1)
            ->one();
        if ($model)
            return true;
        return false;
    }

    public static function getReviewsImages($reviews)
    {
        foreach ($reviews as &$r) {
            $images = $r['review']->getImages();
            if (count($images) == 1 && get_class($images[0]) == 'rico\yii2images\models\PlaceHolder') {
                $r['images'] = [];
                continue;
            }
            foreach ($images as $image) {
                $r['images'][] = $image;
            }
        }
        return $reviews;
    }

    public static function create(ReviewForm $modelReview)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            $review = new Review();
            $review->product_id = Html::decode($modelReview['product_id']);
            $review->name = Html::decode($modelReview['name']);
            $review->text = Html::decode($modelReview['text']);
            $review->rate = Html::decode($modelReview['rate']);
            $review->phone = Html::decode($modelReview['phone']);
            $review->save(false);

            if ($modelReview->imageFiles) {
                $modelReview->upload($review);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
        }

    }

}
<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\db\Exception;


class CallUs extends ActiveRecord
{

    public static function add()
    {
        $request = Yii::$app->request->get();
        $callUs = new CallUs();
        $callUs->date_added = date("Y-m-d H:i:s");
        $callUs->text = $request['text'];
        $callUs->phone = $request['phone'];
        $callUs->save();

        Yii::$app->mailer->compose()->
        setFrom(['admin@matrasovich.com.ua' => 'matrasovich.com.ua'])->
//        setTo('oleg.valen.com@gmail.com')->
        setTo(['oleg.valen.com@gmail.com', 'zinkovskaya.t11@gmail.com'])->
        setSubject('Новый вопрос!!! ' . date("Y-m-d H:i:s") . ' ' . $request['phone'])->
        setHtmlBody($request['text'])->
        send();

    }

}
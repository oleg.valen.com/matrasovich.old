<?php
/**
 * Created by PhpStorm.
 * User: oleg-d
 * Date: 13.05.18
 * Time: 17:16
 */

namespace app\controllers;

use app\models\Product;
use app\models\Serie;
use Yii;
use yii\web\Controller;
use app\models\SearchForm;

class SerieController extends Controller
{

    public function actionIndex()
    {

        $data = [];
        $get = Yii::$app->request->get();

        $this->view->params['modelSearchForm'] = new SearchForm();

        $serie = Serie::getSerie($get['serie_id']);
        $data['serie'] = $serie;
        $data['products'] = Product::getProductSimilar([], '', implode(Serie::getProductIds($serie->detail_id), ','));

        return $this->render('index', $data);
    }

}
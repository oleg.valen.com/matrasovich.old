<?php
/**
 * Created by PhpStorm.
 * User: oleg-d
 * Date: 13.05.18
 * Time: 17:16
 */

namespace app\controllers;

use app\models\Attribute;
use app\models\Category;
use app\models\NostandartForm;
use app\models\OnCreditForm;
use app\models\Product;
use app\models\Cart;
use app\models\ProductOption;
use app\models\Review;
use app\models\ReviewForm;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\i18n\Formatter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use app\models\SearchForm;
use yii\helpers\Url;
use yii\httpclient\Client;
use simialbi\yii2\schemaorg\models\Brand as BrandLd;
use simialbi\yii2\schemaorg\models\Product as ProductLd;
use simialbi\yii2\schemaorg\models\Rating as RatingLd;
use simialbi\yii2\schemaorg\models\AggregateRating as AggregateRatingLd;
use simialbi\yii2\schemaorg\models\Person as PersonLd;
use simialbi\yii2\schemaorg\models\Review as ReviewLd;
use simialbi\yii2\schemaorg\models\Offer as OfferLd;
use simialbi\yii2\schemaorg\models\OfferShippingDetails as OfferShippingDetailsLd;
use simialbi\yii2\schemaorg\models\DefinedRegion as DefinedRegionLd;
use simialbi\yii2\schemaorg\models\ShippingDeliveryTime as ShippingDeliveryTimeLd;
use simialbi\yii2\schemaorg\models\QuantitativeValue as QuantitativeValueLd;
use simialbi\yii2\schemaorg\models\OpeningHoursSpecification as OpeningHoursSpecificationLd;
use simialbi\yii2\schemaorg\helpers\JsonLDHelper;

class ProductController extends Controller
{

    public function beforeAction($action)
    {
        if (in_array($action->id, ['get-review-images'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        $data = $reviewsLd = [];
        $noindex = 'index';
        $nofollow = 'follow';
        $price_nostantart = 0;
        $reviewValidated = null;

        $this->view->params['modelSearchForm'] = new SearchForm();

        $get = Yii::$app->request->get();

        $modelReview = new ReviewForm();
        if ($modelReview->load(Yii::$app->request->post())) {
            $modelReview->imageFiles = UploadedFile::getInstances($modelReview, 'imageFiles');
            $reviewValidated = $modelReview->validate();
            if ($reviewValidated) {
                Review::create($modelReview);
                $this->refresh();
            }
        }
        $data['reviewIsActive'] = $reviewValidated === false ? ' is-active' : '';


        $productQuery = Product::getProductQueryById($get['product_id']);
        $product = $productQuery->one();

        if($product->status != 1) {
            throw new NotFoundHttpException();
        }

        $categoryParents = Category::getCategoryParents($product->category_id);
        $links = [];
        foreach ($categoryParents as $key => $cp) {
            if ($key == 0) continue;
            $links[] = ['label' => $cp->category->name, 'url' => ["category/{$cp['path_id']}"]];
        }
//        $links[] = ['label' => $product->category->name, 'url' => [Url::to(['category/index', 'category_id' => $product['category_id'], 's1' => $product->category->seo_url, 's2' => 'c'])]];
        $links[] = ['label' => $product->category->name, 'url' => ["category/c{$product['category_id']}"]];
        $links[] = $product->name;

        $data['breadcrumbs'] = Breadcrumbs::widget([
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);

        $data['product'] = $product;

        $options = array_map(function ($item) use ($product) {
            return [
                'attribute_id' => $item->attribute_id,
                'value' => $item->value,
                'name' => $item->attributeDescription->name,
                'delivery' => Product::getDelivery($product->category_id, $item->attributeDescription->name, $item->value)
            ];
        }, Product::getProductOptions($product->product_id));


        $data['options'] = $options;

        if (count($data['options'])) {
            $data['price'] = $data['options'][0]['value'];
        } else {
            $data['price'] = 0;
        };

        foreach ($data['options'] as $option) {
            if ($option['attribute_id'] == Attribute::ATTRIBUTE_ID_NOSTANDART) {
                $price_nostantart = (float)$option['value'];
            }
        }

        $stickerDiscount = 0;
        $oldSum = 0;
        foreach ($product->productStickers as $productSticker) {

            if (substr($productSticker->sticker->name, 0, 1) == '-') {
                $pos = strpos($productSticker->sticker->name, '%');
                $stickerDiscount = intval(substr($productSticker->sticker->name, 1, $pos - 1));
                $oldSum = Yii::$app->formatter->asInteger($product->price / ((100 - $stickerDiscount) / 100));
            }
        }

        $data['stickerDiscount'] = $stickerDiscount;
        $data['oldSum'] = $oldSum;

        $data['cartProductsIDs'] = Cart::getProductsIDs();

        $data['detailAttributes'] = Product::getProductDetailAttributes($product->product_id);

        $data['productSimilar'] = Product::getProductSimilar($get, $product->product_id);
        $data['isSized'] = isset($get['s']);
        $data['attributeId'] = isset($get['s']) ? $get['s'] : '';

        if (key_exists('s', $get)) {
            $noindex = 'noindex';
            $nofollow = 'nofollow';
        }

        $data['robots'] = $noindex . ', ' . $nofollow;

        $nostandartForm = new NostandartForm();
        $nostandartForm->price = $price_nostantart;
        $data['modelNostandartForm'] = $nostandartForm;

        $data['modelOnCreditForm'] = new OnCreditForm();

        $reviewsStars = Review::getReviewsStars([$product->product_id]);
        $data['reviewsStars'] = $reviewsStars;
        $reviews = Review::getReviews($product->product_id);
        $reviews = Review::getReviewsImages($reviews);
        $data['reviews'] = $reviews;
        $data['reviewsHasImages'] = Review::reviewsHasImages(ArrayHelper::getColumn($reviews, 'review_id'));
        $data['modelReview'] = $modelReview;

        $data['delivery'] = Product::getDelivery($product->category_id, '70x190', $data['price']);

        //https://developers.facebook.com/docs/payments/product
        $data['opengraph'] = Yii::$app->opengraph
            ->getProduct()
            ->setTitle($product->productDescription->h1)
            ->setType('product')
            ->setUrl(Url::base(true) . Url::current())
            ->setDescription($product->productDescription->description)
            ->setLocale(Yii::$app->language == 'ru' ? 'ru_RU' : 'ua_UA')
            ->setLocalAlternate([Yii::$app->language == 'ru' ? 'ua_UA' : 'ru_RU'])
            ->setImage(Url::base(true) . DIRECTORY_SEPARATOR . $product->getImage()->getPath('650x650'))
            ->setWidth(['650'])
            ->setHeight(['650'])
            ->setPrice([$data['price']])
            ->setCurrency(['UAH']);


        $productLd = new ProductLd();
        $productLd->name = $product->productDescription->h1;
        $productLd->image = Url::base(true) . DIRECTORY_SEPARATOR . $product->getImage()->getPathToOrigin();
        $productLd->url = Url::base(true) . Url::current();
        $productLd->description = $product->productDescription->description;
        $productLd->sku = $product->product_id;

        $brandLd = new BrandLd();
        $brandLd->name = 'Екатеринославские мебельные мастерские';
        $brandLd->sameAs = 'https://emm.ua';
        $brandLd->description = Yii::t('app', 'ЕММ - ведущий производитель матрасов в Украине');
        $brandLd->alternateName = 'ЕММ';
        $productLd->brand = $brandLd;

        if (!empty($reviews)) {
            $reviewLd = new ReviewLd();
            $ratingLd = new RatingLd();
            $ratingLd->ratingValue = $reviews[0]['review']->rate;
            $authorLd = new PersonLd();
            $authorLd->name = $reviews[0]['review']->name;
            $reviewLd->reviewRating = $ratingLd;
            $reviewLd->author = $authorLd;
            $productLd->review = $reviewLd;

            $aggRating = new AggregateRatingLd();
            $aggRating->ratingValue = round($reviewsStars[$product->product_id]['avgrate']);
            $aggRating->reviewCount = count($reviews);
            $productLd->aggregateRating = $aggRating;
        }

        $offer = new OfferLd();
        $offer->priceCurrency = 'UAH';
        $offer->price = $product->price;
        $offer->priceValidUntil = date('Y-m-d', strtotime('last day of this month'));
        $offer->itemCondition = 'https://schema.org/NewCondition';
        $offer->availability = 'https://schema.org/InStock';
        $offer->url = Url::base(true) . Url::current();

        $shippingDestination = new DefinedRegionLd();
        $shippingDestination->addressCountry = 'UA';

        $deliveryTime = new ShippingDeliveryTimeLd();
        $transitTime = new QuantitativeValueLd();
        $transitTime->minValue = 1;
        $transitTime->maxValue = 7;
        $businessDays = new OpeningHoursSpecificationLd();
        $businessDays->dayOfWeek = [
            "https://schema.org/Monday",
            "https://schema.org/Tuesday",
            "https://schema.org/Wednesday",
            "https://schema.org/Thursday",
            "https://schema.org/Friday",
            "https://schema.org/Saturday",
            "https://schema.org/Sunday"
        ];
        $deliveryTime->transitTime = $transitTime;
        $deliveryTime->businessDays = $businessDays;
        $deliveryTime->cutoffTime = "21:00-09:00";

        $shippingDetails = new OfferShippingDetailsLd();
        $shippingDetails->shippingRate = Product::getMonetaryAmount($product->category_id);
        $shippingDetails->shippingDestination = $shippingDestination;
        $shippingDetails->deliveryTime = $deliveryTime;
        $offer->shippingDetails = $shippingDetails;

        $productLd->offers = $offer;

        JsonLDHelper::add($productLd);

        return $this->render('index', $data);
    }

    public function actionInfo()
    {
        $post = Yii::$app->request->post();
        $product_id = $post['product_id'];
        $detail_id = $post['detail_id'];
        $pdd = Product::getProductDetailDescription($product_id, $detail_id);
        return json_encode([
            'key' => $pdd[0]['detailAttributeDescriptions'][0]['value'],
            'imageSrc' => $pdd[0]['detailAttributeDescriptions'][0]['detailAttribute']['image'],
            'imageAlt' => $pdd[0]['detailAttributeDescriptions'][0]['value'],
            'text' => $pdd[0]['detailAttributeDescriptions'][0]['text'],
        ]);
    }

    public function actionGetproducts()
    {
        $html = '';
        $language = Yii::$app->request->cookies->getValue('_language', 'ru');
        $titleSeenProducts = $language == 'ru' ? 'Просмотренные товары' : ($language == 'ua' ? 'Переглянуті товари' : 'Viewed');
        $language = $language == 'ru' ? '' : '/' . $language;
        $products = Product::getProductsByOrderLastSeen();
        if (empty($products)) {
            return '';
        }

        $html .= "<div class='title is-size-5'>{$titleSeenProducts}</div>";
        $html .= "<div class='columns is-multiline is-mobile'>";

        foreach ($products as $item) {
            $html .= "<div class='column is-4-mobile is-2-tablet is-1-desktop'>";
            $url = Url::to($language . "/{$item->seo_url}/p{$item->product_id}");
//            $url = Url::to(['product/index', 'product_id' => $item->product_id, 's1' => $item->seo_url, 's2' => 'p']);
            $html .= "<p class='has-text-centered'><a href='{$url}' title='{$item['name']}'><img src='/{$item->getImage()->getPath('250x250')}' alt='{$item->name}'</a></p>";
            $html .= "</div>";
        }
        return $html;
    }

    public function actionOncredit()
    {

        $post = Yii::$app->request->post();
        if (empty($post['price']) || empty($post['monthCount']) || empty($post['name'])) {
            return;
        }

        $password = 'cc898afceaa2419a9c0c2a679430ace4';
        $storeId = 'D22DF00457AD41E892D2';
        $orderId = uniqid();
        $amount = $post['price'];
        $partsCount = $post['monthCount'];
        $merchantType = 'II';
        $responseUrl = '';
        $redirectUrl = Url::home(true);
        $productsName = $post['name'];
        $productsCount = 1;
        $productsPrice = $post['price'];
        $productsString = $productsName . $productsCount . round($productsPrice) . '00';
        $signature = base64_encode(sha1($password . $storeId . $orderId . (string)($amount * 100) . $partsCount . $merchantType . $responseUrl . $redirectUrl . $productsString . $password, true));


        $client = new Client(['baseUrl' => 'https://payparts2.privatbank.ua/ipp/v2/payment/create']);
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setHeaders(['accept' => 'application/json; charset=utf-8'])
            ->setData([
                'storeId' => $storeId,
                'orderId' => $orderId,
                'amount' => $amount,
                'partsCount' => $partsCount,
                'merchantType' => $merchantType,
                'products' => [
                    ['name' => $productsName, 'count' => $productsCount, 'price' => $productsPrice],
                ],
//                'recipientId' => $recipientId,
                'responseUrl' => '',
                'redirectUrl' => $redirectUrl,
                'signature' => $signature,
            ])
            ->send();

        if ($response->data['state'] == 'SUCCESS') {
            return $this->redirect('https://payparts2.privatbank.ua/ipp/v2/payment?token=' . $response->data['token']);
        }
    }

    public function actionGetReviewImages()
    {
        $html = '';
        $data = json_decode(Yii::$app->request->post()['data'], true);
        $reviewId = $data['reviewId'];
        $review = Review::findOne($reviewId);
        if ($review) {
            $data['success'] = true;
            $data['name'] = Html::decode($review->name);
            $data['created_at'] = date('d.m.Y', $review->created_at);
//            $html .= "<div class='columns'>";
            foreach ($review->getImages() as $image) {
                $html .= "<div class='columns'><div class='column'><figure class='image'><img src='/{$image->getPath('650x')}'></figure></div></div>";
            }
//            $html .= "</div>";
            $data['images'] = $html;
            return json_encode($data);
        }
        return json_encode(['success' => false]);

    }


}
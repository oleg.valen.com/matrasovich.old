<?php
/**
 * Created by PhpStorm.
 * User: oleg-d
 * Date: 13.05.18
 * Time: 17:16
 */

namespace app\controllers;

use app\models\AttributeDescription;
use app\models\Category;
use app\models\CategoryAttributeDescription;
use app\models\Product;
use app\models\Cart;

//use http\Url;
use app\models\Review;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\widgets\Breadcrumbs;
use app\models\SelectionForm;
use yii\base\Model;
use app\models\SearchForm;

class CategoryController extends Controller
{

    public function actionIndex()
    {

        $this->view->params['modelSearchForm'] = new SearchForm();
        $data = [];
        $cache = Yii::$app->cache;
        $filterDelete = [];
        $reviews = [];

        $get = Yii::$app->request->get();
        $sortOrder = 'sort_order ASC';
        $data['sortName'] = Yii::t('app', 'Сортировать') . ':';
        if (isset($get['sort'])) {
            if ($get['sort'] == 'cheap') {
                $sortOrder = 'price ASC';
                $data['sortName'] = 'По возрастанию цены';
            } elseif ($get['sort'] == 'expensive') {
                $sortOrder = 'price DESC';
                $data['sortName'] = 'По убыванию цены';
            } elseif ($get['sort'] == 'rating') {
                $data['sortName'] = 'По рейтингу';
            }
        }

//        $categoryQuery = Category::getCategoryQueryBySeoUrl($get['category']);
        $categoryQuery = Category::getCategoryQueryById($get['category_id']);
        $category = $categoryQuery->one();

        $categoryParents = Category::getCategoryParents($category->category_id);
        $links = [];
        foreach ($categoryParents as $key => $cp) {
            if ($key == 0) continue;
            $links[] = ['label' => $cp->category->name, 'url' => ["category/{$cp['path_id']}"]];
        }
        $links[] = $category->categoryDescriptions->name;

        $data['breadcrumbs'] = Breadcrumbs::widget([
//            'homelink' => ['label' => Yii::t('app', 'Главная'), 'url' => '/'],
            'options' => ['class' => 'breadcrumb'],
            'itemTemplate' => "<li>{link}</li>\n",
            'links' => $links,
        ]);


//        $cache->delete('categoryCacheFilter' . $category->category_id);
//        $data['filter'] = $cache->getOrSet('categoryCacheFilter' . $category->category_id, function () use ($categoryQuery) {
//            return Category::getFilter($categoryQuery);
//        }, 86400);

        $getFilter = $this->getGetFilter($get);
        $data['filter'] = Category::getFilter($categoryQuery, $get);
        $data['filter'] = $this->addUrlToFilter($get, $data['filter'], $getFilter);
        $data['title'] = $category->name;

        foreach ($data['filter'] as $filter) {
            foreach ($filter['attrs'] as $attr) {
                if ($attr['checked'] == ' checked') {
                    $filterDelete[$attr['name']] = $attr['href'];
                }
            }
        }

        $data['filterDeleteClearHref'] = Url::to(['category/index', 'category_id' => $get['category_id'], 's1' => $get['s1'], 's2' => $get['s2']]);
        $data['filterDelete'] = $filterDelete;

        $productAttributeIds = $this->getProductAttributeIds($getFilter, $data['filter']);
        $isSized = array_key_exists('size', $productAttributeIds) && count($productAttributeIds['size']) == 1 ? true : false;
        $attributeId = $isSized ? $productAttributeIds['size'][0] : 0;
        $data['isSized'] = $isSized;
        $data['isNoStandart'] = array_key_exists('size', $getFilter) && in_array('no-standart', $getFilter['size']);
        $data['size'] = $isSized ? $getFilter['size'][0] : 0;
        $data['attributeId'] = $attributeId;

        $productsQuery = Product::getProductsQueryByCategory($category->category_id, $productAttributeIds, $isSized, $attributeId);
        $pages = new Pagination(['totalCount' => $productsQuery->count(), 'pageSize' => 9]);
        $pages->pageSizeParam = false;

        $products = $productsQuery->offset($pages->offset)->orderBy($sortOrder)->limit($pages->limit)->all();
        $data['products'] = $products;
        $data['pages'] = $pages;

        $adTextDescription = '';
        list($data['description'], $data['keywords'], $data['robots'], $data['title'], $data['h1'], $adTextDescription) =
            $this->getMetaTags($category, $this->getSortedFilter($getFilter, $data['filter']), $data['filter'], $get);

        if ($data['robots'] == 'index, follow') {
//            $data['text_description'] = empty($adTextDescription) ? $category->categoryDescriptions->text_description : $adTextDescription;
            if (!$getFilter) {
                $data['text_description'] = $category->categoryDescriptions->text_description;
            } else {
                $data['text_description'] = $adTextDescription;
            }
        } else
            $data['text_description'] = '';

//        $data['text_description'] = $data['description'];

        $data['cartProductsIDs'] = Cart::getProductsIDs();

        $btnFilter = Yii::$app->response->cookies->getValue('btn-filter') ? Yii::$app->response->cookies->getValue('btn-filter') : Yii::$app->request->cookies->getValue('btn-filter');
        $data['btnFilter'] = !isset($btnFilter) || trim($btnFilter) === '' ? 'times' : $btnFilter;

        $productIds = ArrayHelper::getColumn($products, 'product_id');
        $reviewStars = Review::getReviewsStars($productIds);

        $data['reviewsStars'] = $reviewStars;
        foreach ($reviewStars as $k => $v) {
            $reviews[$k] = [
                'reviewsHasImages' => Review::reviewsHasImages(ArrayHelper::getColumn(Review::getReviews($k), 'review_id')),
            ];
        }
        $data['reviews'] = $reviews;

        return $this->render('index', $data);
    }

    public function actionSelection()
    {

        throw new \yii\web\NotFoundHttpException();

        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Подбор матраса по параметрам';
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Подбор топпера на диван, ортопедического матраса по параметрам']);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'подбор топпера на диван, подбор матраса, выбрать топпер на диван, выбрать ортопедический матрас']);

        $data = $modelsAd = $modelsAdContent = $productAttributeIds = $categories = [];
        $attributeId = $size = '';
        $post = Yii::$app->request->post();
        $modelSa = new SelectionForm();
        $categoryIsChanged = false;

        foreach (Category::find()->indexBy('category_id')->orderBy('sort_order')->where(['!=', 'category_id', 1])->asArray()->all() as $key => $item) {
            $categories[$key] = $item['name'];
        }
        $data['categories'] = $categories;

        if (isset($post['category-id'])) {
            $categoryIsChanged = true;
            $categoryId = $post['category-id'];
        } elseif (isset($post['SelectionForm'])) {
            $categoryId = $post['SelectionForm']['categoryId'];
        } else {
            reset($categories);
            $categoryId = key($categories);
        }

        $categoryQuery = Category::getCategoryQueryById($categoryId);
        $category = $categoryQuery->one();
//        $data['categoryName'] = $category->name;

        list($modelsAd, $modelsAdContent) = Category::getModelsAd($categoryQuery, $modelsAd, $modelsAdContent);
        if (Model::loadMultiple($modelsAd, $post) && $modelSa->load($post)) {
            //возникала ошибка Confirm Form Resubmission при обратном переходе
            //Yii::$app->response->getHeaders()->add('Cache-Control', 'no-store, no-cache, must-revalidate'); //было так
            Yii::$app->response->getHeaders()->add('Cache-Control', 'no-cache, must-revalidate');
            $data['isPost'] = true;
            foreach ($modelsAd as $key => $a) {
                if ($a->value == 1) {
                    $productAttributeIds[$a->agdName][] = $key;
                    if ($a->agdName == 'Размер') {
                        $attributeId = $key;
                    }
                }
            }

            if (!$categoryIsChanged) {
                $products = Product::getProductsQueryByCategory($category->category_id, $productAttributeIds, true, $attributeId)->all();
                if ($modelSa->sortAsc == 1) {
                    usort($products, function ($a, $b) {
                        return intval($a->productOption->value) < intval($b->productOption->value) ? -1 : 1;
                    });
                } else {
                    usort($products, function ($a, $b) {
                        return intval($b->productOption->value) < intval($a->productOption->value) ? -1 : 1;
                    });
                }
                $data['products'] = $products;
            } else {
                foreach ($modelsAd as $key => $a) {
                    $a->value = 0;
                }
                foreach ($modelsAd as $key => $a) {
                    if (strcmp($a->agdName, 'Размер') == 0) {
                        $a->value = 1;
                        break;
                    }
                }
            }

        } else {
            $data['isPost'] = false;
            foreach ($modelsAd as $key => $a) {
                if (strcmp($a->agdName, 'Размер') == 0) {
                    $a->value = 1;
                    break;
                }
            }
            $modelSa->sortAsc = 1;
        }

        $data['modelSa'] = $modelSa;
//        $data['size'] = $size;
        $data['attributeId'] = $attributeId;
        $data['modelsAd'] = $modelsAd;
        $data['modelsAdContent'] = $modelsAdContent;
        $data['categoryIsChanged'] = $categoryIsChanged;

        return $this->render('selection', $data);
    }

    public function addUrlToFilter($get, &$filter, $getFilter)
    {
        ///toppery/filter/category=premium,nedorogie;size=90x190
        foreach ($filter as &$agd) {
            $agdExists = key_exists($agd['seo_url'], $getFilter);
            foreach ($agd['attrs'] as &$ad) {
                $getFilterCopy = $getFilter;
                if ($agdExists) {
                    $key = array_search($ad['seo_url'], $getFilterCopy[$agd['seo_url']]);
                    if ($key !== false) {
                        unset($getFilterCopy[$agd['seo_url']][$key]);
                        $checked = ' checked';
                    } else {
                        $getFilterCopy[$agd['seo_url']][] = $ad['seo_url'];
                        $checked = '';
                    }
                } else {
                    $getFilterCopy[$agd['seo_url']][] = $ad['seo_url'];
                    $checked = '';
                }
                $sortedFilter = $this->getSortedFilter($getFilterCopy, $filter);
//                $href = "/{$get['category']}/c{$get['category_id']}" . (count($sortedFilter) != 0 ? '/filter/' : '');
//                $href = "/{$get['s1']}/c{$get['category_id']}" . (count($sortedFilter) != 0 ? '/' : '');
//                foreach ($sortedFilter as $key => $value) {
//                    $href .= "{$key}=" . implode(',', $value) . ';';
//                }
//                $ad['href'] = rtrim($href, ';');

                $suffix = '';
                foreach ($sortedFilter as $key => $value) {
                    $suffix .= "{$key}=" . implode(',', $value) . ';';
                }
                $suffix = rtrim($suffix, ';');
                $params = ['category/index', 'category_id' => $get['category_id'], 's1' => $get['s1'], 's2' => $get['s2']];
                $parsedUrl = parse_url(Yii::$app->request->url);
                if ($suffix)
                    $params['s3'] = $suffix . (isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '');
                $ad['href'] = urldecode(Url::to($params));
                $ad['checked'] = $checked;
                $ad['rel'] = ($getFilter) ? ' rel="nofollow"' : '';
            }
            unset($ad);
        }
        unset($agd);
        return $filter;
    }

    public function getGetFilter($get)
    {
        $getFilter = [];
        foreach ($get as $key => $value) {
            if (isset($get['s3'])) {
                foreach (explode(';', $get['s3']) as $v1) {
                    $v2 = explode('=', $v1);
                    $getFilter[$v2[0]] = explode(',', $v2[1]);
                }
            }
        }
        return $getFilter;
    }

    public function getSortedFilter($getFilter, $filter)
    {
        $sortedFilter = [];
        foreach ($filter as $key => $agd) {
            if (key_exists($agd['seo_url'], $getFilter)) {
                foreach ($agd['attrs'] as $ag) {
                    if (in_array($ag['seo_url'], $getFilter[$agd['seo_url']]))
                        $sortedFilter[$agd['seo_url']][] = $ag['seo_url'];
                }
            }
        }
        return $sortedFilter;
    }

    public function getProductAttributeIds($getFilter, $filter)
    {

        $ids = [];
        foreach ($filter as $agd) {
            if (key_exists($agd['seo_url'], $getFilter)) {
                $ads = [];
                foreach ($agd['attrs'] as $ad) {
                    if (in_array($ad['seo_url'], $getFilter[$agd['seo_url']])) {
                        $ads[] = $ad['attribute_id'];
                    }
                }
                $ids[$agd['seo_url']] = $ads;
//                $ids[] = $ads;
            }
        }
        return $ids;
    }

    public function getMetaTags($category, $getFilter, $filter, $get)
    {
        $description = $keywords = $robots = $title = $h1 = '';
        $descriptionFollow = $keywordsFollow = $robots = $titleFollow = $h1Follow = '';
        $noindex = 'index';
        $nofollow = 'follow';
        $adTextDescription = '';

        if (empty($getFilter) && empty($filter)) {
            throw new \yii\web\NotFoundHttpException();
        }

        $noi = 0;
        $nof = 0;
        $attribute_id = 0;
        foreach ($filter as $agd) {
            if (key_exists($agd['seo_url'], $getFilter)) {
                if ($agd['noindex'] == 'noindex') {
                    $noindex = 'noindex';
                } elseif ($agd['noindex'] == 'index') {
                    $noi++;
                }
                if ($agd['nofollow'] == 'nofollow') {
                    $nofollow = 'nofollow';
                } elseif ($agd['nofollow'] == 'follow') {
                    $nof++;
                }

//                $description .= "{$agd['description']} ";
//                $keywords .= "{$agd['keyword']} ";
//                $title .= "{$agd['title']} ";
//                $h1 .= "{$agd['h1']} ";
                $num_groups = 0;
                foreach ($agd['attrs'] as $ad) {
                    if (in_array($ad['seo_url'], $getFilter[$agd['seo_url']])) {
                        $description .= "{$ad['description']}, ";
                        $keywords .= "{$ad['keyword']}, ";
                        $title .= "{$ad['title']}, ";
                        $h1 .= "{$ad['h1']}, ";

                        $descriptionFollow = $ad['description'];
                        $keywordsFollow = $ad['keyword'];
                        $titleFollow = $ad['title'];
                        $h1Follow = $ad['h1'];
                        $adTextDescription = $ad['text_description'];

                        $attribute_id = $ad['attribute_id'];

                        $num_groups++;
                        if ($num_groups > 1) {
                            $noindex = 'noindex';
                            $nofollow = 'nofollow';
                            $adTextDescription = '';
                        }
                    }
                }
            }
        }
        if ($noi > 1) {
            $noindex = 'noindex';
        }
        if ($nof > 1) {
            $nofollow = 'nofollow';
        }

        if (key_exists('sort', $get) || key_exists('search', $get)) {
            $noindex = 'noindex';
            $nofollow = 'nofollow';
        }

        if (key_exists('page', $get)) {
            $noindex = 'noindex';
            $nofollow = 'follow';
        }

        if ($getFilter) {

//            if ($noindex == 'noindex') {
//                $description = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($description), ', ') . Yii::$app->params['descriptionEnd'];
//                $keywords = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($keywords), ', ');
//                $title = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($title), ', ');
//                $h1 = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($h1), ', ');
//            } else {
            $categoryAttributeDescription = CategoryAttributeDescription::getCategoryAttributeDescription($category->category_id, $attribute_id, Yii::$app->language);
            if ($categoryAttributeDescription && $noindex == 'index') {
                $description = $categoryAttributeDescription->description . Yii::$app->params['descriptionEnd'];
                $keywords = $categoryAttributeDescription->keyword;
                $title = $categoryAttributeDescription->title;
                $h1 = $categoryAttributeDescription->h1;
                $adTextDescription = $categoryAttributeDescription->text_description;
            } else {
//                $description = $descriptionFollow . Yii::$app->params['descriptionEnd'];
//                $keywords = $keywordsFollow;
//                $title = $titleFollow;
//                $h1 = $h1Follow;
//                }
                $description = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($description), ', ') . Yii::$app->params['descriptionEnd'];
                $keywords = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($keywords), ', ');
                $title = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($title), ', ') . Yii::t('app', Yii::$app->params['titleEnd']);
                $h1 = $category->categoryDescriptions->h1 . ' ' . rtrim(mb_strtolower($h1), ', ');

            }
        } else {
            $description = $category->categoryDescriptions->description . Yii::$app->params['descriptionEnd'];
            $keywords = $category->categoryDescriptions->keyword;
            $title = $category->categoryDescriptions->title;
            $h1 = $category->categoryDescriptions->h1;
        }
        $description = Yii::$app->myComponent->mb_uctoupper($description, 'UTF-8');
        $title = Yii::$app->myComponent->mb_uctoupper($title, 'UTF-8');
        $h1 = Yii::$app->myComponent->mb_uctoupper($h1, 'UTF-8');

        $robots = $noindex . ', ' . $nofollow;

        return [$description, $keywords, $robots, $title, $h1, $adTextDescription];
    }

}
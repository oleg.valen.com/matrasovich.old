<?php
/**
 * Created by PhpStorm.
 * User: oleg-d
 * Date: 13.05.18
 * Time: 17:16
 */

namespace app\controllers;

use app\models\Cart;
use app\models\CartForm;
use app\models\Product;
use app\models\Wishlist;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use Yii;
use app\models\SearchForm;
use app\models\CallUs;

class CartController extends Controller
{

    public function actionIndex()
    {

//        if (Yii::$app->request->isPost)
//            return;

        $data = [];
//        $data['cart'] = [];
        $modelCartForm = new CartForm();
        $modelCartForm->serviceType = 'DoorsWarehouse';
        $data['modelCartForm'] = $modelCartForm;
        $this->view->params['modelSearchForm'] = new SearchForm();
        $html = '';
        $request = Yii::$app->request;
        $language = Yii::$app->request->cookies->getValue('_language', 'ru');
        if ($language == 'ru') {
//            $html .= Наименование  Размер Кол-во Цена Сумма
//            $html .= 'Убрать 1 шт.' 'Добавить 1 шт.' Удалить

            $nameStr = 'Наименование';
            $sizeStr = 'Размер';
            $amountStr = 'Кол-во';
            $priceStr = 'Цена';
            $sumStr = 'Сумма';
            $minusStr = 'Отнять 1 шт.';
            $plusStr = 'Прибавить 1 шт.';
            $deleteStr = 'Удалить';
        } elseif ($language == 'ua') {
            $nameStr = 'Найменування';
            $sizeStr = 'Розмір';
            $amountStr = 'Кількість';
            $priceStr = 'Ціна';
            $sumStr = 'Сума';
            $minusStr = 'Відняти 1 шт.';
            $plusStr = 'Додати 1 шт.';
            $deleteStr = 'Видалити';
        } else {
            $nameStr = 'Name';
            $sizeStr = 'Size';
            $amountStr = 'Amount';
            $priceStr = 'Price';
            $sumStr = 'Sum';
            $minusStr = 'Minus 1 pc.';
            $plusStr = 'Add 1 pc.';
            $deleteStr = 'Delete';
        }
        $language = $language == 'ru' ? '' : '/' . $language;

        $this->view->title = 'Matrasovish.com.ua | Корзина';
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Корзина. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Корзина на Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

        if ($request->isAjax) {
            $html .= "<div class='columns is-hidden-mobile has-text-weight-bold'><div class='column'><div class='columns is-mobile'><div class='column'></div><div class='column'>{$nameStr}</div><div class='column'>{$sizeStr}</div></div></div><div class='column'><div class='columns is-mobile'><div class='column is-one-third'>{$amountStr}</div><div class='column'>{$priceStr}</div><div class='column'>{$sumStr}</div><div class='column'></div></div></div></div><hr>";
            $cart = Product::getProductsOptions($request->post()['cart']);
            foreach ($cart['products'] as $productId => $pas) {
                foreach ($pas as $attributeId => $pa) {
                    $url = Url::to($language . "/{$pa['product']->seo_url}/p{$pa['product']->product_id}");
                    //foreach
                    $html .= "<div class='columns'><div class='column'><div class='columns is-mobile'><div class='column'><a href='{$url}' title='{$pa['product']->productDescriptionLangCookies->name}'<figure class='image is-64x64'><img src='/{$pa['product']->getImage()->getPath('65x65')}' alt='{$pa['pdName']}'</figure></a></div><div class='column'><a href='{$url}'>{$pa['pdName']}</a></div><div class='column'>{$pa['paName']}</div></div></div>";
                    $html .= "<div class='column'><div class='columns is-mobile ok-cart-row'><div class='column is-one-third'><div class='field has-addons ok-cart-addons' data-product-id='{$productId}' data-attribute-id='{$attributeId}'><button class='button is-primary is-inverted ok-cart-minus' title={$minusStr}><span class='icon is-small'><i class='fas fa-minus-circle'></i></span></button><div class='field'> <input class='input is-primary has-text-centered ok-cart-input' type='number' value='{$pa['amount']}'></div><button class='button is-primary is-inverted ok-cart-plus' title='{$plusStr}'><span class='icon is-small'><i class='fas fa-plus-circle'></i></span></button>";
                    $html .= "</div></div><div class='column ok-cart-row-price'>{$pa['price']}</div><div class='column ok-cart-row-sum'>{$pa['sum']}</div><div class='column ok-cart-delete'><a class='is-primary' title='{$deleteStr}' data-product-id='{$productId}' data-attribute-id='{$attributeId}'><span class='icon has-text-primary'><i class='fas fa-lg fa-times-circle'></i></span></a></div></div></div></div><hr>";
                }
            }
            $html .= "<div class='columns has-text-weight-bold'><div class='column is-hidden-mobile'><div class='columns is-mobile'><div class='column'></div><div class='column'></div><div class='column'></div></div></div><div class='column'><div class='columns is-mobile'><div class='column has-text-centered ok-cart-amount'>{$cart['amount']}</div><div class='column'></div><div class='column has-text-centered ok-cart-sum'>{$cart['sum']}</div><div class='column'></div></div></div></div><br>";
            return $html;
        }
//        $data['cart'] = Product::getProductsOptions([72 => [28 => 1, 29 => 2]]);
//        $data['cart'] = Product::getProductsOptions("{\"72\":{\"28\":1, \"29\":2}}");
        return $this->render('index', $data);
    }

    public function actionOrder()
    {

        $params = [];
        $modelCartForm = new CartForm();
        $post = Yii::$app->request->post();

        parse_str($post['form'], $params);
        $modelCartForm->load($params);
        if (!$modelCartForm->validate())
//            return json_encode([
//                'success' => false
//            ]);
            if ($modelCartForm->hasErrors()) {
                return json_encode([
                    'success' => false,
                    'data' => $modelCartForm->errors
                ]);
            }

        //        $cart = Product::getProductsOptions([72 => [28 => 1, 29 => 2]]);
        $cart = Product::getProductsOptions($post['cart']);


        foreach ($cart['products'] as $product) {
            foreach ($product as $option) {
                $callUs = new CallUs();
                $callUs->date_added = date("Y-m-d H:i:s");
                $callUs->text = $option['pdName'] . ', ' . $option['paName'] . ', ' . $option['amount'] . ' шт, ' . $option['price'] . ' грн.';
                $callUs->phone = $modelCartForm['telephone'];
                $callUs->last_name = $modelCartForm['lastName'];
                $callUs->first_name = $modelCartForm['firstName'];
                $callUs->middle_name = $modelCartForm['middleName'];
                $callUs->service_type = $modelCartForm['serviceType'];
                $callUs->city = $modelCartForm['city'];
                $callUs->warehouse = $modelCartForm['warehouse'];
                $callUs->street = $modelCartForm['street'];
                $callUs->building = $modelCartForm['building'];
                $callUs->flat = $modelCartForm['flat'];
                $callUs->save();
            }
        }

        $html = "<p>Телефон: {$modelCartForm['telephone']} </p>
                <p>Количество: {$cart['amount']}</p>
                <p>Сумма: {$cart['sum']}</p>
                <p>Фамилия: {$modelCartForm['lastName']}</p>
                <p>Имя: {$modelCartForm['firstName']}</p>
                <p>Отчество: {$modelCartForm['middleName']}</p>
                <p>Тип (DoorsDoors - адресная доставка; DoorsWarehouse - доставка на отделение): {$modelCartForm['serviceType']}</p>
                <p>Город: {$modelCartForm['city']}</p>
                <p>Отделение: {$modelCartForm['warehouse']}</p>
                <p>Улица: {$modelCartForm['street']}</p>
                <p>Дом: {$modelCartForm['building']}</p>
                <p>Квартира: {$modelCartForm['flat']}</p>
                <ul>";
        foreach ($cart['products'] as $product) {
            foreach ($product as $option) {
                $html .= "<li>{$option['pdName']}, {$option['paName']}, {$option['amount']} шт, {$option['price']} грн.</li>";
            }
        }
        $html .= '</ul>';

        Yii::$app->mailer->compose()->
        setFrom(['admin@matrasovich.com.ua' => 'matrasovich.com.ua'])->
//        setTo('oleg.valen.com@gmail.com')->
        setTo(['oleg.valen.com@gmail.com', 'zinkovskaya.t11@gmail.com'])->
        setSubject('Новый вопрос!!! ' . date("Y-m-d H:i:s"))->
        setHtmlBody($html)->
        send();

        return json_encode([
            'success' => true
        ]);
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->user->setReturnUrl(Yii::$app->request->get('pathname'));
            return $this->redirect('/login');
        }

        $product_id = Yii::$app->request->get('id');
        $attribute_id = Yii::$app->request->get('attribute_id') !== '' ? Yii::$app->request->get('attribute_id') : null;
        $product = Product::findOne(['product_id' => $product_id]);
        if (!$product) return false;

        Cart::add($product_id, $attribute_id);
    }

    public function actionClear()
    {
        $product_id = Yii::$app->request->get('id');
        $product = Product::findOne(['product_id' => $product_id]);
        if (!$product) return false;

        Cart::clear($product_id);
    }

    public function actionClearall()
    {
        Cart::clearAll();
    }

    public function actionCheckout()
    {
        Cart::checktout();
    }

    public function actionWishlist()
    {
        $product_id = Yii::$app->request->get('id');
        $product = Product::findOne(['product_id' => $product_id]);
        if (!$product) return false;

        Cart::clear($product_id);

        $wishlist = new Wishlist();
        $wishlist->add($product);
    }

    public function actionChangeattribute()
    {
        $product_id = Yii::$app->request->get('id');
        $attribute_id = Yii::$app->request->get('attribute_id');
        $attribute_idOld = Yii::$app->request->get('attribute_idOld') !== '' ? Yii::$app->request->get('attribute_idOld') : null;
        return Cart::changeattribute($product_id, $attribute_id, $attribute_idOld);
    }

    public function actionChangeqty()
    {
        $product_id = Yii::$app->request->get('id');
        $attribute_id = Yii::$app->request->get('attribute_id') !== '' ? Yii::$app->request->get('attribute_id') : null;
        $sign = Yii::$app->request->get('sign');
        return Cart::changeqty($product_id, $attribute_id, $sign);
    }

    public function actionGetCities()
    {
        $data = [];
        $post = Yii::$app->request->post();

        $client = new Client(['transport' => 'yii\httpclient\CurlTransport']); // only cURL supports the options we need
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl(Yii::$app->params['novaposhta']['url'])
            ->setData([
                'apiKey' => Yii::$app->params['novaposhta']['apiKey'],
                'modelName' => 'Address',
                'calledMethod' => 'getCities',
                'methodProperties' => [
                    'FindByString' => Html::decode($post['city'])
                ]
            ])
            ->send();
        if (!$response->data['success'])
            return json_encode([
                'success' => false,
                'data' => $response->data['errors']
            ]);
        foreach ($response->data['data'] as $item) {
            $data[$item['AreaDescription']][] = [
                'description' => $item['Description'], //Дніпрельстан
                'ref' => $item['Ref'], //eb54475c-e5e4-11e9-b48a-005056b24375
                'type' => $item['SettlementTypeDescription'], //село
                'area' => $item['AreaDescription'] //Запорізька
            ];
        }

        return json_encode([
            'success' => true,
            'data' => $data
        ]);
    }

    public function actionGetWarehouses()
    {
        $data = [];
        $post = Yii::$app->request->post();

        $client = new Client(['transport' => 'yii\httpclient\CurlTransport']); // only cURL supports the options we need
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl(Yii::$app->params['novaposhta']['url'])
            ->setData([
                'apiKey' => Yii::$app->params['novaposhta']['apiKey'],
                'modelName' => 'AddressGeneral',
                'calledMethod' => 'getWarehouses',
                'methodProperties' => [
                    'CityRef' => Html::decode($post['cityRef']),
                    'TypeOfWarehouseRef' => '9a68df70-0267-42a8-bb5c-37f427e36ee4' //Вантажне відділення
                ]
            ])
            ->send();
        if (!$response->data['success'])
            return json_encode([
                'success' => false,
                'data' => $response->data['errors']
            ]);
        foreach ($response->data['data'] as $item) {
            $data[] = [
                'description' => $item['Description'], //Пункт приймання-видачі (до 30 кг): вул. Леніна, 54
                'ref' => $item['Ref'] //3294c49f-23ea-11ea-8ac1-0025b502a04e
            ];
        }

        return json_encode([
            'success' => true,
            'data' => $data
        ]);
    }

    public function actionGetStreets()
    {
        $data = [];
        $post = Yii::$app->request->post();

        $client = new Client(['transport' => 'yii\httpclient\CurlTransport']); // only cURL supports the options we need
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl(Yii::$app->params['novaposhta']['url'])
            ->setData([
                'apiKey' => Yii::$app->params['novaposhta']['apiKey'],
                'modelName' => 'Address',
                'calledMethod' => 'getStreet',
                'methodProperties' => [
                    'CityRef' => Html::decode($post['cityRef']),
                    'FindByString' => Html::decode($post['street'])
                ]
            ])
            ->send();
        if (!$response->data['success'])
            return json_encode([
                'success' => false,
                'data' => $response->data['errors']
            ]);
        foreach ($response->data['data'] as $item) {
            $data[] = [
                'description' => $item['Description'] . ' ' . $item['StreetsType'],
                'ref' => $item['Ref']
            ];
        }

        return json_encode([
            'success' => true,
            'data' => $data
        ]);
    }

}
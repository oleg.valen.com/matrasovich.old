<?php
/**
 * Created by PhpStorm.
 * User: oleg-d
 * Date: 13.05.18
 * Time: 17:16
 */

namespace app\controllers;

use app\models\AttributeDescription;
use app\models\CategoryDescription;
use app\models\Product;
use app\models\Stock;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;
use yii\web\Controller;
use app\models\SearchForm;

class StockController extends Controller
{

    public function actionIndex()
    {

        $data = [];
        $products = [];
        $cats = [];

        $this->view->params['modelSearchForm'] = new SearchForm();

        $stocks = Stock::find()
            ->with('productOption')
            ->asArray()
            ->all();
        $productsIds = ArrayHelper::getColumn($stocks, 'product_id');

        $prods = Product::find()
            ->where(['in', 'product_id', $productsIds])
            ->indexBy('product_id')
            ->all();

        $attrs = AttributeDescription::find()
            ->where(['in', 'attribute_id', ArrayHelper::getColumn($stocks, 'attr_id')])
            ->andWhere(['language_id' => Yii::$app->language])
            ->indexBy('attribute_id')
            ->asArray()
            ->all();

        foreach ($stocks as $stock) {
            $productId = $stock['product_id'];
            $attrId = $stock['attr_id'];
            if (array_key_exists($productId, $prods)
                && array_key_exists($attrId, $attrs)) {
                $product = $prods[$productId];
                $attr = $attrs[$attrId];
                $products[$product->category_id][$productId][$attrId] = [
                    'pmodel' => $product,
                    'pName' => $product->productDescription->name,
                    'href' => Url::to([
                        'product/index',
                        'product_id' => $productId,
                        's1' => $product->seo_url,
                        's2' => 'p',
                        's' => $attrId
                    ]),
                    'img' => Html::img($product->getImage()->getPath('128x128'), ['alt' => $product->productDescription->name]),
                    'aName' => $attr['name'],
                    'price' => Yii::$app->formatter->asInteger($stock['productOption']['value']) . ' ' . Yii::t('app', 'грн.'),
                    'productId' => $productId,
                    'attrId' => $attrId,
                ];
                if (!in_array($product->category_id, $cats))
                    $cats[] = $product->category_id;
            }
        }
        $data['products'] = $products;

        $categories = CategoryDescription::find()
            ->select('category_id, name')
            ->where(['in', 'category_id', $cats])
            ->andWhere(['language_id' => Yii::$app->language])
            ->indexBy('category_id')
            ->asArray()
            ->all();
        $data['categories'] = $categories;

        return $this->render('index', $data);
    }

}
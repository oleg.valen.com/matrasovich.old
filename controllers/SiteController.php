<?php

namespace app\controllers;

use app\models\AttributeGroupDescription;
use app\models\BlogDescription;
use app\models\CallUs;
use app\models\CategoryAttributeDescription;
use app\models\CategoryDescription;
use app\models\ChangepasswordForm;
use app\models\DetailAttributeDescription;
use app\models\ForgotpasswordForm;
use app\models\NewcustomerForm;
use app\models\Product;
use app\models\ProductDescription;
use app\models\Review;
use app\models\SearchForm;
use app\models\Subscribe;
use app\models\SubscribeForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;
use yii\helpers\Url;
use app\models\AttributeDescription;
use app\models\ProductOption;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'change-password', 'cart'],
                'rules' => [
                    [
                        'actions' => ['logout', 'change-password', 'cart'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $data = [];
        $data['robots'] = $noindex = $nofollow = '';
        $get = Yii::$app->request->get();

        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = Yii::t('app', 'Тонкие матрасы на диван и ортопедические матрасы | Купить в Днепре, Киеве, Харькове, Львове: цена, отзывы, продажа');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Выравнивающие матрасы на диван, ортопедические матрасы от производителя | Цены | Бесплатная доставка матрасов для дивана по Украине')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'матрасы на диван, купить матрас на диван, ортопедические матрасы, купить ортопедический матрас')]);

        if (key_exists('route', $get)) {
            $noindex = 'noindex';
            $nofollow = 'nofollow';
            $data['robots'] = $noindex . ', ' . $nofollow;
        }

        $data['products'] = Product::getProductsByIds([
            40, 52, 47, 46,
            48, 53, 50, 41,
            59, 54, 69, 86,
            74, 75, 67, 81,
            54, 76, 139, 144,
        ]);


//            'productsCheap' => Product::getProductsByIds([10, 1, 4, 5]),
        return $this->render('index', $data);

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Yii::$app->user->getReturnUrl());
//            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAccount()
    {
        $this->view->title = 'Matrasovish.com.ua | Личный кабинет';
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Личный кабинет. Интернет-магазин Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'Личный кабинет на Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

        //        return $this->render('account');
        if (!Yii::$app->user->isGuest) {
            return $this->render('account', [
            ]);
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
            return $this->render('account', [
            ]);
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionSubscribe()
    {
        $model = new SubscribeForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            echo 'Email is valid';
            Subscribe::subscribe(Yii::$app->request->post()['SubscribeForm']['email']);
        }
        $this->goBack();
    }

    public function actionCompare()
    {

        $request = Yii::$app->request;
        $language = Yii::$app->request->cookies->getValue('_language', 'ru');
        $deleteStr = $language == 'ru' ? 'Удалить' : ($language == 'ua' ? 'Видалити' : 'Delete');
        $nameStr = $language == 'ru' ? 'Наименование' : ($language == 'ua' ? 'Найменування' : 'Name');
        $favoritesStr = $language == 'ru' ? 'Добавить в избранное' : ($language == 'ua' ? 'Додати в обране' : 'Add to favorites');
        $language = $language == 'ru' ? '' : '/' . $language;

        if ($request->isAjax) {
            $post = $request->post();

            $productSimilar = Product::getProductSimilar([], 0, $post['comparisons']);
            if (empty($productSimilar['details']))
                return '';
            $html = "<table class='is-fullwidth is-hoverable is-striped table is-size-7'>";
            $html .= "<thead>";
            $html .= "<tr>";
            $html .= "<th></th>";
            foreach ($productSimilar['products'] as $item) {
                $url = Url::to($language . "/{$item->seo_url}/p{$item->product_id}");
                $html .= "<th>";
                $html .= "<a class='delete is-medium is-primary ok-compare-delete' data-product-id=\"{$item->product_id}\" title='{$deleteStr}'></a>";
                $html .= "<a href='{$url}' title='{$item->productDescriptionLangCookies->name}'><img src='/{$item->getImage()->getPath('250x250')}' alt='{$item->productDescriptionLangCookies->name}'</a>";
                $html .= "</th>";
            }
            $html .= "</tr>";
            $html .= "</thead>";
            $html .= "<tbody>";
            $html .= "<tr>";
            $html .= "<th>{$nameStr}</th>";
            foreach ($productSimilar['products'] as $item) {
                $url = Url::to($language . "/{$item->seo_url}/p{$item->product_id}");
                $html .= "<td>";
                $html .= "<a class='has-text-link' href='{$url}' title='{$item->productDescriptionLangCookies->name}'>{$item->productDescriptionLangCookies->name}</a>";
                $html .= "</td>";
            }
            $html .= "</tr>";

            foreach ($productSimilar['details'] as $key => $details) {
                $html .= "<tr>";
                $html .= "<th>{$key}</th>";
                foreach ($details as $id => $detail) {
                    $html .= "<td>";
                    end($detail);
                    $lastElementKey = key($detail);
                    foreach ($detail as $k => $v) {
                        $html .= $v;
                        if ($k !== $lastElementKey) {
                            $html .= '<br>';
                        }
                    }
                    $html .= "</td>";
                }
                $html .= "</tr>";
            }

            $html .= "<tr>";
            $html .= "<th></th>";
            foreach ($productSimilar['products'] as $item) {
                $html .= "<td>";
                $html .= "<div>";
//                $html .= "<a class='button is-warning has-text-weight-bold is-small ok-main-btn-call-us' title='Быстрая покупка' data-name='{$item->name}' style='margin-right: 0.2em;'>Купить</a>";
                $html .= "<a class='button is-warning is-small ok-product-favorite' title='{$favoritesStr}'><span class='icon' data-product-id='{$item->product_id}'><i class='fas fa-heart' style='cursor: pointer;'></i></span></a>";
                $html .= "</div>";
                $html .= "</td>";
            }
            $html .= "</tr>";

            $html .= "</tbody>";
            $html .= "</table>";
            return $html;
        }

        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Сравнение');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Сравнение. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Сравнение на Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);
        return $this->render('compare',
            [
            ]);
    }

    public function actionFavorites()
    {

        $html = '';
        $request = Yii::$app->request;
        $language = Yii::$app->request->cookies->getValue('_language', 'ru');
        if ($language == 'ru') {
            $deleteStr = 'Удалить';
            $compareStr = 'Добавить в сравнение';
        } elseif ($language == 'ua') {
            $deleteStr = 'Видалити';
            $compareStr = 'Додати в порівняння';
        } else {
            $deleteStr = 'Delete';
            $compareStr = 'Add to compare';
        }

        $language = $language == 'ru' ? '' : '/' . $language;

        if ($request->isAjax) {
            $post = $request->post();

            $productSimilar = Product::getProductSimilar([], 0, $post['favorites']);
            if (empty($productSimilar['details']))
                return '';
            foreach ($productSimilar['products'] as $item) {
                $html .= "<div class='column is-half-mobile is-one-third-tablet is-one-fifth-desktop'>";
                $url = Url::to($language . "/{$item->seo_url}/p{$item->product_id}");
                $html .= "<a class='delete is-medium is-primary ok-favorite-delete' data-product-id=\"{$item->product_id}\" title='{$deleteStr}'></a>";
                $html .= "<a href='{$url}' title='{$item->productDescriptionLangCookies->name}'><img src='/{$item->getImage()->getPath('250x250')}' alt='{$item->productDescriptionLangCookies->name}'</a>";
                $html .= "<p class='has-text-centered'><a href='{$url}' title='{$item['name']}' class='has-text-grey-dark ok-carousel-name'>{$item->productDescriptionLangCookies->name}</a></p>";
                $html .= "<div class='has-text-centered'>";
//                $html .= "<a class='button is-warning has-text-weight-bold is-medium ok-main-btn-call-us' title='Быстрая покупка' data-name='{$item->name}' style='margin-right: 0.2em;'>Купить</a>";
                $html .= "<a class='button is-warning is-medium ok-product-compare' title='{$compareStr}'><span class='icon' data-product-id='{$item->product_id}'><i class='fas fa-balance-scale' style='cursor: pointer;'></i></span></a>";
                $html .= "</div>";
                $html .= "</div>";
            }
            return $html;
        }

        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Избранное');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Избранное. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Избранное на Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);
        return $this->render('favorites',
            [
            ]);
    }

    public function actionNewCustomer()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | Новый покупатель';
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Новый покупатель. Интернет-магазин Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'Новый покупатель на Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

        $model = new NewcustomerForm();
        if ($model->load(Yii::$app->request->post()) && $model->newcustomer()) {
            Yii::$app->session->setFlash('newcustomer.success', 'Профиль успешно создался!');
            return $this->refresh();
        }
//        $model->password = '';
//        $model->confirmation = '';
        return $this->render('newCustomer', ['model' => $model,
        ]);
    }

    public function actionForgotPassword()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | Забыли пароль';
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Забыли пароль. Интернет-магазин Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'Забыли пароль на Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

        $model = new ForgotpasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->forgotpassword()) {
            Yii::$app->session->setFlash('forgotpassword.success', 'На указанный e-mail выслано письмо с новым паролем!');
            return $this->refresh();
        }
//        $model->password = '';
//        $model->confirmation = '';
        return $this->render('forgotPassword', ['model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }

        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | Изменение пароля';
        $this->view->registerMetaTag(['name' => 'description', 'content' => 'Изменение пароля. Интернет-магазин Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => 'Изменение пароля на Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

        $model = new ChangepasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->changepassword()) {
            Yii::$app->session->setFlash('changepassword.success', 'Пароль успешно изменен!');
            return $this->refresh();
        }


//        $model->password = '';
        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }

    public function actionAboutUs()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'О нас');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'О нас. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'О нас на Matrasovich.com.ua')]);
        return $this->render('aboutUs', [
        ]);
    }

    public function actionGarantiyaIVozvrat()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Гарантия и возврат');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Гарантия и возврат. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Гарантия и возврат на Matrasovich.com.ua')]);
        return $this->render('garantiyaIVozvrat', [
        ]);
    }

    public function actionPravilaEkspluataciiMatrasa()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Правила эксплуатации матраса');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Правила эксплуатации матраса. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Правила эксплуатации матраса на Matrasovich.com.ua')]);
        return $this->render('pravilaEkspluataciiMatrasa', [
        ]);
    }

    public function actionPrivacy()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Политика безопасности');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Политика безопасности. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Политика безопасности на Matrasovich.com.ua')]);
        return $this->render('privacy', [
        ]);
    }

    public function actionTerms()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Условия соглашения');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Условия соглашения. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Условия соглашения на Matrasovich.com.ua')]);
        return $this->render('terms', [
        ]);
    }

    public function actionSearch()
    {
        $data = [];
        $get = Yii::$app->request->get();

        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Поиск по запросу');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Поиск. Интернет-магазин Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Поиск на Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

        $productsQuery = Product::getProductsQueryBySearchQuery($get['q']);
        $pages = new Pagination(['totalCount' => $productsQuery->count(), 'pageSize' => 12]);
        $pages->pageSizeParam = false;

        $products = $productsQuery->offset($pages->offset)->limit($pages->limit)->all();
        $data['products'] = $products;
        $data['pages'] = $pages;

        $productIds = array_reduce($products, function ($c, $i) {
            $c[] = $i->product_id;
            return $c;
        }, []);
        $data['reviewsStars'] = Review::getReviewsStars($productIds);

        $this->view->params['modelSearchForm'] = new SearchForm();
        $data['q'] = $get['q'];

        return $this->render('search', $data);

    }

    public function actionStudy()
    {
        $this->view->title = 'Matrasovish.com.ua | Study';
        return $this->render('study');
    }

    public function actionCallus()
    {

        CallUs::add();

    }

    public function actionDostavkaIOplata()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Условия доставки и оплаты');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Доставка и оплата') . ' | Matrasovich.com.ua']);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Доставка и оплата') . ' | Matrasovich.com.ua']);
        return $this->render('dostavkaIOplata', [
        ]);
    }

    public function actionKontrolKachestva()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Контроль качества матрасов и материалов');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Контроль качества изделий и материалов | Лаборатория качества матрасов | Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Контроль качества матрасов и материалов | Matrasovich.com.ua')]);
        return $this->render('kontrolKachestva', [
        ]);
    }

    public function actionCashbackForReview()
    {
        $this->view->params['modelSearchForm'] = new SearchForm();
        $this->view->title = 'Matrasovish.com.ua | ' . Yii::t('app', 'Получите кешбэк 100 грн. за отзыв с фото');
        $this->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('app', 'Получите кешбэк 100 грн. за отзыв с фото | Matrasovich.com.ua')]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('app', 'Получите кешбэк 100 грн. за отзыв с фото | Matrasovich.com.ua')]);
        return $this->render('cashbackForReview', [
        ]);
    }

    public function actionSetcookie()
    {
        $get = Yii::$app->request->get();
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => $get['name'],
            'value' => $get['value'],
        ]));
    }

    public function actionStart()
    {

//        $products = Product::find()->all();
//        $ads = AttributeDescription::find()->indexBy('name')->all();
//
//        foreach ($products as $product) {
////            if (empty($product->manufacturer_url) || $product->product_id >= 147) continue;
//            if (empty($product->manufacturer_url) || !($product->product_id >= 147 && $product->product_id <= 161)) continue;
//
//            $ch = curl_init();
//            $options = array(
//                CURLOPT_URL => $product->manufacturer_url,
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_HEADER => true,
//                CURLOPT_FOLLOWLOCATION => true,
//                CURLOPT_ENCODING => "",
//                CURLOPT_AUTOREFERER => true,
//                CURLOPT_CONNECTTIMEOUT => 120,
//                CURLOPT_TIMEOUT => 120,
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_REFERER => 'https://emm.ua',
//                CURLOPT_USERAGENT => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0',
//            );
//            curl_setopt_array($ch, $options);
//            $response = curl_exec($ch);
////            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
////
////        if ($httpCode != 200) {
////            echo "Return code is {$httpCode} \n"
////                . curl_error($ch);
////        } else {
////            echo "<pre>" . htmlspecialchars($response) . "</pre>";
////        }
//            curl_close($ch);
//
//
//            $first = true;
//            $html = \phpQuery::newDocument($response);
//            $options = pq($html)->find('.form-control.select_size')->find('option')->elements;
//            foreach ($options as $option) {
//                $text = trim($option->textContent);
//                $array = explode('|', $text);
//                $size = explode('x', $array[0]);
//                $width = intval($size[0]);
//                $length = intval($size[1]);
//                $price = intval($array[1]);
//                if (array_key_exists($width . 'x' . $length, $ads)) {
//                    $attribute_id = $ads[$width . 'x' . $length]->attribute_id;
//                    $po = ProductOption::find()->where(['product_id' => $product->product_id, 'attribute_id' => $attribute_id])->one();
//                    if (!empty($po)) {
//                        $po->value = round($price);
//                        $po->save();
////                        echo $product->name . ' | ' . $po->value . ' | ' . $price . PHP_EOL;
//                    }
//                }
//                if ($first) {
//                    $product->price = round($price);
//                    $product->save();
//                }
//                $first = false;
//            }
////            break;
//        }

//        $connection = Yii::$app->getDb();
//        $models = BlogDescription::find()->all();
//        foreach ($models as $m) {
//            $command = $connection->createCommand("
//        insert into blog_description (blog_id, language_id, name, text, description, keywords, title)
//        select blog_id,
//       'ua',
//       name,
//       text,
//       description,
//       keywords,
//       title
//        from blog_description
//        where blog_id = :id", [':id' => $m->blog_id]);
//            $result = $command->query();
//        }
    }

}